package com.codecaique.easy1rx.ui.MapToGetAddressActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.os.Build;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.rquests.AddAddressModel;
import com.codecaique.easy1rx.Models.rquests.UpdateAddressRequest;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.ui.GPSTracker;
import com.codecaique.easy1rx.ui.addresses_management_activity.AdressesActivity;
import com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.new_order.PatientNewOrderFragment;
import com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.referred_order.PatientReferredOrderFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapToGetAddress extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    private TextView textViewAddress;
    private Button buttonOk;
    private LatLng latLng;
    private Marker marker;
    Geocoder geocoder;


    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    UiSettings settings;

    String type ;


    protected Location lastLocation;

    //    private AddressResultReceiver resultReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        textViewAddress = findViewById(R.id.textViewAddress);
        buttonOk = findViewById(R.id.buttonOK);
        type = getIntent().getExtras().getString("type");
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double lat = mMap.getCameraPosition().target.latitude;
                double lng = mMap.getCameraPosition().target.longitude;
                Geocoder geocoder = new Geocoder(MapToGetAddress.this, Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                    if (type.equals("newAddress")) {

                        Log.e("CITY Address das",addresses.get(0).getLocality()
                                + "   ");

                        AdressesActivity.city_name = addresses.get(0).getLocality();
                        AdressesActivity.lat = String.valueOf(addresses.get(0).getLatitude());
                        AdressesActivity.lng = String.valueOf(addresses.get(0).getLongitude());

                        AdressesActivity.editTextNewAddress.setText(textViewAddress.getText().toString());
                        finish();
                    }
                    if (type.equals("updateAddress")) {

                        Log.e("CITY Address",addresses.get(0).getLocality()
                                + addresses.get(0).getAddressLine(0) + "   ");
                        AdressesActivity.city_name = addresses.get(0).getLocality();
                        AdressesActivity.lat = String.valueOf(addresses.get(0).getLatitude());
                        AdressesActivity.lng = String.valueOf(addresses.get(0).getLongitude());


                        AdressesActivity.editTextNewAddressUpdate.setText(textViewAddress.getText().toString());
                        finish();
                    } else if (type.equals("newOrder")) {
                        PatientNewOrderFragment.city_name = addresses.get(0).getLocality();
                        PatientNewOrderFragment.lat = String.valueOf(addresses.get(0).getLatitude());
                        PatientNewOrderFragment.lng = String.valueOf(addresses.get(0).getLongitude());
                        PatientNewOrderFragment.editTextNewAddress.setText(textViewAddress.getText().toString());


                        finish();
                    } else if (type.equals("referredOrder")) {
                        PatientReferredOrderFragment.city_name = addresses.get(0).getLocality();
                        PatientReferredOrderFragment.lat = String.valueOf(addresses.get(0).getLatitude());
                        PatientReferredOrderFragment.lng = String.valueOf(addresses.get(0).getLongitude());
                        PatientReferredOrderFragment.editTextNewAddress.setText(textViewAddress.getText().toString());
                        PatientReferredOrderFragment.textViewCity.setText(textViewAddress.getText().toString());
                        finish();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        } else
            Log.e("MAP NULL", "NUll");

    }

    @Override
    protected void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
//        if (mGoogleApiClient != null) {
//            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
//        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {


        mMap = googleMap;

        //        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
//                == PackageManager.PERMISSION_GRANTED) {
//            mMap.setMyLocationEnabled(true);
//        } else {
//            // Show rationale and request permission.
//        }

//        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        GPSTracker tracker = new GPSTracker(MapToGetAddress.this);

        double lat = tracker.getLatitude();
        double lng = tracker.getLongitude();
        Geocoder geocoder = new Geocoder(MapToGetAddress.this, Locale.getDefault());

        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);

            if (addresses.size() > 0)
            {
                if (type.equals("newAddress")) {
                    Log.e("CITY Address das",addresses.get(0).getLocality()
                            + "   ");
                    AdressesActivity.city_name = addresses.get(0).getLocality();
                    AdressesActivity.lat = String.valueOf(lat);
                    AdressesActivity.lng = String.valueOf(lng);
                }
                if (type.equals("updateAddress")) {
                    Log.e("CITY Address",addresses.get(0).getLocality()
                            + addresses.get(0).getAddressLine(0) + "   ");
                    AdressesActivity.city_name = addresses.get(0).getLocality();
                    AdressesActivity.lat = String.valueOf(lat);
                    AdressesActivity.lng = String.valueOf(lng);
                }

                else if (type.equals("newOrder")) {
                    PatientNewOrderFragment.city_name = addresses.get(0).getLocality();
                    PatientNewOrderFragment.lat = String.valueOf(lat);
                    PatientNewOrderFragment.lng = String.valueOf(lng);
                }

                else if (type.equals("referredOrder")) {
                    PatientReferredOrderFragment.city_name = addresses.get(0).getLocality();
                    PatientReferredOrderFragment.lat = String.valueOf(lat);
                    PatientReferredOrderFragment.lng = String.valueOf(lng);
                }
            }
            else
            {
                Log.e("SIZE", "ZEROO00000");
            }

            } catch (IOException e) {
            e.printStackTrace();
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
//                Toast.makeText(MapToGetAddress.this, "" + latLng, Toast.LENGTH_SHORT).show();

//                settings =  mMap.getUiSettings();
                mMap.clear();
//                MarkerOptions markerOptions = new MarkerOptions();
//                markerOptions.position(latLng);


//                markerOptions.title(latLng.latitude + " : " + latLng.longitude);
//                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
//                mCurrLocationMarker = mMap.addMarker(markerOptions);
                mMap.addMarker(new MarkerOptions().position(latLng));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                //move map camera
//                settings.setZoomControlsEnabled(true);
//                CameraPosition cameraPosition = new CameraPosition.Builder().target
//                        (new LatLng(latLng.latitude,latLng.longitude)).zoom(15).build();
//                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15f));
//                mMap.animateCamera(CameraUpdateFactory.zoomIn());

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(MapToGetAddress.this, Locale.getDefault());// Setting the title for the marker.

                // This will be displayed on taping the marker

                try {
                    addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses != null) {
                        String address = addresses.get(0).getAddressLine(0);
                        Log.e("Address aaaaa", address);
                        // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                        PatientNewOrderFragment.city_name = addresses.get(0).getLocality();


                        AdressesActivity.city_name = addresses.get(0).getLocality();

                        Log.e("Address aaaaa dd", AdressesActivity.city_name + "    ");


//                        Log.e("CITY", PatientNewOrderFragment.city_name);
                        PatientReferredOrderFragment.city_name = addresses.get(0).getLocality();
                        new AddAddressModel().setCity_id(addresses.get(0).getLocality());
                        new UpdateAddressRequest().setCity_id(addresses.get(0).getLocality());


                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
//                        Toast.makeText(MapToGetAddress.this, address, Toast.LENGTH_SHORT).show();
//                        textViewAddress.setText("");
                        textViewAddress.setText(address);
//                        Log.e("country", country);
                        String postalCode = addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName();
//                        Toast.makeText(MapToGetAddress.this, knownName, Toast.LENGTH_SHORT).show();
                        //
                    } else {
                        Toast.makeText(MapToGetAddress.this, "No Address returned!"
                                , Toast.LENGTH_SHORT).show();

                    }

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Toast.makeText(MapToGetAddress.this, "Canont get Address!", Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    boolean check = true;

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        if (check) {

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(MapToGetAddress.this, Locale.getDefault());// Setting the title for the marker.

            // This will be displayed on taping the marker

            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude()
                        , 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (addresses != null) {
                    String address = addresses.get(0).getAddressLine(0);
                    Log.e("Address", address);
                    // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
//                        Toast.makeText(MapToGetAddress.this, address, Toast.LENGTH_SHORT).show();
//                        textViewAddress.setText("");
                    textViewAddress.setText(address);
//                        Log.e("country", country);
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();
//                        Toast.makeText(MapToGetAddress.this, knownName, Toast.LENGTH_SHORT).show();
                    //
                } else {
                    Toast.makeText(MapToGetAddress.this, "No Address returned!"
                            , Toast.LENGTH_SHORT).show();

                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Toast.makeText(MapToGetAddress.this, "Cannot get Address!", Toast.LENGTH_SHORT).show();

            }

            mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15f));
            check = false;
        }

//        //Place current location marker
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title("Current Position");
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
//        mCurrLocationMarker = mMap.addMarker(markerOptions);
//
//        //move map camera
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapToGetAddress.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    public final class Constants {
        public static final int SUCCESS_RESULT = 0;
        public static final int FAILURE_RESULT = 1;
        public static final String PACKAGE_NAME =
                "com.google.android.gms.location.sample.locationaddress";
        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
        public static final String RESULT_DATA_KEY = PACKAGE_NAME +
                ".RESULT_DATA_KEY";
        public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
                ".LOCATION_DATA_EXTRA";
    }

    private void setUpMap() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.getUiSettings().setMapToolbarEnabled(false);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                //save current location
                latLng = point;

                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(point.latitude, point.longitude,1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                android.location.Address address = addresses.get(0);

                if (address != null) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++){
                        sb.append(address.getAddressLine(i) + "\n");
                    }
                    Toast.makeText(MapToGetAddress.this, sb.toString(), Toast.LENGTH_LONG).show();
                }

                //remove previously placed Marker
                if (marker != null) {
                    marker.remove();
                }

                //place marker where user just clicked
                marker = mMap.addMarker(new MarkerOptions().position(point).title("Marker")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));

            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}
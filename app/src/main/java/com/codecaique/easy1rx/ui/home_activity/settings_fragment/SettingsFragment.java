package com.codecaique.easy1rx.ui.home_activity.settings_fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.Models.responses.CancelOrderResponse;
import com.codecaique.easy1rx.LanguageHelper;
import com.codecaique.easy1rx.ui.splash_activity.SplashActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codecaique.easy1rx.LocaleHelper;
import com.codecaique.easy1rx.Models.responses.GetUserAddressesResponse;
import com.codecaique.easy1rx.Models.responses.Get_whats_app_phone;
import com.codecaique.easy1rx.ui.edit_profile_Activity.EditProfileActivity;
import com.codecaique.easy1rx.Models.responses.ProfileResponse;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.codecaique.easy1rx.ui.signin_activity.SignInActivity;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class SettingsFragment extends Fragment {
    private OnFragmentInteractionListener listener;
    TextView textViewEditProfile;
    CircleImageView imageView;
    TextView textViewUserName;
    TextView textViewUserEmail;
    TextView textViewUserPhone;
    TextView textViewlanguage;
    TextView textViewLogout;
    ImageButton buttonBack;
    LinearLayout layout_lang;
    //TextView textViewUserAddress;
   //TextView textViewLandMark;
    LanguageHelper languageHelper;
    ImageButton imageButtonLogout, imageButtonLanguage;
    Dialog pbar;
    RecyclerView recyclerView;
    AdressesAdapterInSettings adapter;
    private FloatingActionButton buttonWhatsApp;
    String whatsAppNumber="";

    private RecyclerView.LayoutManager layoutManager;
    List<GetUserAddressesResponse.DataBean> userAdressesDataBean;


    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_settings, container, false);
        textViewEditProfile= view.findViewById(R.id.edit);
        textViewUserName= view.findViewById(R.id.username);
        textViewUserEmail= view.findViewById(R.id.userEmail);
        textViewUserPhone= view.findViewById(R.id.userPhone);
        textViewlanguage= view.findViewById(R.id.textViewLanguage);
        textViewLogout= view.findViewById(R.id.textViewLogout);
        buttonBack=(ImageButton)view.findViewById(R.id.buttonBack);
        layout_lang=view.findViewById(R.id.lin_language);
//        textViewUserAddress=(TextView)view.findViewById(R.id.userAddress);
//        textViewLandMark=(TextView)view.findViewById(R.id.landmark);
        imageView= view.findViewById(R.id.profile_image);
        imageButtonLogout= view.findViewById(R.id.logout);
        imageButtonLanguage= view.findViewById(R.id.imageButtonLangauge);
        buttonWhatsApp= view.findViewById(R.id.buttonWhatsapp);


        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        ArrayList<String> arrayList=new ArrayList<String>();
//        arrayList.add("23st, beba");
//        arrayList.add("24, new bns, beni_suif");
//        arrayList.add("655,wasta");
//        arrayList.add("33 st elzahra, fashn");

//        adapter = new AdressesAdapterInSettings(getContext(),userAdressesDataBean,new CustomItemClickListener() {
//            @Override
//            public void onItemClick(View v, int position) {
//
//            }
//        });
//
//        recyclerView.setAdapter(adapter);

        textViewEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getContext(),EditProfileActivity.class);
                startActivity(intent);
            }
        });

        pbar = new Dialog(getContext());
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pbar.setContentView(R.layout.prograss_dialog);
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pbar.setTitle(getString(R.string.please_wait));


//        SharedPreferences mPrefs = getActivity().getPreferences(MODE_PRIVATE);
//        Gson gson = new Gson();
//        String json = mPrefs.getString("userModel", "");
//        UserModel userModel = gson.fromJson(json, UserModel.class);

//        int userId=userModel.getUser_id();


        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

        String userId=pref.getString("userId","");
        NetworkConnection connection = new NetworkConnection();

        if (!connection.isOnline()) {
            pbar.show();
            getUserAdresses(userId);
            fetchUserData(""+userId);

        } else {
            Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();


        }

//        imageButtonLanguage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(textViewlanguage.getText().toString().equals("العربية"))
//                {
//                    LocaleHelper.setLocale(getContext(),"ar");
//                    saveLocale("ar");
//
//                }
//                else if(textViewlanguage.getText().toString().equals("English"))
//                {
//                    LocaleHelper.setLocale(getContext(),"en");
//                    saveLocale("en");
//
//                }
//            }
//        });

         languageHelper = new LanguageHelper();

        layout_lang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textViewlanguage.getText().toString().equals("العربية"))
                {
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);// 0 - for private mode
                    String userId=pref.getString("userId","");
                    changeLang(userId,"ar");

                }
                else if(textViewlanguage.getText().toString().equals("English"))
                {
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);// 0 - for private mode
                    String userId=pref.getString("userId","");
                    changeLang(userId,"en");

                }
            }
        });

        imageButtonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();

                Intent intent=new Intent(getContext(),SignInActivity.class);
                startActivity(intent);
            }
        });

        textViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                SharedPreferences pref2 = getActivity().getApplicationContext().getSharedPreferences("CommonPrefs", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                SharedPreferences.Editor editor2 = pref2.edit();

//                editor.putString("userId", "");
//                editor.putBoolean("SignState", false);
//                editor.commit();

                editor.clear().apply();
                editor2.clear().apply();


                Intent intent=new Intent(getContext(),SignInActivity.class);
                startActivity(intent);
            }
        });

        buttonWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                openWhatsApp(view);
                get_whats_app_phone();
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("onClick", "onClick:" );
                Intent intent=new Intent(getContext(), HomeActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


    private void fetchUserData(String userId) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<ProfileResponse> call = projectAPIs.getUserData(userId);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                    if (response.body().getMessage().equals("show data of user")) {

                        pbar.hide();


                        String imagePath="http://pharmacyapi.codecaique.com/uploads/users/"+response.body().getUser_data().get(0).getImage();
                        String fName=response.body().getUser_data().get(0).getFirst_name();
                        if(fName==null)
                        {
                            fName="";
                        }
                        String lName=response.body().getUser_data().get(0).getLast_name();
                        if(lName==null)
                        {
                            lName="";
                        }
                        String userName=fName+" "+lName;
                        String email=response.body().getUser_data().get(0).getEmail();

                        String userPhone=response.body().getUser_data().get(0).getPhone();
//                    String userAdress=response.body().getUser_data().get(0).getAddress();
//                    String landMark=response.body().getUser_data().get(0).getLand_mark();
                        if(response.body().getUser_data().get(0).getImage()==null) {
                            Glide.with(getContext()).load(R.drawable.image0).into(imageView);
                        }
                        else {
                            Glide.with(getContext()).load(imagePath).into(imageView);
                        }
                        textViewUserName.setText(userName);
                        textViewUserEmail.setText(email);
                        textViewUserPhone.setText(userPhone);
//                   if (userAdress==null)
//                   {
////                       textViewUserAddress.setText("click on edit to set you address");
//                   }
//                   else {
////                       textViewUserAddress.setText(userAdress);
//                   }
//                   if (landMark==null)
//                   {
//                       textViewLandMark.setText("click on edit to set you Land Mark");
//                   }
//                   else {
//                       textViewLandMark.setText(landMark);
//                   }
                    }


                    else {
                        pbar.hide();
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", 0); // 0 - for private mode
        boolean state = pref.getBoolean("SignState",true);
        Log.e("State","   "+ state);
        if (!state)
        {
            startActivity(new Intent(getContext(),SignInActivity.class));
        }
    }

    private void getUserAdresses(String userId) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<GetUserAddressesResponse> call = projectAPIs.getUserAddresses(userId);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<GetUserAddressesResponse>() {
            @Override
            public void onResponse(Call<GetUserAddressesResponse> call, Response<GetUserAddressesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                    if (response.body().getMessage().equals("show address data")) {
                        userAdressesDataBean=new ArrayList<GetUserAddressesResponse.DataBean>();
                        userAdressesDataBean=response.body().getData();
                        adapter = new AdressesAdapterInSettings(getContext(),userAdressesDataBean,new CustomItemClickListener() {
                            @Override
                            public void onItemClick(View v, int position) {

                            }
                        });

                        recyclerView.setAdapter(adapter);

                        adapter.notifyDataSetChanged();

                    }


                    else {
                        pbar.hide();
//                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
            }

            @Override
            public void onFailure(Call<GetUserAddressesResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(getActivity(), t.getMessage()+"", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getActivity().getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }


    private void get_whats_app_phone() {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<Get_whats_app_phone> call = projectAPIs.get_whats_app_phone();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<Get_whats_app_phone>() {
            @Override
            public void onResponse(Call<Get_whats_app_phone> call, Response<Get_whats_app_phone> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {

                    Get_whats_app_phone get_whats_app_phone = response.body();
                    String phone=get_whats_app_phone.getAbout_data().getPhone();
                    whatsAppNumber=phone;

                    openWhatsApp(null);

                }
            }
            @Override
            public void onFailure(Call<Get_whats_app_phone> call, Throwable t) {
            }
        });
    }


    public void openWhatsApp(View view){
        PackageManager pm=getActivity().getPackageManager();
        try {


            String toNumber = whatsAppNumber; // Replace with mobile phone number without +Sign or leading zeros, but with country code.
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.

//            toNumber=toNumber.substring(1);
//
//
//            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + "966570133304" +
//                    ""+ "?body=" + ""));
//            sendIntent.setPackage("com.whatsapp");
//            startActivity(sendIntent);

            try {
                // Raise exception if whatsapp doesn't exist
//                get_whats_app_phone();
                String url = "https://api.whatsapp.com/send?phone=" + whatsAppNumber;

//                String url = "https://api.whatsapp.com/send?phone=+966570133304";

                Log.e("URL ssad",url);

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setPackage("com.whatsapp");
                i.setData(Uri.parse(url));
                startActivity(i);


            } catch (Exception e) {
                Toast.makeText(getActivity(), "Please install whatsapp app", Toast.LENGTH_SHORT)
                        .show();
            }

        }
        catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.dont_have_whats_app),Toast.LENGTH_LONG).show();

        }
    }

    public void changeLang(String user_id,String lang)
    {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        Log.e("user ",user_id + lang);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<CancelOrderResponse> call = projectAPIs.change_language(
                RequestBody.create(MediaType.parse("multipart/form-data"), user_id),
                RequestBody.create(MediaType.parse("multipart/form-data"), lang));

        call.enqueue(new Callback<CancelOrderResponse>() {
            @Override
            public void onResponse(Call<CancelOrderResponse> call, Response<CancelOrderResponse> response) {

                if (response.isSuccessful())
                {
                    assert response.body() != null;
                    if (response.body().getError() == 0)
                   {
                       if (lang.equals("ar"))
                       {
                           Toast.makeText(getContext(), "تم", Toast.LENGTH_SHORT).show();
                           languageHelper.ChangeLang(SettingsFragment.this.getResources(),"ar");
                           saveLocale("ar");
                           Intent intent=new Intent(getActivity(), SplashActivity.class);
                           startActivity(intent);

                       } else if (lang.equals("en")) {
                           languageHelper.ChangeLang(SettingsFragment.this.getResources(),"en");
                           saveLocale("en");
                           Toast.makeText(getContext(), "Done", Toast.LENGTH_SHORT).show();
                           Intent intent=new Intent(getActivity(), SplashActivity.class);
                           startActivity(intent);
                       }
                   }
                }

            }

            @Override
            public void onFailure(Call<CancelOrderResponse> call, Throwable t) {

            }
        });
    }


    public interface OnFragmentInteractionListener {
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

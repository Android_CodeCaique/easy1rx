package com.codecaique.easy1rx.ui.make_order_activity;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.BitmapFactory;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.CustomItemClickListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class OrderImagesRecyclerAdapter extends RecyclerView.Adapter<OrderImagesRecyclerAdapter.ViewHolder> {

    ArrayList<Bitmap> data;
    ArrayList<String> paths;

    Context mContext;
    CustomItemClickListener listener;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.make_order_item, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        ImageView imageView = holder.imageView;
//        Bitmap rotatedBitmap=data.get(position);
//        rotatedBitmap=Bitmap.createScaledBitmap(rotatedBitmap,2000,2000,false);

        Glide.with(mContext).load(paths.get(position)).into(holder.imageView);

        holder.delete_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removePhoto(position);
            }
        });


//        ExifInterface ei = null;
//        try {
//            ei = new ExifInterface(paths.get(position));
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
//                ExifInterface.ORIENTATION_UNDEFINED);
//        switch(orientation) {
//
//            case ExifInterface.ORIENTATION_ROTATE_90: {
//                rotatedBitmap = rotateImage(rotatedBitmap, 90);
//                imageView.setImageBitmap(rotatedBitmap);
//
//                break;
//            }
//            case ExifInterface.ORIENTATION_ROTATE_180: {
//                rotatedBitmap = rotateImage(rotatedBitmap, 180);
//                imageView.setImageBitmap(rotatedBitmap);
//
//                break;
//            }
//            case ExifInterface.ORIENTATION_ROTATE_270: {
//                rotatedBitmap = rotateImage(rotatedBitmap, 270);
//                imageView.setImageBitmap(rotatedBitmap);
//
//                break;
//            }
//
//            case ExifInterface.ORIENTATION_NORMAL:
//            default: {
//                rotatedBitmap = rotatedBitmap;
//                imageView.setImageBitmap(rotatedBitmap);
//            }
//        }


    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public OrderImagesRecyclerAdapter(Context mContext, ArrayList<Bitmap> data, ArrayList<String> paths, CustomItemClickListener listener) {
        this.data = data;
        this.mContext = mContext;
        this.listener = listener;
        this.paths = paths;
    }

    public void removePhoto(int i) {

        File fdelete = new File(paths.get(i));
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :");
            } else {
                System.out.println("file not Deleted :");
            }
        }
        data.remove(i);
        paths.remove(i);
        notifyItemRemoved(i);
        notifyDataSetChanged();
//        notifyItemChanged(i);
    }

    public List<MultipartBody.Part> getData()  {

        List<MultipartBody.Part> d = new ArrayList<MultipartBody.Part>();
        for (int i = 0; i < data.size(); i++) {


//            cursor.moveToFirst();
//            int indx = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATA);
//            p = cursor.getString(indx);

            //create a file to write bitmap data
            File f = new File(paths.get(i));
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("TAG", "getData: "+e.getMessage() );
            }

//Convert bitmap to byte array
            Bitmap bitmap =data.get(i);
            byte[] bitmapdata = new byte[0];
            if(bitmap!=null){
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos);
                bitmapdata = bos.toByteArray();
            }else {
                Log.e("error","photonull");
            }
           

//write the bytes in file
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), f);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image[]", f.getName(), reqFile);
            d.add(body);
/*
            /////////////////
//            data.get(i).by
            Bitmap immagex = data.get(i);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            immagex.compress(Bitmap.CompressFormat.PNG, 90, baos);
//            byte[] b = data.get(i).toByteArray();
//            String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
            String p;

            Cursor cursor =
                    mContext.getContentResolver().query(Uri.parse(paths.get(i)), null, null, null, null);
            if (cursor == null) {
                p = Uri.parse(paths.get(i)).toString();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                p = cursor.getString(idx);
            }
            File file = new File(paths.get(i).toString());
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("image/*"), file);

            MultipartBody.Part filesToUpload =
                    MultipartBody.Part.createFormData("image[]", file.getName(), requestFile);

            Log.e("dattaaty", "${Calendar.getInstance().timeInMillis}.png");
            d.add(filesToUpload);
        */
        }
        return d;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, delete_img;


        public ViewHolder(View v) {
            super(v);

            imageView = v.findViewById(R.id.imageView);

            delete_img = v.findViewById(R.id.delete_img);
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
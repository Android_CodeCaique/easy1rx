package com.codecaique.easy1rx.ui.make_order_activity.doctor_fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.ui.custom_photo_gallery_activity.CustomPhotoGalleryActivity;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.codecaique.easy1rx.Models.responses.DoctorMakeOrderResponse;
import com.codecaique.easy1rx.Models.rquests.DoctorMakeorderModel;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.ui.make_order_activity.OrderImagesRecyclerAdapter;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;


public class DoctorMakeOrderFragment extends Fragment {
    private OnFragmentInteractionListener listener;
    ArrayList<Bitmap> arrayListimages;
    ArrayList<String>paths;
    String currentPhotoPath;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private OrderImagesRecyclerAdapter adapter;
    private final int PICK_IMAGE_MULTIPLE =1;
    private final int PICK_IMAGE_FROM_CAMERA=0;
    Button buttonMakeOrder,buttonuploadImage;
    ImageButton imageButtonUploadImage;
    EditText editTextName,editTextPhone,editTextDescription;
    List<MultipartBody.Part>filesToUpload;
    DoctorMakeorderModel.order_data order_data;
    Dialog dialog,pbar,dialogSelectImageType;
    TextView dialogOrderId;
    private String CAPTURE_IMAGE_FILE_PROVIDER = "com.codecaique.easy1rx.fileprovider";
    ImageView gifImg;
    CountryCodePicker ccp;




    public static DoctorMakeOrderFragment newInstance() {
        return new DoctorMakeOrderFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 1);
        }



        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_doctor_make_order, container, false);
        buttonMakeOrder= view.findViewById(R.id.buttonMakeOrder);
        imageButtonUploadImage= view.findViewById(R.id.imageButtonUploadImage);
        buttonuploadImage= view.findViewById(R.id.buttonuploadImage);
        ccp = view.findViewById(R.id.ccp);

        editTextName= view.findViewById(R.id.editTextName);
        editTextPhone= view.findViewById(R.id.editTextPhone);
        editTextDescription= view.findViewById(R.id.editTextDescription);
        ccp.registerPhoneNumberTextView(editTextPhone);


        filesToUpload= new ArrayList<>();
        arrayListimages=new ArrayList<>();
        paths=new ArrayList<>();
        recyclerView= view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new GridLayoutManager(getContext(),2);
//        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter=new OrderImagesRecyclerAdapter(getContext(), arrayListimages,paths, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                RecyclerView.ViewHolder viewHolder = new OrderImagesRecyclerAdapter.ViewHolder(v);
                position = viewHolder.getAdapterPosition();
                arrayListimages.remove(position);
                paths.remove(position);
                filesToUpload.remove(position);
                adapter.notifyDataSetChanged();
            }
        });

        recyclerView.setAdapter(adapter);



        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_orderi_nserted);
        dialogOrderId=dialog.findViewById(R.id.dialogOrderId);



        Button continueShopping = dialog.findViewById(R.id.buttonConitnueShopping);
//                        text.setText("Text view 1");

        continueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
                Intent intent=new Intent(getContext(), HomeActivity.class);
                startActivity(intent);
            }
        });


        pbar = new Dialog(getContext(),R.style.DialogCustomTheme);
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);

        pbar.setContentView(R.layout.prograss_dialog);

        pbar.setTitle(getString(R.string.please_wait));
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        gifImg = pbar.findViewById(R.id.gifImage);
//        Glide.with(getContext()).load(R.drawable.ripple).into(gifImg);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                int position = viewHolder.getAdapterPosition();
                paths.remove(position);
                arrayListimages.remove(position);
                filesToUpload.remove(position);
                adapter.notifyDataSetChanged();
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


        imageButtonUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });
        buttonuploadImage
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        chooseImage();
                    }
                });

        buttonMakeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkConnection connection = new NetworkConnection();

                if (!connection.isOnline()) {

                    String name=editTextName.getText().toString();
                    String phone=editTextPhone.getText().toString();
                    phone=phone.replace(" ","");
                    phone=phone.trim();
                    String description=editTextDescription.getText().toString();
                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    String userId=pref.getString("userId","");
                    order_data=new DoctorMakeorderModel.order_data();
                    order_data.setName(name);
                    order_data.setPhone(phone);
                    order_data.setPro_description(description);
                    order_data.setOrder_id(Integer.parseInt(userId));

                    if(name.equals("")){
                        editTextName.setError(getString(R.string.name));
                    }else if(phone.equals("")){
                        editTextPhone.setError(getString(R.string.phone));
                    }else if(phone.startsWith("0") || phone.length()<9 || phone.length()>9){
                        editTextPhone.setError(getString(R.string.phone_correct));
                    }else if(description.equals("")){
                        editTextDescription.setError(getString(R.string.description));
                    }else if(arrayListimages.size()==0){
                        Toast.makeText(getContext(), getString(R.string.upload_image), Toast.LENGTH_SHORT).show();
                    }else
                    {
                        pbar.show();
                        makeDoctorOrder(Integer.parseInt(userId), adapter.getData(), name, phone, description);
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();


                }


                //                Intent intent=new Intent(getContext(),DeleveryActivity.class);
//                startActivity(intent);

            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }




    public void chooseImage() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);

//        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        startActivityForResult(pickPhoto, 1);
//


//Intent intent=new Intent(getContext(), SelectImagesActivity.class);
//startActivity(intent);


        dialogSelectImageType = new Dialog(getContext());
        dialogSelectImageType.setContentView(R.layout.dialog_image_type);

        ImageButton camera = dialogSelectImageType.findViewById(R.id.buttonCamera);
        ImageButton gallery = dialogSelectImageType.findViewById(R.id.buttonGallery);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectImageType.hide();
//                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takePicture, PICK_IMAGE_FROM_CAMERA);

//                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//                int imageId=pref.getInt("image",0);
//
//                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
//                if (!path.exists()) path.mkdirs();
//                File image = new File(path, "image_capture_"+""+imageId+".jpg");
//                Uri imageUri = FileProvider.getUriForFile(getActivity().getApplicationContext(), CAPTURE_IMAGE_FILE_PROVIDER, image);
//                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//                providePermissionForProvider(cameraIntent, imageUri);
//                startActivityForResult(cameraIntent, PICK_IMAGE_FROM_CAMERA);

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Ensure that there's a camera activity to handle the intent
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(getContext(),
                                "com.codecaique.easy1rx.fileprovider",
                                photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, PICK_IMAGE_FROM_CAMERA);
                    }
                }

            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectImageType.hide();
                Intent intent = new Intent(getActivity(), CustomPhotoGalleryActivity.class);
                startActivityForResult(intent,PICK_IMAGE_MULTIPLE);

            }
        });

        dialogSelectImageType.show();



    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if(requestCode == PICK_IMAGE_FROM_CAMERA)
            {
//                Bitmap photo = (Bitmap) data.getExtras().get("data");
//                Uri tempUri = getImageUri(getContext(), photo);
//                File file1= new File(getRealPathFromURI(tempUri));
//                arrayListimages.add(photo);
//                adapter.notifyDataSetChanged();
//
//                    RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), file1);
//                    MultipartBody.Part body= MultipartBody.Part.createFormData("image[]", file1.getName(), requestFile);
//
//                    filesToUpload.add(body);
//                Log.e("camera", "camera: "+data.getExtras().get("data"));






//                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//                SharedPreferences.Editor editor = pref.edit();
//                int imageId=pref.getInt("image",0);
//
//                //get the file from the uri path  , it is the same declared in the path file in xml folder. in this case it is Gallery/MyImages/
//                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
//                if (!path.exists()) path.mkdirs();
//                File imageFile = new File(path, "image_capture_"+""+imageId+".jpg");
//
//                Uri stringUri = Uri.fromFile(imageFile);





                // Get the dimensions of the bitmap
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;



                // Determine how much to scale down the image

                // Decode the image file into a Bitmap sized to fill the View
                bmOptions.inJustDecodeBounds = false;
                bmOptions.inPurgeable = true;

//                Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
                File imageFile = new File(currentPhotoPath);

//                try {
                Bitmap bitmapFromMedia = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);





                arrayListimages.add(bitmapFromMedia);
                paths.add(currentPhotoPath);
                adapter.notifyDataSetChanged();

//                    editor.putInt("image", imageId+1);
//                    editor.commit();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

                RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), imageFile);
                MultipartBody.Part body= MultipartBody.Part.createFormData("image[]", imageFile.getName(), requestFile);

                filesToUpload.add(body);

            }
            if(requestCode == PICK_IMAGE_MULTIPLE){
                String[] imagesPath = data.getStringExtra("data").split("\\|");

                for (int i=0;i<imagesPath.length;i++){
                    Log.e("onActivityResult", "onActivityResult: "+imagesPath[i]);

                    Bitmap bitmap = BitmapFactory.decodeFile(imagesPath[i]);
                    arrayListimages.add(bitmap);
                    paths.add(imagesPath[i]);


                    File file1=new File(imagesPath[i]);
                    RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), file1);
                    MultipartBody.Part body= MultipartBody.Part.createFormData("image[]", file1.getName(), requestFile);

                    filesToUpload.add(body);

                }

                adapter.notifyDataSetChanged();
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }


    public interface OnFragmentInteractionListener {
    }

    private void makeDoctorOrder(final int user_id, List<MultipartBody.Part> image,String name,String phone,String pro_description) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<DoctorMakeOrderResponse> call = projectAPIs.makeDoctorOrder2(
                user_id,
                image,
                RequestBody.create(MediaType.parse("multipart/form-data"),name),
                RequestBody.create(MediaType.parse("multipart/form-data"),phone),
                RequestBody.create(MediaType.parse("multipart/form-data"),pro_description));
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<DoctorMakeOrderResponse>() {
            @Override
            public void onResponse(Call<DoctorMakeOrderResponse> call, Response<DoctorMakeOrderResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */pbar.hide();
                if (response.isSuccessful()) {
                    if (response.body().getMessage().contains("order inserted success") ||
                            response.body().getMessage().contains("تم ادخال الطلب بنجاح")) {
                        pbar.hide();
                        dialogOrderId.setText("#" + response.body().getOrder_data().getId());
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        dialog.show();
                    } else {
                        pbar.hide();
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<DoctorMakeOrderResponse> call, Throwable t) {

                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                pbar.hide();

            }
        });
    }

    private void showdialoge() {
    }

    private void providePermissionForProvider(Intent intent, Uri uri) {
        List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            getActivity().grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
    public void sendSMS()
    {
        Uri uri = Uri.parse("smsto:01096030098");
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", "there is order placed for you");
        startActivity(it);
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}
package com.codecaique.easy1rx.ui.home_activity.offers_fragment;

/**
 * Created by I Love Allah on 06/10/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.Models.responses.PharmacyResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class PharmacyOffersRecyclerAdapter extends RecyclerView.Adapter<PharmacyOffersRecyclerAdapter.ViewHolder> implements Filterable {

    List<PharmacyResponse.OffersDataBean> data;
    List<PharmacyResponse.OffersDataBean> filteredData;

    Context mContext;
    CustomItemClickListener listener;

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.offersitem, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView textViewTitle = holder.textViewTitle;
        TextView textViewprice = holder.textViewprice;
        TextView textViewPriceAfter = holder.textViewPriceAfter;

        ImageView imageView = holder.imageView;

        Glide.with(mContext).load("http://pharmacyapi.codecaique.com/uploads/offers/"+data.get(position).getImage()).into(imageView);

        SharedPreferences pref = mContext.getSharedPreferences("CommonPrefs", 0); // 0 - for private mode

        String lang = pref.getString("Language","");

        assert lang != null;
        if (lang.equals("ar"))
        {
            textViewTitle.setText(data.get(position).getArabic_name());
        }
        else
            textViewTitle.setText(data.get(position).getEnglish_name());

//        imageView.setImageResource(data.get(position).g));
        textViewprice.setText(data.get(position).getPrice_before()+"");
        textViewprice.setPaintFlags(textViewprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        textViewPriceAfter.setText("   "+data.get(position).getOffer_price());
    }




    @Override
    public int getItemCount() {
        return data.size();
    }

    public PharmacyOffersRecyclerAdapter(Context mContext, List<PharmacyResponse.OffersDataBean> data, CustomItemClickListener listener) {
        this.data = data;
        this.filteredData=data;
        this.mContext = mContext;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String charString = constraint.toString();

                if (charString.isEmpty()){
                    data = filteredData;
                }else{

                    List<PharmacyResponse.OffersDataBean> filterList = new ArrayList<PharmacyResponse.OffersDataBean>();

                    for (PharmacyResponse.OffersDataBean objectbean : filteredData){

                        if ((""+objectbean.getId()).toLowerCase().contains(charString)){
                            filterList.add(objectbean);
                        }
                    }

                    data = filterList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = data;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                data = (List<PharmacyResponse.OffersDataBean>) results.values;
                notifyDataSetChanged();
            }
        };

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textViewTitle;
        TextView textViewprice;
        TextView textViewPriceAfter;

        ViewHolder(View v) {
            super(v);
            textViewTitle = v
                    .findViewById(R.id.textViewTitle);

            textViewprice = v
                    .findViewById(R.id.textViewActiveOrNot);


            textViewPriceAfter = v
                    .findViewById(R.id.textViewSortId);


            imageView = v.findViewById(R.id.imageView);
        }
    }

}

package com.codecaique.easy1rx.ui.signup_activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.UserModel;
import com.codecaique.easy1rx.Models.responses.Registerationresponse;
import com.codecaique.easy1rx.Models.rquests.RegisterationModel;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.codecaique.easy1rx.ui.signin_activity.SignInActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = SignUpActivity.class.getSimpleName();

    private int PICK_IMAGE_REQUEST = 1;
    Uri uri;
    String Phone_num;
    EditText editTextfirst_name;
    EditText editTextphone;
    EditText editTextlast_name;
    TextInputEditText editTextpassword;
    EditText editTextemail;
    CountryCodePicker ccp;

    //    Spinner city_id;
    ImageButton image;
    Button buttonSingup,buttonDelete,buttonSelectImage;
    RadioGroup radioGroup;
    RadioButton radioButtonMale;
    RadioButton radioButtonFemale;
    TextView textViewSignIn;
    Dialog pbar,dialogSelectImage;
    Uri imageuri = null;
    RegisterationModel.UserDataBean userDataBean;
    UserModel userModel;
    MultipartBody.Part filesToUpload=null;
    boolean selected=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SignUpActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SignUpActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SignUpActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
        }
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_sign_up);

        userDataBean = new RegisterationModel.UserDataBean();
        userModel = new UserModel();

        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        editTextfirst_name = findViewById(R.id.editTextFirstName);
        editTextlast_name = findViewById(R.id.editTextLastName);
        editTextphone = findViewById(R.id.editTextPhone);
        editTextpassword = findViewById(R.id.editTextPassword);
        editTextemail = findViewById(R.id.editTextEmail);
//        city_id=(Spinner) findViewById(R.id.spinner1);
        image = findViewById(R.id.imageButtonUploadImage);
        radioGroup = findViewById(R.id.radiogroup);
        radioButtonMale = findViewById(R.id.radioButtonMale);
        radioButtonFemale = findViewById(R.id.radioButtonFemale);
        buttonSingup = findViewById(R.id.buttonSignup);
        textViewSignIn = findViewById(R.id.textSignup);

        dialogSelectImage = new Dialog(SignUpActivity.this);
        dialogSelectImage.setContentView(R.layout.dialog_select_image);
        buttonDelete=(Button) dialogSelectImage.findViewById(R.id.buttonDeletImage);
        buttonSelectImage=(Button) dialogSelectImage.findViewById(R.id.buttonUpdateImage);
        ccp.registerPhoneNumberTextView(editTextphone);

       buttonSelectImage.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               dialogSelectImage.hide();
               chooseImage();
           }
       });
       buttonDelete.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               selected=false;
               image.setImageBitmap(null);
               image.setBackgroundResource(R.drawable.image0);
               filesToUpload=null;
               dialogSelectImage.hide();
           }
       });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected==false)
                {
                    chooseImage();
                }
                else {
                    dialogSelectImage.show();
                }
            }
        });


        buttonSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: ");
                String firstname = editTextfirst_name.getText().toString();
                String lastname = editTextlast_name.getText().toString();
                String email = editTextemail.getText().toString();
                String phone = editTextphone.getText().toString();
                String password = editTextpassword.getText().toString();
                phone=phone.replace(" ","");
                phone=phone.trim();
                String gender = "";
                if (radioButtonMale.isChecked()) {
                    gender = getString(R.string.male);
                    userDataBean.setGender(gender);
                    userModel.setGender(gender);

                } else if (radioButtonFemale.isChecked()) {

                    gender = getString(R.string.female);
                    userDataBean.setGender(gender);
                    userModel.setGender(gender);

                }

                if(firstname.equals("")){
                    editTextfirst_name.setError(getString(R.string.input_fname));
                }else if(lastname.equals("")){
                    editTextlast_name.setError(getString(R.string.input_Lname));
                }else if(email.equals("")){
                    editTextemail.setError(getString(R.string.input_email));
                }else if(phone.equals("")){
                    editTextphone.setError(getString(R.string.error_enter_your_phone));
                }
//                else if(phone.startsWith("0")){
//                    editTextphone.setError(getString(R.string.phone_correct));
//                }
//                else if(phone.length()>9 || phone.length()<9){
//                    editTextphone.setError(getString(R.string.phone_correct));
//                } else if(password.equals("")){
//                    editTextpassword.setError(getString(R.string.input_password));
//                }
                else{
                        Phone_num=phone;

                       // make_Register(firstname, lastname, password, filesToUpload, Phone_num, email, gender);
                    Intent intent = new Intent(SignUpActivity.this, Verification_Actitvity.class);
                    intent.putExtra("phone",Phone_num);
                    intent.putExtra("firstname",firstname);
                    intent.putExtra("lastname",lastname);
                    intent.putExtra("password",password);
                    intent.setData(uri);
                    intent.putExtra("email",email);
                    intent.putExtra("gender",gender);
                    intent.putExtra("action","r");

                    //  intent.putExtra("action","r");
                    // intent.putExtra("code",response.body().getCode());
                    startActivity(intent);
                }

                }
        });


        textViewSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });

    }

//    private void make_Register(String firstname, String lastname, String password,
//                               MultipartBody.Part filesToUpload, String phone_num,
//                               String email, String gender) {
//
//        NetworkConnection connection = new NetworkConnection();
//
//        if (!connection.isOnline()) {
//
//            pbar = new Dialog(SignUpActivity.this);
//            pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            pbar.setContentView(R.layout.prograss_dialog);
//            Window window = pbar.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            pbar.setTitle(getString(R.string.please_wait));
//            pbar.show();
//
//            RegisterUser(firstname, lastname, password, filesToUpload, Phone_num, email, gender);
//
//        } else {
//
//            Toast.makeText(SignUpActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
//
//        }
//    }

//    private void RegisterUser(String firstname, String lastname, String passwords, MultipartBody.Part image, final String Phone_num, String email, String gender) {
//
//        //Obtain an instance of Retrofit by calling the static method.
//        Retrofit retrofit = NetworkClient.getRetrofitClient();
//        /*
//        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
//        */
//        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
//        /*
//        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
//        */
//        String firebaseToken = FirebaseInstanceId.getInstance().getToken();
//
//        Call<Registerationresponse> call = projectAPIs.registerUser(
//                RequestBody.create(MediaType.parse("multipart/form-data"), firstname),
//                RequestBody.create(MediaType.parse("multipart/form-data"), lastname),
//                RequestBody.create(MediaType.parse("multipart/form-data"), passwords),
//                image,
//                RequestBody.create(MediaType.parse("multipart/form-data"), Phone_num),
//                RequestBody.create(MediaType.parse("multipart/form-data"), email),
//                RequestBody.create(MediaType.parse("multipart/form-data"), gender),
//                RequestBody.create(MediaType.parse("multipart/form-data"), firebaseToken ));
//        /*
//        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
//        */
//        call.enqueue(new Callback<Registerationresponse>() {
//            @Override
//            public void onResponse(Call<Registerationresponse> call, Response<Registerationresponse> response) {
//                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
//                 */
//                pbar.hide();
//                if (response.isSuccessful()) {
//                    if (response.body().getError() == 0) {
//                        pbar.hide();
//                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//                        SharedPreferences.Editor editor = pref.edit();
////                        Toast.makeText(SignUpActivity.this, "id" + response.body().getUser_data().getId(), Toast.LENGTH_SHORT).show();
//                        editor.putString("userId", "" + response.body().getUser_data().getId());
//                        editor.putString("userState", "" + response.body().getUser_data().getState());
//                        editor.putString("phone",Phone_num);
//                        editor.putString("action","r");
//                        editor.putBoolean("SignState", false);
//                        editor.putBoolean("verification",false);
//                        editor.commit();
//
//
//                    }
//                    else {
//                        pbar.hide();
//
//                        Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                        Log.e("onResponse", response.body().getMessage());
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Registerationresponse> call, Throwable t) {
//                pbar.hide();
//                Log.e("failure", t.getMessage());
//
//                Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }


    public void chooseImage() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), 1);
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, PICK_IMAGE_REQUEST);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

             uri = data.getData();
            image.setImageURI(uri);
//            userDataBean.setImage(uri);
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//                // Log.d(TAG, String.valueOf(bitmap));
//                image.setBackground(null);
//                image.setImageBitmap(bitmap);
//                selected=true;
//
//                MultipartBody.Part body = null;
//                if (uri != null) {
//                    Cursor cursor = null;
//                    String p = null;
//                    try {
//                        String[] proj = {MediaStore.Images.Media.DATA};
//                        cursor = getContentResolver().query(uri, proj, null, null, null);
//                        assert cursor != null;
//                        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                        cursor.moveToFirst();
//                        p = cursor.getString(column_index);
//                    } finally {
//                        if (cursor != null) {
//                            cursor.close();
//                        }
//                    }
//                    File file1 = new File(p);
//                    RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file1);
//                    body = MultipartBody.Part.createFormData("image", file1.getName(), requestFile);
//
//                    filesToUpload=(body);
//
//                }
//
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
    }
    public void loadLocale() {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        changeLang(language);
    }
    public  void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        Locale myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());

    }
    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

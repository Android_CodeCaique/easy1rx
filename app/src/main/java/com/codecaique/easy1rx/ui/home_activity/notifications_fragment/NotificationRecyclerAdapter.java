package com.codecaique.easy1rx.ui.home_activity.notifications_fragment;

/**
 * Created by I Love Allah on 06/10/2018.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.Models.responses.NotificationResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;


public class NotificationRecyclerAdapter extends RecyclerView.Adapter<NotificationRecyclerAdapter.ViewHolder> {

    List<NotificationResponse.NotificationDataBean> data;

    Context mContext;
    CustomItemClickListener listener;

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView textViewtitle = holder.textViewtitle;
        TextView textViewstate = holder.textViewstate;
        TextView textViewDescription = holder.textViewDescription;
        ImageView imageView=holder.imageView;




        textViewtitle.setText(data.get(position).getTitle());
        textViewstate.setText(data.get(position).getState());
        textViewDescription.setText(data.get(position).getBody());

        Glide.with(mContext).load(R.drawable.logo).into(imageView);



    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public NotificationRecyclerAdapter(Context mContext, List<NotificationResponse.NotificationDataBean> data, CustomItemClickListener listener) {
        this.data = data;
        this.mContext = mContext;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewtitle;
        TextView textViewstate;
        TextView textViewDescription;
        ImageView imageView;

        ViewHolder(View v) {
            super(v);
            textViewtitle = v
                    .findViewById(R.id.textViewTitle);

            textViewstate = v
                    .findViewById(R.id.textViewState);


            textViewDescription = v
                    .findViewById(R.id.textViewDescription);

            imageView = v
                    .findViewById(R.id.imageView);


        }
    }

}

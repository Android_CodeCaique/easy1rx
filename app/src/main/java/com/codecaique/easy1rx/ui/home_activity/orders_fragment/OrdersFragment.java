package com.codecaique.easy1rx.ui.home_activity.orders_fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;


import com.codecaique.easy1rx.Models.responses.Get_whats_app_phone;
import com.codecaique.easy1rx.ui.OrderDetailsActivity.OrderDetails;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.codecaique.easy1rx.Models.responses.PatientOrdersResponse;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class OrdersFragment extends Fragment {
    private OnFragmentInteractionListener listener;
    private RecyclerView recyclerView;
    ImageButton buttonBack;
    ImageView gifImg;

    //    private TextView textViewDone;
private FloatingActionButton buttonWhatsApp;
    String whatsAppNumber="";

    private List<PatientOrdersResponse.UserOrdersBean> ordersData;
    private RecyclerView.LayoutManager layoutManager;
    public static PatientOrdersRecyclerAdapter oredersAdapter;
    private Dialog dialog,pbar;
    public static OrdersFragment newInstance() {
        return new OrdersFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_orders, container, false);

        buttonWhatsApp= view.findViewById(R.id.buttonWhatsapp);
        buttonBack=(ImageButton)view.findViewById(R.id.buttonBack);

        recyclerView = view. findViewById(R.id.recyclerView);
//        textViewDone=(TextView)view.findViewById(R.id.textViewDone);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


//        textViewDone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(textViewDone.getText().toString().equals("Cancel"))
//                {
//                    textViewDone.setText("Done");
//                }
//                else if(textViewDone.getText().toString().equals("Done"))
//                {
//                    textViewDone.setText("Cancel");
//                }
//            }
//        });

        pbar = new Dialog(getContext());
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pbar.setContentView(R.layout.prograss_dialog);
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pbar.setTitle(getString(R.string.please_wait));
//        gifImg = pbar.findViewById(R.id.gifImage);
//        Glide.with(getContext()).load(R.drawable.ripple).into(gifImg);

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.empty_cart);
//                        dialog.setTitle("Title");

// set the custom dialog components - text, image and button
        Button continueShopping = dialog.findViewById(R.id.buttonConitnueShopping);
//                        text.setText("Text view 1");

        continueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
                Intent intent=new Intent(getContext(), HomeActivity.class);
                startActivity(intent);
            }
        });




//        SharedPreferences mPrefs = getActivity().getPreferences(MODE_PRIVATE);
//        Gson gson = new Gson();
//        String json = mPrefs.getString("userModel", "");
//        UserModel userModel = gson.fromJson(json, UserModel.class);

//        int userId=userModel.getUser_id();
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

        String userId=pref.getString("userId","");

        NetworkConnection connection = new NetworkConnection();

        if (!connection.isOnline()) {
            pbar.show();
            fetchOrdersDetails(""+userId);


//            OrdersViewModel ordersViewModel;
//            ordersViewModel = ViewModelProviders.of().get(OrdersViewModel.class);
//
//
//            ordersViewModel.getOrders();
//
//            //
//
//
//
//            final OrdersAdapter adapter = new OrdersAdapter();
//
//            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//            recyclerView.setAdapter(adapter);
//
//
//            ordersViewModel.newsListMutableLiveData.observe(this, new Observer<List<PatientOrdersResponse.UserOrdersBean>>() {
//                @Override
//                public void onChanged(List<PatientOrdersResponse.UserOrdersBean> newsResponses) {
//                    if(newsResponses.size() > 0){
//                    }
//                    adapter.setList(newsResponses);
//                }
//            });








        } else {

            Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();


        }


//        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {
//
//            @Override
//            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//                Toast.makeText(getContext(), "on Move", Toast.LENGTH_SHORT).show();
//                return false;
//            }
//
//            @Override
//            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
//                //Remove swiped item from list and notify the RecyclerView
//                int position = viewHolder.getAdapterPosition();
//                String orderId=""+ordersData.get(position).getId();
//                CancelOrder(orderId,position);
//
//            }
//        };
//
//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
//        itemTouchHelper.attachToRecyclerView(recyclerView);


        buttonWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                openWhatsApp(view);
                get_whats_app_phone();

            }
        });



        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("onClick", "onClick:" );
                Intent intent=new Intent(getContext(), HomeActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }



    private void fetchOrdersDetails(String userId) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<PatientOrdersResponse> call = projectAPIs.getPatientOrders(userId);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<PatientOrdersResponse>() {
            @Override
            public void onResponse(Call<PatientOrdersResponse> call, Response<PatientOrdersResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                    if(response.body().getMessage().equals("show all orders for user")) {
                    ordersData=new ArrayList<PatientOrdersResponse.UserOrdersBean>();

                    List<PatientOrdersResponse.UserOrdersBean> orders = response.body().getUser_orders();

                    ordersData =  orders;

                    oredersAdapter = new PatientOrdersRecyclerAdapter(getContext(), ordersData, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position) {
                            Intent intent=new Intent(getContext(), OrderDetails.class);
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("my object2",ordersData.get(position));
                            intent.putExtra("data2",bundle);
                            startActivity(intent);
                        }
                    });

                   pbar.hide();

                    recyclerView.setAdapter(oredersAdapter);
                    oredersAdapter.notifyDataSetChanged();




                }
                else{

                    pbar.hide();
                        dialog.show();

                    }
            }
            @Override
            public void onFailure(Call<PatientOrdersResponse> call, Throwable t) {
            pbar.hide();
                Toast.makeText(getContext(), getString(R.string.network__problems), Toast.LENGTH_SHORT).show();
            }
        });
    }



//    private void CancelOrder(String orderId, final int itemPosition) {
//
//        //Obtain an instance of Retrofit by calling the static method.
//        Retrofit retrofit = NetworkClient.getRetrofitClient();
//        /*
//        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
//        */
//        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
//        /*
//        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
//        */
//        Call<GetOredrIdResponse> call = projectAPIs.cancelOrder(orderId);
//        /*
//        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
//        */
//        call.enqueue(new Callback<GetOredrIdResponse>() {
//            @Override
//            public void onResponse(Call<GetOredrIdResponse> call, Response<GetOredrIdResponse> response) {
//                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
//                 */
//                if(response.isSuccessful()) {
//                    if(response.body().getMessage().equals("order canceled successfully")) {
//                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                        ordersData.remove(itemPosition);
//                        oredersAdapter.notifyDataSetChanged();
//                    }
//
//
//                }
//            }
//            @Override
//            public void onFailure(Call<GetOredrIdResponse> call, Throwable t) {
//            }
//        });
//    }

    private void get_whats_app_phone() {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<Get_whats_app_phone> call = projectAPIs.get_whats_app_phone();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<Get_whats_app_phone>() {
            @Override
            public void onResponse(Call<Get_whats_app_phone> call, Response<Get_whats_app_phone> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {

                    Get_whats_app_phone get_whats_app_phone = response.body();
                    String phone=get_whats_app_phone.getAbout_data().getPhone();
                    whatsAppNumber=phone;

                    openWhatsApp(null);

                }
            }
            @Override
            public void onFailure(Call<Get_whats_app_phone> call, Throwable t) {
            }
        });
    }


    public void openWhatsApp(View view){
        PackageManager pm=getActivity().getPackageManager();
        try {


            String toNumber = whatsAppNumber; // Replace with mobile phone number without +Sign or leading zeros, but with country code.
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.

//            toNumber=toNumber.substring(1);
//
//
//            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + "966570133304" +
//                    ""+ "?body=" + ""));
//            sendIntent.setPackage("com.whatsapp");
//            startActivity(sendIntent);

            try {
                // Raise exception if whatsapp doesn't exist
//                get_whats_app_phone();
                String url = "https://api.whatsapp.com/send?phone=" + whatsAppNumber;

//                String url = "https://api.whatsapp.com/send?phone=+966570133304";

                Log.e("URL ssad",url);

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setPackage("com.whatsapp");
                i.setData(Uri.parse(url));
                startActivity(i);


            } catch (Exception e) {
                Toast.makeText(getActivity(), "Please install whatsapp app", Toast.LENGTH_SHORT)
                        .show();
            }

        }
        catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.dont_have_whats_app),Toast.LENGTH_LONG).show();

        }
    }


    public interface OnFragmentInteractionListener {
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

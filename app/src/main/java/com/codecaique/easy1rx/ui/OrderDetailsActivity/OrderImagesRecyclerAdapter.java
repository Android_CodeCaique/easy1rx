package com.codecaique.easy1rx.ui.OrderDetailsActivity;

/**
 * Created by I Love Allah on 06/10/2018.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;


public class OrderImagesRecyclerAdapter extends RecyclerView.Adapter<OrderImagesRecyclerAdapter.ViewHolder> {

    ArrayList<String> data;
    String TAG;

    Context mContext;
    CustomItemClickListener listener;

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.make_order_item, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        ImageView imageView = holder.imageView;


        String orderImagesPath="http://pharmacyapi.codecaique.com/uploads/orders/"+data.get(position);

//        Picasso.with(mContext).load(orderImagesPath).into(imageView);

//        if(position==data.size()-1)
//        {
//            Picasso.with(mContext)
//                    .load(orderImagesPath)
//                    .into(imageView, new com.squareup.picasso.Callback() {
//                        @Override
//                        public void onSuccess() {
////                        Toast.makeText(mContext, "success", Toast.LENGTH_SHORT).show();
//                            OrderDetails.pbar.hide();
//                        }
//                        @Override
//                        public void onError() {
//                            OrderDetails.pbar.hide();
//                            Toast.makeText(mContext, "Network Problems", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//        }
//       else {
//            Picasso.with(mContext)
//                    .load(orderImagesPath)
//                    .into(imageView, new com.squareup.picasso.Callback() {
//                        @Override
//                        public void onSuccess() {
////                        Toast.makeText(mContext, "success", Toast.LENGTH_SHORT).show();
//
//                        }
//
//                        @Override
//                        public void onError() {
//                            Toast.makeText(mContext, "Network Problems", Toast.LENGTH_SHORT).show();
//                        }
//                    });

        if (TAG.equals("show"))
        {
            holder.delete.setVisibility(View.INVISIBLE);
        }
        if(position==data.size()-1)
        {
            Glide.with(mContext).load(orderImagesPath).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    OrderDetails.pbar.hide();
                    Toast.makeText(mContext, mContext.getString(R.string.network__problems), Toast.LENGTH_SHORT).show();
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    OrderDetails.pbar.hide();
                    return false;
                }
            }).into(imageView);


        }
        else {
            Glide.with(mContext).load(orderImagesPath).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).into(imageView);
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public OrderImagesRecyclerAdapter(Context mContext, ArrayList<String> data
            , CustomItemClickListener listener, String TAG) {
        this.data = data;
        this.mContext = mContext;
        this.listener = listener;
        this.TAG = TAG;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        ImageView delete;

        ViewHolder(View v) {
            super(v);






            imageView = (ImageView) v.findViewById(R.id.imageView);
            delete = (ImageView) v.findViewById(R.id.delete_img);
        }
    }

}

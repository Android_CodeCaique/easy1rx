package com.codecaique.easy1rx.ui.signin_activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;

import com.codecaique.easy1rx.ui.forget_password.ForgetPasswordActivity;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.UserModel;
import com.codecaique.easy1rx.Models.responses.SignInResponse;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.ui.signup_activity.SignUpActivity;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class SignInActivity extends AppCompatActivity {
    EditText edetTextphone;
    TextInputEditText editTextpassword;
    Button buttonSingIn;
    TextView textSignup,forget_pass;
    Dialog pbar;
    CountryCodePicker ccp;
    SignInResponse.UserDataBean userDataBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        loadLocale();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        edetTextphone = findViewById(R.id.editTextPhone);
        editTextpassword = findViewById(R.id.editTextPassword);
        buttonSingIn= findViewById(R.id.buttonSignIn);
        textSignup = findViewById(R.id.textSignup);
        ccp.registerPhoneNumberTextView(edetTextphone);
        userDataBean=new SignInResponse.UserDataBean();

        textSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        forget_pass = findViewById(R.id.forget_pass);
        forget_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, ForgetPasswordActivity.class));
            }
        });

        buttonSingIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firebaseToken= FirebaseInstanceId.getInstance().getToken();
                userDataBean.setFirebase_token(firebaseToken);
                String phone=edetTextphone.getText().toString();
                userDataBean.setPhone(phone);
                String password=editTextpassword.getText().toString();
                userDataBean.setPasswords(password);
                phone=phone.replace(" ","");
                phone=phone.trim();
                if (phone.equals("")) {
                    edetTextphone.setError(getString(R.string.error_enter_your_phone));
                }
//                else if(phone.length() >9 || phone.length()<9){
//                    edetTextphone.setError(getString(R.string.phone_correct));
//                }else if(phone.startsWith("0")){
//                    edetTextphone.setError(getString(R.string.phone_correct));
//                }
                else if (password.equals("")) {
                    editTextpassword.setError(getString(R.string.error_enter_your_password));
                }else {


                    NetworkConnection connection = new NetworkConnection();

                    if (!connection.isOnline()) {
                        pbar = new Dialog(SignInActivity.this);
                        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        pbar.setContentView(R.layout.prograss_dialog);
                        Window window = pbar.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        pbar.setTitle(getString(R.string.please_wait));
                        pbar.show();
                        sigInUser(userDataBean);

                    } else {
                        Toast.makeText(SignInActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

                    }

                }


            }
        });

    }

//    private boolean doubleBackToExitPressedOnce;
//    private final Handler mHandler = new Handler(Looper.getMainLooper());
//
//    private final Runnable mRunnable = new Runnable() {
//        @Override
//        public void run() {
//            doubleBackToExitPressedOnce = false;
//        }
//    };


    @Override
    public void onBackPressed() {
//        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
//
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
            finish();
//
//        }
//
//        else {
//            this.doubleBackToExitPressedOnce = true;
//            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
//        }
//
//        mHandler.postDelayed(mRunnable, 2000);
    }

    private void sigInUser(SignInResponse.UserDataBean userDataBean) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<SignInResponse> call = projectAPIs.signInUser(userDataBean);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                assert response.body() != null;
                if(response.body() != null && response.body().getMessage().equals("login success"))
                {
                    SignInResponse.UserDataBean user_data= response.body().getUser_data();
                    UserModel userModel=new UserModel();
                    userModel.setUser_id(user_data.getId());
                    userModel.setFirst_name(user_data.getFirst_name());
                    userModel.setLast_name(user_data.getLast_name());
                    userModel.setPhone(user_data.getPhone());
                    userModel.setPassword(user_data.getPasswords());
                    userModel.setEmail(user_data.getEmail());
                    userModel.setLand_mark(user_data.getLand_mark());
                    userModel.setGender(user_data.getGender());
                    userModel.setAddress(user_data.getAddress());

                    userModel.setState(user_data.getState());

//                    SharedPreferences mPrefs = getPreferences(MODE_PRIVATE);
//                    SharedPreferences.Editor prefsEditor = mPrefs.edit();
//                    Gson gson = new Gson();
//                    String json = gson.toJson(userModel);
//                    prefsEditor.putString("userModel", json);
//                    prefsEditor.putString("userId", ""+user_data.getId());
//
//
//                    //for checkLoginStatus
//                    prefsEditor.putString("SignState", "Login");
//                    prefsEditor.commit();


                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("userId", ""+user_data.getId());
                    editor.putString("userState", response.body().getUser_data().getState());
                    editor.putBoolean("SignState", true);
                    editor.putBoolean("verification",true);
                    editor.putString("username",response.body().getUser_data().getPhone());
                    editor.putString("password",response.body().getUser_data().getPasswords());
                    editor.commit();
                    pbar.hide();
                    Intent intent=new Intent(SignInActivity.this, HomeActivity.class);
                    startActivity(intent);

                }else{
                    pbar.hide();
                    Toast.makeText(SignInActivity.this, "There is an error please try again", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(SignInActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void loadLocale() {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        changeLang(language);
    }
    public  void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        Locale myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());

    }
    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

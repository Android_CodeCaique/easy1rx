package com.codecaique.easy1rx.ui.edit_profile_Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.Models.responses.UpdateAddressResponse;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.codecaique.easy1rx.ui.home_activity.settings_fragment.SettingsFragment;
import com.codecaique.easy1rx.ui.signin_activity.SignInActivity;
import com.codecaique.easy1rx.ui.signup_activity.Verification_Actitvity;
import com.google.android.material.textfield.TextInputEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.codecaique.easy1rx.ui.addresses_management_activity.AdressesActivity;
import com.codecaique.easy1rx.Models.responses.ProfileResponse;
import com.codecaique.easy1rx.Models.responses.ShowCitiesResponse;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditProfileActivity extends AppCompatActivity {
    EditText editTextfirst_name;
    EditText editTextphone;
    EditText editTextlast_name;
    TextInputEditText editTextpassword;
    EditText editTextemail;
    Dialog pbar;
    CircleImageView image;
    Button buttonSaveChanges;
    ImageButton buttonBack;
    RadioGroup radioGroup;
    RadioButton radioButtonMale;
    RadioButton radioButtonFemale;
    EditText editTexCarId;
    Button ButtonAdressesManagement;
    List<MultipartBody.Part> partimage=null;
    MultipartBody.Part filesToUpload;
    CountryCodePicker ccp;

    String phone_txt;

    String userId=null;
    String gender=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        if (ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
//        }
//
//        if (ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//        }
//
//        if (ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
//        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        buttonBack= findViewById(R.id.buttonBack);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        editTextfirst_name= findViewById(R.id.editTextFirstName);
        editTextlast_name= findViewById(R.id.editTextLastName);
        editTextphone= findViewById(R.id.editTextPhone);
        editTextpassword= findViewById(R.id.editTextPassword);
        editTextemail= findViewById(R.id.editTextEmail);
        ButtonAdressesManagement= findViewById(R.id.buttonAddressesManagement);
        ccp.registerPhoneNumberTextView(editTextphone);
        image= findViewById(R.id.profile_image);
        radioGroup= findViewById(R.id.radiogroup);
        radioButtonMale = findViewById(R.id.radioButtonMale);
        radioButtonFemale = findViewById(R.id.radioButtonFemale);

        editTexCarId= findViewById(R.id.editTextCarId);
        buttonSaveChanges= findViewById(R.id.buttonSaveChanges);
        filesToUpload=null;
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                RadioButton checkedRadioButton = group.findViewById(checkedId);
                boolean isChecked = checkedRadioButton.isChecked();
                if (isChecked)
                {
                    if(checkedRadioButton.getId()==R.id.radioButtonMale)
                    {
                        gender=getString(R.string.male);
                    }
                    else {
                        gender=getString(R.string.female);

                    }
                }
            }
        });


        ButtonAdressesManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(EditProfileActivity.this, AdressesActivity.class);
                startActivity(intent);
            }
        });



        pbar = new Dialog(EditProfileActivity.this);
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pbar.setContentView(R.layout.prograss_dialog);
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        pbar.setTitle(getString(R.string.please_wait));



        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

        userId=pref.getString("userId","");

        pbar.show();
        fetchUserData(""+userId);


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pbar.hide();
                pbar.dismiss();
                hasStoragePermission(1);
            }
        });

        buttonSaveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get user id from sharedPref

                if (radioButtonMale.isChecked()) {
                    gender = getString(R.string.male);
                } else if (radioButtonFemale.isChecked()) {
                    gender = getString(R.string.female);
                }

                String firstname = editTextfirst_name.getText().toString();
                String lastname = editTextlast_name.getText().toString();
                String phone = editTextphone.getText().toString();
                String password = editTextpassword.getText().toString();
                String email = editTextemail.getText().toString();
                String address = "";
                int cityId = 0;
                phone=phone.replace(" ","");
                phone=phone.trim();
                String landMark = "";
                String careId = editTexCarId.getText().toString();

                if (phone.equals("")) {
                    editTextphone.setError(getString(R.string.error_enter_your_phone));
                }else if(phone.length() >9 || phone.length()<9){
                    editTextphone.setError(getString(R.string.phone_correct));
                }else if(phone.startsWith("0")){
                    editTextphone.setError(getString(R.string.phone_correct));
                }
                else if (password.equals("")) {
                    editTextpassword.setError(getString(R.string.error_enter_your_password));
                } else {


                    NetworkConnection connection = new NetworkConnection();

                    if (!connection.isOnline()) {
                        pbar.show();

                        RegisterUser(Integer.parseInt(userId), firstname, lastname, password, filesToUpload, phone, email, gender, landMark, careId, address);

                    } else {
                        Toast.makeText(EditProfileActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();


                    }

            }
            }
        });


        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("onClick", "onClick:" );
                onBackPressed();

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        boolean state = pref.getBoolean("SignState",true);
        if (!state)
        {
            startActivity(new Intent(EditProfileActivity.this,SignInActivity.class));
        }

    }

    private void RegisterUser(int user_id, String firstname, String lastname, String password, MultipartBody.Part image, final String phone, String email, String gender, String landMark, String relityPro, String address) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */

        Call<UpdateAddressResponse> call = projectAPIs.updateProfile(user_id,
                RequestBody.create(MediaType.parse("multipart/form-data"),firstname),
                RequestBody.create(MediaType.parse("multipart/form-data"),lastname),
                RequestBody.create(MediaType.parse("multipart/form-data"),password),
                image,
                RequestBody.create(MediaType.parse("multipart/form-data"),phone),
                RequestBody.create(MediaType.parse("multipart/form-data"),email),
                RequestBody.create(MediaType.parse("multipart/form-data"),gender),
                RequestBody.create(MediaType.parse("multipart/form-data"),relityPro));

        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */

        call.enqueue(new Callback<UpdateAddressResponse>() {
            @Override
            public void onResponse(Call<UpdateAddressResponse> call, Response<UpdateAddressResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                {
                    assert response.body() != null;
                    if(response.body().getError() == 0)
                    {

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.commit();
                        pbar.hide();

                        Toast.makeText(EditProfileActivity.this, getString(R.string.updated_data_successfully), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(EditProfileActivity.this, HomeActivity.class));

                    }

                    if(response.body().getError() == 3)
                    {
                        pbar.hide();
                        if (!phone.equals(phone_txt))
                        {

                           // Toast.makeText(EditProfileActivity.this,response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(EditProfileActivity.this, Verification_Actitvity.class);
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("action","u");
                            editor.commit();
                            intent.putExtra("phone",phone);
                            intent.putExtra("action","u");
                            intent.putExtra("code",response.body().getCode());
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            Toast.makeText(EditProfileActivity.this, getString(R.string.updated_data_successfully), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(EditProfileActivity.this, HomeActivity.class));
                        }
                    }

                    if(response.body().getError() == 1){
                        pbar.hide();
                        Toast.makeText(EditProfileActivity.this,"\n"
                                + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else {
                        pbar.hide();
                        Toast.makeText(EditProfileActivity.this,"\n"
                                + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<UpdateAddressResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(EditProfileActivity.this, "Failure  \n"+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchUserData(String userid) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<ProfileResponse> call = projectAPIs.getUserData(userid);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if (response.isSuccessful()) {

                    ProfileResponse.UserDataBean pUserDataBean=response.body().getUser_data().get(0);
                    String imagePath="http://pharmacyapi.codecaique.com/uploads/users/"+pUserDataBean.getImage();
                    String firstName=pUserDataBean.getFirst_name();
                    String lastName=pUserDataBean.getLast_name();

                    String email=pUserDataBean.getEmail();
                    String userPhone=pUserDataBean.getPhone();
                    String password=pUserDataBean.getPasswords();
                    String careId=pUserDataBean.getReality_program();
                    String gender=pUserDataBean.getGender();
//                    String userAdress=pUserDataBean.getAddress();
//                    String landMark=pUserDataBean.getLand_mark();
                    if(pUserDataBean.getImage()!=null){
                        Glide.with(EditProfileActivity.this).load(imagePath).into(image);

                    }
                    else {
                        Glide.with(EditProfileActivity.this).load(R.drawable.image0).into(image);

                    }

                    if(firstName!=null){
                        editTextfirst_name.setText(firstName);

                    }

                    if(lastName!=null){
                        editTextlast_name.setText(lastName);

                    }

                    if(email!=null){
                        editTextemail.setText(email);
                    }
                    if(userPhone!=null){
                        phone_txt = userPhone;
                        editTextphone.setText(userPhone);
                    }
                    if(password!=null){
                        editTextpassword.setText(password);
                    }
                    if(gender!=null)
                    {
                        if(gender.equals("Male")||gender.equals("ذكر"))
                        {
                            radioGroup.check(R.id.radioButtonMale);
                        }
                        else
                        {
                            radioGroup.check(R.id.radioButtonFemale);
                        }
                    }
                    if(careId!=null)
                    {
                        editTexCarId.setText(careId);
                    }
//                    if(landMark!=null){
//                        editTextLandMark.setText(landMark);
//                    }
//                    if(userAdress!=null){
//                        editTextAddress.setText(userAdress);
//                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//                        SharedPreferences.Editor editor = pref.edit();
//                        editor.putString("userAddress", userAdress);
//                        editor.commit();
//
//
//                    }
//                    if(pUserDataBean.getCity_id()!=null) {
//                     int cityId = Integer.parseInt(pUserDataBean.getCity_id());
//                   spinner.setSelection(cityId);
//
                    pbar.hide();
//                    }

                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(EditProfileActivity.this, getString(R.string.network__problems), Toast.LENGTH_SHORT).show();
            }
        });


    }

//    public void setViewsWithSavedData(){
//        SharedPreferences mPrefs = getPreferences(MODE_PRIVATE);
//        Gson gson = new Gson();
//        String json = mPrefs.getString("userModel", "");
//        UserModel userModel = gson.fromJson(json, UserModel.class);
//        String imagePath="http://pharmacyapi.codecaique.com/uploads/users/"+userModel.getImage();
//        String firstName=userModel.getFirst_name();
//        String lastName=userModel.getLast_name();
//
//        String email=userModel.getEmail();
//
//        String userPhone=userModel.getPhone();
//        String password=userModel.getPassword();
//        int city_id=Integer.parseInt(userModel.getCity_id());
//        String userAdress=userModel.getAddress();
//        String landMark=userModel.getLand_mark();
//        String car_id=userModel.getReality_pro();
//        String gender=userModel.getGender();
//
//        if(gender.equals("Male"))
//            radioButtonMale.setChecked(true);
//        else if(gender.equals("Female"))
//            radioButtonFemale.setChecked(true);
//
////        Picasso.with(EditProfileActivity.this).load(imagePath).into(image);
//        editTextfirst_name.setText(firstName);
//        editTextlast_name.setText(lastName);
//        editTextphone.setText(userPhone);
//        editTextemail.setText(email);
//        editTextpassword.setText(password);
//        spinner.setSelection(city_id);
//        editTextLandMark.setText(landMark);
//        editTexCarId.setText(car_id);
//        editTextAddress.setText(userAdress);
//
//    }




    public void chooseImage() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), 1);
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, 1);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
//            userDataBean.setImage(uri);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                image.setImageBitmap(bitmap);



                MultipartBody.Part body=null;
                if(uri!=null)
                {
                    Cursor cursor = null;
                    String p=null;
                    try {
                        String[] proj = { MediaStore.Images.Media.DATA };
                        cursor = getContentResolver().query(uri, proj, null, null, null);
                        assert cursor != null;
                        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        cursor.moveToFirst();
                        p= cursor.getString(column_index);
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                    File file1=new File(p);
                    RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), file1);
                    body= MultipartBody.Part.createFormData("image", file1.getName(), requestFile);

                    filesToUpload=(body);

                }




            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private boolean hasStoragePermission(int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    ||checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
                return false;
            } else {
                chooseImage();
                return true;
            }
        } else {
            chooseImage();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && (grantResults[0] == PackageManager.PERMISSION_GRANTED ||
                grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
            if (requestCode == 1)
                chooseImage();
            else
                chooseImage();
        }
    }

    private void showCities() {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<ShowCitiesResponse> call = projectAPIs.showCities();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<ShowCitiesResponse>() {
            @Override
            public void onResponse(Call<ShowCitiesResponse> call, Response<ShowCitiesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {
                    List<ShowCitiesResponse.UserDataBean> cities=response.body().getUser_data();
                    ArrayList<String> citiesEnglishNames=new ArrayList<String>();
                    for(int i=0;i<cities.size();i++)
                    {
                        citiesEnglishNames.add(cities.get(i).getE_name());
                    }

                    ArrayAdapter<String> citiesAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_item, citiesEnglishNames);

//                    spinner.setAdapter(citiesAdapter);

                }
                else {
                }
            }
            @Override
            public void onFailure(Call<ShowCitiesResponse> call, Throwable t) {
                pbar.hide();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

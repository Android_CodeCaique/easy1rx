package com.codecaique.easy1rx.ui.home_activity.orders_fragment;

/**
 * Created by I Love Allah on 06/10/2018.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.Models.responses.GetOredrIdResponse;
import com.codecaique.easy1rx.Models.responses.PatientOrdersResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class PatientOrdersRecyclerAdapter extends RecyclerView.Adapter<PatientOrdersRecyclerAdapter.ViewHolder> {

    List<PatientOrdersResponse.UserOrdersBean> data;

    Context mContext;
    CustomItemClickListener listener;
    private Dialog pbar;

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.orders_item, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        TextView textViewPrice = holder.textViewPrice;
        TextView textViewStatus = holder.textViewStaus;
        TextView textViewSortId = holder.textViewSortId;
        TextView textViewDescription=holder.textViewDescription;
//        ImageButton imageButtonoButtondelete=holder.imageButtonoButtondelete;
        ImageView imageView=holder.imageView;


        textViewStatus.setText(data.get(position).getState());
        if(data.get(position).getPrice()!=null)
        {textViewPrice.setText(data.get(position).getPrice()+mContext.getString(R.string.pound));

        }
        textViewSortId.setText((mContext.getString(R.string.order_number_hash)+data.get(position).getId()));
        if(data.get(position).getPro_description()!=null) {
            textViewDescription.setText("" + data.get(position).getPro_description());
        }
//        if(data.get(position).getState().equals("canceled"))
//        {
//            imageButtonoButtondelete.setVisibility(View.INVISIBLE);
//        }
//        else{
//            imageButtonoButtondelete.setVisibility(View.VISIBLE);
//
//        }
        Glide.with(mContext).load("http://pharmacyapi.codecaique.com/uploads/orders/"+data.get(position).getImage()).into(imageView);


//        imageButtonoButtondelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Toast.makeText(mContext, "Order Canceled", Toast.LENGTH_SHORT).show();
//
//                pbar = new Dialog(mContext);
//                pbar.setContentView(R.layout.prograss_dialog);
//                pbar.setTitle(mContext.getString(R.string.please_wait));
//                pbar.show();
//                CancelOrder(""+data.get(position).getId(),position);
//
//            }
//        });



//        String state=data.get(position).getState();
//        if(state.equals("accept"))
//        {
//            textViewStatus.setTextColor(R.color.colorAccent);
//        }
//        else if(state.equals("receive"))
//        {
//            textViewStatus.setTextColor(R.color.colorBlack);
//        }
//        else if(state.equals("refused"))
//        {
//            textViewStatus.setTextColor(R.color.colorGray);
//        }



    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public PatientOrdersRecyclerAdapter(Context mContext, List<PatientOrdersResponse.UserOrdersBean> data, CustomItemClickListener listener) {
        this.data = data;
        this.mContext = mContext;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewPrice;
        TextView textViewStaus;
        TextView textViewSortId;
        TextView textViewDescription;
//        ImageButton imageButtonoButtondelete;
        ImageView imageView;

        ViewHolder(View v) {
            super(v);
            textViewPrice = v
                    .findViewById(R.id.textViewprice);

            textViewStaus = v
                    .findViewById(R.id.textVieworderStatus);


            textViewSortId = v
                    .findViewById(R.id.orderId);
            textViewDescription= v
            .findViewById(R.id.textViewdescription);

//            imageButtonoButtondelete= v
//            .findViewById(R.id.delete);
            imageView= v
                    .findViewById(R.id.imageView);
        }


    }


    private void CancelOrder(String orderId, final int position) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<GetOredrIdResponse> call = projectAPIs.cancelOrder(orderId);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<GetOredrIdResponse>() {
            @Override
            public void onResponse(Call<GetOredrIdResponse> call, Response<GetOredrIdResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {
                    if(response.body().getMessage().equals("order canceled successfully")) {
                  Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        data.remove(position);
                        pbar.hide();
                        notifyDataSetChanged();
                    }


                }
            }
            @Override
            public void onFailure(Call<GetOredrIdResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }



}

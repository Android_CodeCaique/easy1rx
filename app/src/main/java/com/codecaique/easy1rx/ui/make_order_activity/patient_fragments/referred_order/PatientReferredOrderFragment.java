package com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.referred_order;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.Models.responses.CheckOrderResponse;
import com.codecaique.easy1rx.Models.responses.GetUserAddressesResponse;
import com.codecaique.easy1rx.Models.responses.Notification_order_response;
import com.codecaique.easy1rx.Models.responses.PatientMakeOrderResponse;
import com.codecaique.easy1rx.Models.responses.PatientOrdersResponse;
import com.codecaique.easy1rx.Models.responses.ShowCitiesResponse;
import com.codecaique.easy1rx.Models.rquests.PatentMakeOrderModel;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.BranchesMapActivity.BranchesMapActivity;
import com.codecaique.easy1rx.ui.MapToGetAddressActivity.MapToGetAddress;
import com.codecaique.easy1rx.ui.custom_photo_gallery_activity.CustomPhotoGalleryActivity;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.codecaique.easy1rx.ui.home_activity.settings_fragment.AdressesAdapterInSettings;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;


public class PatientReferredOrderFragment extends Fragment {
    private PatientReferredOrderFragment.OnFragmentInteractionListener listener;
    private ArrayList<Bitmap> arrayListimages;
    private RecyclerView recyclerView,recyclerViewAddresse;
    private RecyclerView.LayoutManager layoutManager;
    private ReferredOrderImagesRecyclerAdapter adapter;
    AdressesAdapterInSettings adapterAddresses;
    private PatentMakeOrderModel.order_data order_data;
    String deleveryState="0";
    String addressState="";
    int address_id;
    private final int PICK_IMAGE_MULTIPLE =1;
    private final int PICK_IMAGE_FROM_CAMERA=0;
    List<MultipartBody.Part> filesToUpload;
    List<RequestBody>filesNames;
    List<ShowCitiesResponse.UserDataBean> cities;
    EditText editTextPromoCode;
    CardView cardView;
    CircleImageView doctorImage;
    TextView doctorName;
    TextView textViewDeliveryAddress;

    //    EditText editTextAddress;
    ImageButton buttonBack;
    Button buttonNewAddress,buttonSelectAddress;
    public static Button buttonMakeOrder;
    RadioGroup radioGroup;
    Spinner spinner;
    public static EditText editTextNewAddress;

    public static String city_name = "" , lat = "", lng = "";

    EditText editTextLandMark;
    Button buttonOK;
    public static TextView textViewStoreName;
    public  static int branchId=0;
    private String CAPTURE_IMAGE_FILE_PROVIDER = "com.codecaique.easy1rx.fileprovider";

    //    TextView textViewDeliveryAddress;
    Dialog dialogOrderInserted,pbar,dialogSelectAddress,dialogAddNewAddress,dialogSelectImageType;
    TextView dialogOrderId;
    public  static  TextView textViewCity;
    TextView textViewAddress;
    TextView textViewLandMark;
    Dialog dialogNewAddress;
    ImageButton buttonOpenMap;
    ArrayList<String> images;
    List<GetUserAddressesResponse.DataBean> userAdressesDataBean;

    public static PatientReferredOrderFragment newInstance() {
        return new PatientReferredOrderFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 1);
        }
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view=inflater.inflate(R.layout.fragment_referred_order, container, false);
        buttonMakeOrder=(Button)view.findViewById(R.id.buttonMakeOrder);
        buttonNewAddress=(Button)view.findViewById(R.id.buttonAddNewAddress);
        buttonSelectAddress=(Button)view.findViewById(R.id.buttonSelectAddress);

        editTextPromoCode=(EditText)view.findViewById(R.id.editTextPromoCode);
        textViewCity=(TextView)view.findViewById(R.id.textViewCity);
        textViewAddress=(TextView)view.findViewById(R.id.textViewAddress);
        textViewLandMark=(TextView)view.findViewById(R.id.textViewLandMark);
        textViewDeliveryAddress=(TextView)view.findViewById(R.id.textViewDeliveyAddress);
        cardView=(CardView) view.findViewById(R.id.card_view);
        doctorImage=(CircleImageView)view.findViewById(R.id.profile_image);
        doctorName=(TextView)view.findViewById(R.id.username);
        textViewStoreName=(TextView)view.findViewById(R.id.storeName);

        buttonNewAddress.setVisibility(View.GONE);
        buttonSelectAddress.setVisibility(View.GONE);
        buttonMakeOrder.setVisibility(View.GONE);
        textViewDeliveryAddress.setVisibility(View.GONE);
        cardView.setVisibility(View.GONE);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new GridLayoutManager(getContext(),2);
//        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());



        radioGroup=(RadioGroup)view.findViewById(R.id.radiogroup);
        if(!textViewStoreName.getText().equals(""))

        {
            textViewStoreName.setVisibility(View.GONE);
        }


        String orderType=getActivity().getIntent().getExtras().getString("orderType");

        if(orderType.equals("reOrder"))
        {
            PatientOrdersResponse.UserOrdersBean orderResponse = (PatientOrdersResponse.UserOrdersBean) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
            TextView textView=view.findViewById(R.id.referredText);
            textView.setVisibility(View.GONE);
            doctorImage.setVisibility(View.GONE);
            doctorName.setVisibility(View.GONE);
            images=getActivity().getIntent().getExtras().getStringArrayList("images");

            adapter=new ReferredOrderImagesRecyclerAdapter(getContext(), images, new CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {

                }
            });


            recyclerView.setAdapter(adapter);


        }

        if(orderType.equals("reOrder2"))
        {
            Notification_order_response orderResponse = (Notification_order_response) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
            TextView textView=view.findViewById(R.id.referredText);
            textView.setVisibility(View.GONE);
            doctorImage.setVisibility(View.GONE);
            doctorName.setVisibility(View.GONE);
            images=getActivity().getIntent().getExtras().getStringArrayList("images");

            adapter=new ReferredOrderImagesRecyclerAdapter(getContext(), images, new CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {

                }
            });


            recyclerView.setAdapter(adapter);


        }

        if (orderType.equals("referred"))
        {
            CheckOrderResponse.DataBean orderResponse = (CheckOrderResponse.DataBean)
                    getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");

            TextView textView=view.findViewById(R.id.referredText);
            textView.setVisibility(View.GONE);
            doctorImage.setVisibility(View.GONE);
            doctorName.setVisibility(View.GONE);
//            images=getActivity().getIntent().getExtras().getStringArrayList("images");

            assert orderResponse != null;

            images = new ArrayList<>();

            for (int i = 0; i< orderResponse.getImages().size() ; i++)
            {
                images.add(orderResponse.getImages().get(i).getImage());
            }

            adapter=new ReferredOrderImagesRecyclerAdapter(getContext(), images, new CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {

                }
            });


            recyclerView.setAdapter(adapter);


        }

        else
//            {
//            CheckOrderResponse.DataBean orderResponse = (CheckOrderResponse.DataBean) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
//
//            if (orderResponse.getOrder().getDoctor_image() != null) {
//                Glide.with(getActivity()).load("http://pharmacyapi.codecaique.com/uploads/users/" + orderResponse.getOrder().getDoctor_image()).into(doctorImage);
//
//            } else {
//                Glide.with(getActivity()).load(R.drawable.image0).into(doctorImage);
//
//            }
//            String doctorFname = orderResponse.getOrder().getDoctor_first_name();
//            String doctorLname = orderResponse.getOrder().getDoctor_last_name();
//            doctorName.setText(getString(R.string.dr) + doctorFname + " " + doctorLname);
//            images = new ArrayList<String>();
//            for (int i = 0; i < orderResponse.getImages().size(); i++) {
//                images.add(orderResponse.getImages().get(i).getImage());
//            }
//        }

        adapter=new ReferredOrderImagesRecyclerAdapter(getContext(), images, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {

            }
        });

        recyclerView.setAdapter(adapter);





        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                boolean isChecked = checkedRadioButton.isChecked();
                if (isChecked)
                {
                    if(checkedRadioButton.getId()==R.id.radioButtonDelivery)
                    {
                        deleveryState="1";





                        buttonNewAddress.setVisibility(View.VISIBLE);
                        buttonSelectAddress.setVisibility(View.VISIBLE);
                        textViewStoreName.setVisibility(View.INVISIBLE);
                        buttonMakeOrder.setVisibility(View.GONE);
                    }
                    else {
                        buttonNewAddress.setVisibility(View.GONE);
                        buttonSelectAddress.setVisibility(View.GONE);
                        textViewDeliveryAddress.setVisibility(View.GONE);
                        cardView.setVisibility(View.GONE);

                        if(!textViewStoreName.getText().equals(""))
                        {
                            textViewStoreName.setVisibility(View.VISIBLE);
                            buttonMakeOrder.setVisibility(View.VISIBLE);
                            deleveryState="0";

                        }
                        else
                        {
                            Intent intent=new Intent(getContext(),BranchesMapActivity.class);
                            intent.putExtra("type","referred");
                            startActivity(intent);
                        }



                    }
                }
            }
        });


        dialogOrderInserted = new Dialog(getContext());
        dialogOrderInserted.setContentView(R.layout.dialog_orderi_nserted);
        dialogOrderId= dialogOrderInserted.findViewById(R.id.dialogOrderId);



        Button continueShopping = (Button) dialogOrderInserted.findViewById(R.id.buttonConitnueShopping);

        continueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOrderInserted.hide();
                Intent intent=new Intent(getContext(), HomeActivity.class);
                startActivity(intent);
            }
        });


        pbar = new Dialog(getContext(),R.style.DialogCustomTheme);
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);

        pbar.setContentView(R.layout.prograss_dialog);

        pbar.setTitle(getString(R.string.please_wait));
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));



        dialogSelectAddress = new Dialog(getContext());
        dialogSelectAddress.setContentView(R.layout.dialog_select_address);
        recyclerViewAddresse=(RecyclerView)dialogSelectAddress.findViewById(R.id.recyclerView);
        recyclerViewAddresse.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerViewAddresse.setLayoutManager(layoutManager);
        recyclerViewAddresse.setItemAnimator(new DefaultItemAnimator());
//        ArrayList<String> arrayList=new ArrayList<String>();
//        arrayList.add("23st, beba");
//
//        arrayList.add("24, new bns, beni_suif");
//        arrayList.add("655,wasta");
//        arrayList.add("33 st elzahra, fashn");
        userAdressesDataBean=new ArrayList<GetUserAddressesResponse.DataBean>();
        adapterAddresses = new AdressesAdapterInSettings(getContext(),userAdressesDataBean,new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                addressState="selected";
                address_id=userAdressesDataBean.get(position).getId();
                lat = userAdressesDataBean.get(position).getLat();
                lng = userAdressesDataBean.get(position).getLng();
                textViewDeliveryAddress.setVisibility(View.VISIBLE);
                cardView.setVisibility(View.VISIBLE);
                buttonMakeOrder.setVisibility(View.VISIBLE);


                textViewAddress.setText(userAdressesDataBean.get(position).getAddress());
                textViewCity.setText(userAdressesDataBean.get(position).getCity_arabicname());
                textViewLandMark.setText(userAdressesDataBean.get(position).getLand_mark());



                dialogSelectAddress.hide();
            }
        });
        recyclerViewAddresse.setAdapter(adapterAddresses);




        dialogNewAddress = new Dialog(getContext());
        dialogNewAddress.setContentView(R.layout.dialog_add_new_address);
//        spinner = new Spinner(getContext());
//        spinner =(Spinner)dialogNewAddress. findViewById(R.id.spinner1);
        editTextNewAddress=(EditText)dialogNewAddress.findViewById(R.id.editTextUserAddress);
        editTextLandMark=(EditText)dialogNewAddress.findViewById(R.id.editTextLandMark);
        buttonOK=(Button)dialogNewAddress.findViewById(R.id.buttonOK);
        buttonOpenMap =(ImageButton)dialogNewAddress.findViewById(R.id.buttonMap);
        buttonOpenMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getContext(), MapToGetAddress.class);
                intent.putExtra("type","referredOrder");
                startActivity(intent);
            }
        });
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String address=(editTextNewAddress.getText().toString());
//                String city=spinner.getSelectedItem().toString();
                String landMark=(editTextLandMark.getText().toString());

                textViewAddress.setText(address);
                textViewCity.setText(city_name);
                textViewLandMark.setText(landMark);

                if(address!=null) {
                    addressState="new";
                    textViewDeliveryAddress.setVisibility(View.VISIBLE);
                    cardView.setVisibility(View.VISIBLE);
                    buttonMakeOrder.setVisibility(View.VISIBLE);
                }


                dialogNewAddress.hide();
            }
        });






        buttonSelectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

                String userId=pref.getString("userId","");
                NetworkConnection connection = new NetworkConnection();

                if (!connection.isOnline()) {
                    pbar.show();
                    getUserAdresses(userId);

                } else {
                    Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();


                }
            }
        });

        buttonNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCities();
                dialogNewAddress.show();
            }
        });









        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                int position = viewHolder.getAdapterPosition();
                arrayListimages.remove(position);
                adapter.notifyDataSetChanged();

            }
        };


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


        filesToUpload=new ArrayList<MultipartBody.Part>();
        filesNames=new ArrayList<RequestBody>();




        buttonMakeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//        get cities for comparison
                NetworkConnection connection = new NetworkConnection();
                if (!connection.isOnline()) {
                    //Make order
                    pbar.show();

                    showCities();


                    SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    String userId = pref.getString("userId", "");

                    order_data = new PatentMakeOrderModel.order_data();
                    order_data.setOrder_id(Integer.parseInt(userId));
                    String promocode = editTextPromoCode.getText().toString();

                    String orderType=getActivity().getIntent().getExtras().getString("orderType");

                    if(radioGroup.getCheckedRadioButtonId()==R.id.radioButtonDelivery) {

                        if (addressState.equals("new")) {
                            String address = textViewAddress.getText().toString();
                            String landMark = textViewLandMark.getText().toString();
                            String city = textViewCity.getText().toString();
                            int city_id = 0;
//                            for (int i = 0; i < cities.size(); i++) {
//                                if (cities.get(i).getE_name().equals(city)) {
//                                    city_id = cities.get(i).getId();
//                                }
//                            }
                            assert orderType != null;
                            if(orderType.equals("reOrder"))
                            {
                                PatientOrdersResponse.UserOrdersBean orderResponse2 = (PatientOrdersResponse.UserOrdersBean) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
                                assert orderResponse2 != null;
                                int id=orderResponse2.getId();
                                makePatientReferredOrderNewAddress(id, promocode, deleveryState, address, city_name, landMark,"0");
                            }
                            else{
                                CheckOrderResponse.DataBean orderResponse = (CheckOrderResponse.DataBean) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
                                assert orderResponse != null;
                                int orderId = orderResponse.getOrder().getId();
                                makePatientReferredOrderNewAddress(orderId, promocode, deleveryState, address, city_name, landMark,"1");

                            }
                        } else if (addressState.equals("selected")) {
                            if(orderType.equals("reOrder")) {
                                PatientOrdersResponse.UserOrdersBean orderResponse2 = (PatientOrdersResponse.UserOrdersBean) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
                                int id=orderResponse2.getId();
                                makePatientReferredOrdeSelectAddress(id, promocode, deleveryState, address_id,"0");

                            }

                          else   if(orderType.equals("reOrder2")) {
                                Notification_order_response orderResponse = (Notification_order_response) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
                                int id=orderResponse.getData().get(0).getId();
                                makePatientReferredOrdeSelectAddress(id, promocode, deleveryState, address_id,"0");

                            }

                            else {
                                CheckOrderResponse.DataBean orderResponse = (CheckOrderResponse.DataBean) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
                                int orderId = orderResponse.getOrder().getId();
                                makePatientReferredOrdeSelectAddress(orderId, promocode, deleveryState, address_id,"1");
                            }
                        }
                    }
                    else if(radioGroup.getCheckedRadioButtonId()==R.id.radioButtonPicKupFromStore) {
                        if(orderType.equals("reOrder")) {
                            PatientOrdersResponse.UserOrdersBean orderResponse2 = (PatientOrdersResponse.UserOrdersBean) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
                            int id=orderResponse2.getId();
                            makePateintOrderPickFromStore(id,promocode,deleveryState,"0");

                        }
                       else if(orderType.equals("reOrder2")) {

                            Notification_order_response orderResponse = (Notification_order_response) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
                            int id=orderResponse.getData().get(0).getId();

                            makePateintOrderPickFromStore(id,promocode,deleveryState,"0");

                        }
                        else {

                            CheckOrderResponse.DataBean orderResponse = (CheckOrderResponse.DataBean) getActivity().getIntent().getBundleExtra("data2").getSerializable("my object2");
                            int orderId = orderResponse.getOrder().getId();
                            makePateintOrderPickFromStore(orderId, promocode, deleveryState,"1");
                        }
                    }
                }
                else {
                    Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();


                }



            }
        });






        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }




    public void chooseImage() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);

//        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        startActivityForResult(pickPhoto, 1);
//


//Intent intent=new Intent(getContext(), SelectImagesActivity.class);
//startActivity(intent);


        dialogSelectImageType = new Dialog(getContext());
        dialogSelectImageType.setContentView(R.layout.dialog_image_type);

        ImageButton camera=(ImageButton) dialogSelectImageType.findViewById(R.id.buttonCamera);
        ImageButton gallery=(ImageButton) dialogSelectImageType.findViewById(R.id.buttonGallery);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectImageType.hide();
//                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takePicture, PICK_IMAGE_FROM_CAMERA);


                //                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                int imageId=pref.getInt("image",0);


                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
                if (!path.exists()) path.mkdirs();
                File image = new File(path, "image_capture_"+""+imageId+".jpg");
                Uri imageUri = FileProvider.getUriForFile(getActivity().getApplicationContext(), CAPTURE_IMAGE_FILE_PROVIDER, image);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                providePermissionForProvider(cameraIntent, imageUri);
                startActivityForResult(cameraIntent, PICK_IMAGE_FROM_CAMERA);
//                }
//                else {
//                    //if permission is not provided
//                    new  PermissionsMarshmallow(getActivity());
//                }

            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectImageType.hide();
                Intent intent = new Intent(getActivity(), CustomPhotoGalleryActivity.class);
                startActivityForResult(intent,PICK_IMAGE_MULTIPLE);

            }
        });

        dialogSelectImageType.show();



    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
//
//            Uri uri = data.getData();
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                // Log.d(TAG, String.valueOf(bitmap));
//
//                arrayListimages.add(bitmap);
//
//
//                adapter.notifyDataSetChanged();
//
//
//
//
//                MultipartBody.Part body=null;
//                if(uri!=null)
//                {
////                    String p=null;
////                    Cursor cursor=getActivity().getContentResolver().query(uri,null,null,null,null);
////                if(cursor==null)
////                {
////                    p=uri.getPath().toString();
////                }
////                else {
////                    cursor.moveToFirst();
////                   int indx= cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//////                     int indx= 6;
////
////                    Log.e("index = ", "index = "+indx);
////                   // int indx= cursor.getColumnIndex(cursor.getColumnName(0));
////
////                    p=cursor.getString(indx);
////                }
//
//
//                    Cursor cursor = null;
//                    String p=null;
//                    try {
//                        String[] proj = { MediaStore.Images.Media.DATA };
//                        cursor = getActivity().getContentResolver().query(uri, proj, null, null, null);
//                        assert cursor != null;
//                        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                        cursor.moveToFirst();
//                        p= cursor.getString(column_index);
//                    } finally {
//                        if (cursor != null) {
//                            cursor.close();
//                        }
//                    }
//
//                    File file1=new File(p);
//                    RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), file1);
//                    body= MultipartBody.Part.createFormData("image[]", file1.getName(), requestFile);
//
//                    filesToUpload.add(body);
//
//                }
//
//
//
//
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if(requestCode == PICK_IMAGE_FROM_CAMERA)
            {
//                Bitmap photo = (Bitmap) data.getExtras().get("data");
//                Uri tempUri = getImageUri(getContext(), photo);
//                File file1= new File(getRealPathFromURI(tempUri));
//
//
//                arrayListimages.add(photo);
//                adapter.notifyDataSetChanged();
//
//                RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), file1);
//                MultipartBody.Part body= MultipartBody.Part.createFormData("image[]", file1.getName(), requestFile);
//
//                filesToUpload.add(body);
//                Log.e("camera", "camera: "+data.getExtras().get("data"));



                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                int imageId=pref.getInt("image",0);

                //get the file from the uri path  , it is the same declared in the path file in xml folder. in this case it is Gallery/MyImages/
                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
                if (!path.exists()) path.mkdirs();
                File imageFile = new File(path, "image_capture_"+""+imageId+".jpg");

                Uri stringUri = Uri.fromFile(imageFile);

                try {
                    Bitmap bitmapFromMedia = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), stringUri);
                    arrayListimages.add(bitmapFromMedia);
                    adapter.notifyDataSetChanged();

                    editor.putInt("image", imageId+1);
                    editor.commit();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), imageFile);
                MultipartBody.Part body= MultipartBody.Part.createFormData("image[]", imageFile.getName(), requestFile);

                filesToUpload.add(body);


            }
            if(requestCode == PICK_IMAGE_MULTIPLE){
                String[] imagesPath = data.getStringExtra("data").split("\\|");

                for (int i=0;i<imagesPath.length;i++){
                    Log.e("onActivityResult", "onActivityResult: "+imagesPath[i]);

                    Bitmap bitmap = BitmapFactory.decodeFile(imagesPath[i]);
                    arrayListimages.add(bitmap);


                    File file1=new File(imagesPath[i]);
                    RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), file1);
                    MultipartBody.Part body= MultipartBody.Part.createFormData("image[]", file1.getName(), requestFile);

                    filesToUpload.add(body);

                }

                adapter.notifyDataSetChanged();
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }



    private void makePateintOrderPickFromStore(final int order_id, String promo_code, String delivery,String referred) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */

        Log.e("Branch : ", branchId + "   v ");

        Call<PatientMakeOrderResponse> call = projectAPIs.makepatientReferredOrderPickFromStore(
                order_id,
                RequestBody.create(MediaType.parse("multipart/form-data"),promo_code),
                branchId,
                RequestBody.create(MediaType.parse("multipart/form-data"),delivery),
                RequestBody.create(MediaType.parse("multipart/form-data"),referred),
                RequestBody.create(MediaType.parse("multipart/form-data"),lng),
                RequestBody.create(MediaType.parse("multipart/form-data"),lat)
                );
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<PatientMakeOrderResponse>() {
            @Override
            public void onResponse(Call<PatientMakeOrderResponse> call, Response<PatientMakeOrderResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                Log.e("inner= ", response.message().toString());

                if(response.isSuccessful()) {






                    if (response.body().getMessage().contains("order inserted success") ||response.body().getMessage().contains("تم ادخال الطلب بنجاح")) {
                        pbar.hide();


                        int selectedId= radioGroup.getCheckedRadioButtonId();
                        if(selectedId==R.id.radioButtonPicKupFromStore)
                        {

                            dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                            dialogOrderInserted.show();
                          // Toast.makeText(getContext(), ""+branchId, Toast.LENGTH_SHORT).show();


                        }
                        else if(selectedId==R.id.radioButtonDelivery) {

                            dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                            dialogOrderInserted.show();
                        }

                    } else {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        pbar.hide();

                    }
                }
            }
            @Override
            public void onFailure(Call<PatientMakeOrderResponse> call, Throwable t) {
                pbar.hide();

                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void makePatientReferredOrderNewAddress(final int order_id, String promo_code, String delivery, String address,String city_id,String land_mark
            ,String referred) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */

        Call<PatientMakeOrderResponse> call = projectAPIs.makePatientReferredOrderNewAddress(
                order_id,
                RequestBody.create(MediaType.parse("multipart/form-data"),promo_code),
                RequestBody.create(MediaType.parse("multipart/form-data"),delivery),
                RequestBody.create(MediaType.parse("multipart/form-data"),address),
                RequestBody.create(MediaType.parse("multipart/form-data"),city_id),
                RequestBody.create(MediaType.parse("multipart/form-data"),land_mark),
                RequestBody.create(MediaType.parse("multipart/form-data"),referred),
                RequestBody.create(MediaType.parse("multipart/form-data"),lng),
                RequestBody.create(MediaType.parse("multipart/form-data"),lat)
                );
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<PatientMakeOrderResponse>() {
            @Override
            public void onResponse(Call<PatientMakeOrderResponse> call, Response<PatientMakeOrderResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                Log.e("inner= ", response.message().toString());

                if(response.isSuccessful()) {


                    if (response.body().getMessage().contains("order inserted success") || response.body().getMessage().contains("تم ادخال الطلب بنجاح")) {
                        pbar.hide();

                        int selectedId= radioGroup.getCheckedRadioButtonId();
                        if(selectedId==R.id.radioButtonPicKupFromStore)
                        {

                            dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                            dialogOrderInserted.show();
//                            Uri gmmIntentUri = Uri.parse("https://www.google.co.in/maps/dir/18.6121132,73.707989/18.5,73.7/18.8,73.71/18.9,73.75");
//                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                            mapIntent.setPackage("com.google.android.apps.maps");
//                            startActivity(mapIntent);

//                            Intent intent=new Intent(getContext(), BranchesMapActivity.class);
//                            startActivity(intent);
                        }
                        else if(selectedId==R.id.radioButtonDelivery) {

                            dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                            dialogOrderInserted.show();
                        }

                    } else {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        pbar.hide();

                    }
                }
            }
            @Override
            public void onFailure(Call<PatientMakeOrderResponse> call, Throwable t) {
                pbar.hide();

                Toast.makeText(getContext(), getString(R.string.failure)+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void makePatientReferredOrdeSelectAddress(final int order_id, String promo_code, String delivery, int address_id
            ,String refferred) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */

        Call<PatientMakeOrderResponse> call = projectAPIs.makePatientReferredOrderSelectedAddress(
                order_id,
                RequestBody.create(MediaType.parse("multipart/form-data"),promo_code),
                RequestBody.create(MediaType.parse("multipart/form-data"),delivery),
                address_id,
                RequestBody.create(MediaType.parse("multipart/form-data"),refferred),
                RequestBody.create(MediaType.parse("multipart/form-data"),lng),
                RequestBody.create(MediaType.parse("multipart/form-data"),lat)
                );
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<PatientMakeOrderResponse>() {
            @Override
            public void onResponse(Call<PatientMakeOrderResponse> call, Response<PatientMakeOrderResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                Log.e("inner= ", response.message().toString());

                if(response.isSuccessful()) {






                    if (response.body().getMessage().contains("order inserted success") ||response.body().getMessage().contains("تم ادخال الطلب بنجاح")) {
                        pbar.hide();



                        int selectedId= radioGroup.getCheckedRadioButtonId();
                        if(selectedId==R.id.radioButtonPicKupFromStore)
                        {

                            pbar.hide();


                            filesToUpload.clear();
                            arrayListimages.clear();

                            adapter.notifyDataSetChanged();
                            dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                            dialogOrderInserted.show();
//                            Uri gmmIntentUri = Uri.parse("https://www.google.co.in/maps/dir/18.6121132,73.707989/18.5,73.7/18.8,73.71/18.9,73.75");
//                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                            mapIntent.setPackage("com.google.android.apps.maps");
//                            startActivity(mapIntent);

//                            Intent intent=new Intent(getContext(), BranchesMapActivity.class);
//                            startActivity(intent);
                        }
                        else if(selectedId==R.id.radioButtonDelivery) {

                            dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                            dialogOrderInserted.show();
                        }

                    } else {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        pbar.hide();

                    }
                }
            }
            @Override
            public void onFailure(Call<PatientMakeOrderResponse> call, Throwable t) {
                pbar.hide();

                Toast.makeText(getContext(), getString(R.string.failure)+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }




    private void showCities() {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<ShowCitiesResponse> call = projectAPIs.showCities();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<ShowCitiesResponse>() {
            @Override
            public void onResponse(Call<ShowCitiesResponse> call, Response<ShowCitiesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {
                    assert response.body() != null;
                    cities=response.body().getUser_data();
                    ArrayList<String> citiesEnglishNames=new ArrayList<String>();
                    for(int i=0;i<cities.size();i++)
                    {
                        citiesEnglishNames.add(cities.get(i).getE_name());
                    }

                    ArrayAdapter<String> citiesAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(),R.layout.spinner_item, citiesEnglishNames);

//                    spinner.setAdapter(citiesAdapter);

                }
                else {
                }
            }
            @Override
            public void onFailure(Call<ShowCitiesResponse> call, Throwable t) {
//                pbar.hide();
            }
        });
    }


    private void getUserAdresses(String userId) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<GetUserAddressesResponse> call = projectAPIs.getUserAddresses(userId);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<GetUserAddressesResponse>() {
            @Override
            public void onResponse(Call<GetUserAddressesResponse> call, Response<GetUserAddressesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                    if (response.body().getMessage().equals("show address data")) {


                        userAdressesDataBean=new ArrayList<GetUserAddressesResponse.DataBean>();
                        userAdressesDataBean=response.body().getData();
                        adapterAddresses = new AdressesAdapterInSettings(getContext(),userAdressesDataBean,new CustomItemClickListener() {
                            @Override
                            public void onItemClick(View v, int position) {
                                addressState="selected";
                                address_id=userAdressesDataBean.get(position).getId();
                                textViewDeliveryAddress.setVisibility(View.VISIBLE);
                                cardView.setVisibility(View.VISIBLE);
                                buttonMakeOrder.setVisibility(View.VISIBLE);


                                textViewAddress.setText(userAdressesDataBean.get(position).getAddress());
                                textViewCity.setText(userAdressesDataBean.get(position).getCity_arabicname());
                                textViewLandMark.setText(userAdressesDataBean.get(position).getLand_mark());



                                dialogSelectAddress.hide();
                            }
                        });

                        recyclerViewAddresse.setAdapter(adapterAddresses);

                        adapterAddresses.notifyDataSetChanged();
                        pbar.hide();
                        dialogSelectAddress.show();

                    }


                    else {
                        pbar.hide();
                        Toast.makeText(getContext(), getString(R.string.you_dont_have_addresses), Toast.LENGTH_SHORT).show();
                    }
            }

            @Override
            public void onFailure(Call<GetUserAddressesResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(getContext(), t.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void providePermissionForProvider(Intent intent, Uri uri) {
        List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            getActivity().grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }

    public interface OnFragmentInteractionListener {
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

package com.codecaique.easy1rx.ui;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.widget.Toast;

import com.codecaique.easy1rx.R;

import java.util.List;
import java.util.Locale;

public class GetAddressTask extends AsyncTask<Location, Void, String> {
    Context localContext;
    public GetAddressTask(Context context) {
        super();
        localContext = context;
    }

    @Override
    protected String doInBackground(Location... params) {
        Geocoder geocoder = new Geocoder(localContext, Locale.getDefault());
        Location location = params[0];
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1);
        } catch (Exception exception) {
            return "Error Message";
        }
        if (addresses != null && addresses.size() > 0) {
            Address address = addresses.get(0);
            String addressText = ((address.getMaxAddressLineIndex() > 0) ?
                    address.getAddressLine(0) : "") +  ", " +
                    address.getLocality() + ", " +
                    address.getCountryName();
            return addressText;
        } else {
            return "no_address_found";
        }
    }

    @Override
    protected void onPostExecute(String address) {
        Toast.makeText(localContext, address, Toast.LENGTH_SHORT).show();    }
}


package com.codecaique.easy1rx.ui.home_activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;

import com.codecaique.easy1rx.LanguageHelper;
import com.codecaique.easy1rx.ui.signin_activity.SignInActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.CheckOrderResponse;
import com.codecaique.easy1rx.ui.make_order_activity.MakeOrderActivity;
import com.codecaique.easy1rx.Models.responses.GetOredrIdResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.ui.home_activity.notifications_fragment.NotificationsFragment;
import com.codecaique.easy1rx.ui.home_activity.orders_fragment.OrdersFragment;
import com.codecaique.easy1rx.ui.home_activity.settings_fragment.SettingsFragment;
import com.codecaique.easy1rx.ui.home_activity.offers_fragment.offersFragment;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HomeActivity extends AppCompatActivity implements offersFragment.OnFragmentInteractionListener,NotificationsFragment.OnFragmentInteractionListener,SettingsFragment.OnFragmentInteractionListener,OrdersFragment.OnFragmentInteractionListener {
    //ImageView imageViewOffers,imageViewOrders;
//FrameLayout frameLayout;
    Dialog orderTypeDailog,pbar;
    Button neworder;
    Button refferedorder;
    Button buttonOK;
    EditText editTextOrderId;
    ImageView gifImg;
    private BottomNavigationView bottomNavigationView;
    private FloatingActionButton buttonMakeOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_home);

//        imageViewOffers=(ImageView)findViewById(R.id.imageoffers);
//        imageViewOrders=(ImageView)findViewById(R.id.imageorders);
//        frameLayout=(FrameLayout) findViewById(R.id.frameHome);
//
//        imageViewOffers.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                frameLayout.removeAllViews();
//                imageViewOffers.setImageResource(R.drawable.offers2);
//                offersFragment fragment = new offersFragment();
//                getSupportFragmentManager().beginTransaction().add(R.id.frameHome, fragment).commit();
//            }
//        });
//imageViewOrders.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View v) {
//        frameLayout.removeAllViews();
//        OrdersFragment fragment = new OrdersFragment();
//        getSupportFragmentManager().beginTransaction().add(R.id.frameHome, fragment).commit();
//
//    }
//});


        orderTypeDailog = new Dialog(HomeActivity.this);
        orderTypeDailog.setContentView(R.layout.dialog_order_type);
        orderTypeDailog.setTitle(getString(R.string.order_type));
        neworder=orderTypeDailog.findViewById(R.id.buttonNewOrder);
        refferedorder=orderTypeDailog.findViewById(R.id.buttonRefferredOrder);
        buttonOK=orderTypeDailog.findViewById(R.id.buttonOK);
        editTextOrderId =orderTypeDailog.findViewById(R.id.editTextOrderId);
        buttonOK.setVisibility(View.INVISIBLE);
        editTextOrderId.setVisibility(View.INVISIBLE);


        neworder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderTypeDailog.hide();
                Intent intent=new Intent(HomeActivity.this, MakeOrderActivity.class);
                intent.putExtra("orderType","new");
                startActivity(intent);

            }
        });
        refferedorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextOrderId.setVisibility(View.VISIBLE);
                buttonOK.setVisibility(View.VISIBLE);

            }
        });
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String orderId=editTextOrderId.getText().toString();
                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                String userId=pref.getString("userId","");


                pbar = new Dialog(HomeActivity.this);
                pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
                pbar.setContentView(R.layout.prograss_dialog);
                Window window = pbar.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                gifImg = pbar.findViewById(R.id.gifImage);
//                Glide.with(HomeActivity.this).load(R.drawable.ripple).into(gifImg);
                pbar.setTitle(getString(R.string.please_wait));
                pbar.show();
                CheckReferredOrder(userId,orderId);

            }
        });



        buttonMakeOrder= findViewById(R.id.buttonMakeOrder);
        buttonMakeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                boolean signrState=pref.getBoolean("SignState",false);

                if(signrState==true) {
                    String userstate = pref.getString("userState", "");
                    if (userstate.equals("user")) {
                        orderTypeDailog.show();
                    } else {
                        Intent intent = new Intent(HomeActivity.this, MakeOrderActivity.class);
                        startActivity(intent);
                    }
                }else {

                    Intent intent=new Intent(HomeActivity.this, SignInActivity.class);
                    startActivity(intent);
                    Toast.makeText(HomeActivity.this, R.string.make_login, Toast.LENGTH_SHORT).show();
                }





            }
        });


        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout,offersFragment.newInstance());
        fragmentTransaction.commit();



        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()) {
                    case R.id.offers:
                        fragment = offersFragment.newInstance();
                        break;
                    case R.id.orders:
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                        boolean signrState=pref.getBoolean("SignState",false);
                        if (signrState==true) {
                            fragment = OrdersFragment.newInstance();
                        }else {
                            Intent intent=new Intent(HomeActivity.this, SignInActivity.class);
                            startActivity(intent);
                            Toast.makeText(HomeActivity.this, R.string.make_login, Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case R.id.notifications:
                        fragment = NotificationsFragment.newInstance();
                        break;
                    case R.id.settings:
                        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                        boolean signrState1=pref1.getBoolean("SignState",false);
                        if (signrState1==true) {
                            fragment = SettingsFragment.newInstance();
                        }else {

                            Intent intent=new Intent(HomeActivity.this, SignInActivity.class);
                            startActivity(intent);
                            Toast.makeText(HomeActivity.this, R.string.make_login, Toast.LENGTH_SHORT).show();
                        }
                        break;
					/*case R.id.action_four:
						fragment = FourFragment.newInstance();
						break;*/
                }
                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, fragment);
                    fragmentTransaction.commit();
                }
                return true;
            }
        });

    }

    private void getUserOrder(String userId) {
        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<GetOredrIdResponse> call = projectAPIs.getOrderId(userId);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<GetOredrIdResponse>() {
            @Override
            public void onResponse(Call<GetOredrIdResponse> call, Response<GetOredrIdResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {

                    buttonMakeOrder.setEnabled(true);

                    GetOredrIdResponse getOredrIdResponse = response.body();

                    String orderId=""+getOredrIdResponse.getOrder_data().getId();

                    Intent intent=new Intent(HomeActivity.this,MakeOrderActivity.class);
                    intent.putExtra("id",orderId);
                    startActivity(intent);

                }
            }
            @Override
            public void onFailure(Call<GetOredrIdResponse> call, Throwable t) {
            }
        });
    }



    private void CheckReferredOrder(String user_id,String order_id) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<CheckOrderResponse> call = projectAPIs.checkReferredOrder(user_id,order_id);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<CheckOrderResponse>() {
            @Override
            public void onResponse(Call<CheckOrderResponse> call, Response<CheckOrderResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                    if(response.body().getMessage().equals("show order data") || response.body().getMessage().equals("بيانات الاوردر")) {
//                        Toast.makeText(HomeActivity.this, "order found", Toast.LENGTH_SHORT).show();
                        CheckOrderResponse.DataBean orderResponse=response.body().getData();
                        orderTypeDailog.hide();
                        Intent intent=new Intent(HomeActivity.this,MakeOrderActivity.class);
                        intent.putExtra("orderType","referred");
                        Bundle bundle=new Bundle();
                        bundle.putSerializable("my object2",orderResponse);
                        intent.putExtra("data2",bundle);
                        pbar.hide();
                        startActivity(intent);

                    }
                    else{

                        pbar.hide();
                        Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
            }
            @Override
            public void onFailure(Call<CheckOrderResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(HomeActivity.this,getString(R.string.network__problems), Toast.LENGTH_SHORT).show();
            }
        });
    }




    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    // Before 2.0
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }




    public void loadLocale() {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        changeLang(language);
    }

    public  void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        LanguageHelper languageHelper = new LanguageHelper();

        languageHelper.ChangeLang(this.getResources(),lang);
        saveLocale(lang);

    }

    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

package com.codecaique.easy1rx.ui.offer_details_activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Paint;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.Models.responses.ClinicResponse;
import com.codecaique.easy1rx.Models.responses.Notification_offer_response;
import com.codecaique.easy1rx.Models.responses.OrderImagesResponse;
import com.codecaique.easy1rx.Models.responses.PharmacyResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OfferDetailsActivity extends AppCompatActivity {
    ImageView imageView;
    TextView textViewTitle;
    TextView textViewPriceBefore;
    TextView textViewPriceAfter;
    TextView textViewState;
    TextView textViewTSortId;
    ImageButton buttonBack;

    private Dialog pbar;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);
        imageView=(ImageView)findViewById(R.id.image);
        textViewTitle=(TextView)findViewById(R.id.Title);
        textViewPriceBefore=(TextView)findViewById(R.id.priceBefore);
        textViewPriceAfter=(TextView)findViewById(R.id.priceAfter);
//        textViewState =(TextView)findViewById(R.id.textViewState);
        textViewTSortId =(TextView)findViewById(R.id.sortId);
        buttonBack=(ImageButton)findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();

        if (bundle != null)
        {

            pbar = new Dialog(this);
            pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pbar.setContentView(R.layout.prograss_dialog);
            Window window = pbar.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            pbar.show();

            String from = bundle.getString("from","");
            if(from.equals("notification"))
            {


                Log.e("Notif","From");
                String id=getIntent().getExtras().getString("id");

                Log.e("Notif offer","From id : "+ id);

                getOffer(id);

            }
            else {
                Log.e("ELSE","From");

                String type = getIntent().getExtras().getString("type");
                if(type.equals("clinic"))
                {
                    ClinicResponse.OffersDataBean offersBean = (ClinicResponse.OffersDataBean) getIntent().getBundleExtra("data").getSerializable("my object");

                    Log.e("Notif offer","From id : "+ offersBean.getId());


                    Glide.with(OfferDetailsActivity.this).load("http://pharmacyapi.codecaique.com/uploads/offers/" + offersBean.getImage()).into(imageView);

                    SharedPreferences pref = this.getSharedPreferences("CommonPrefs", 0); // 0 - for private mode

                    String lang = pref.getString("Language","");


                    assert lang != null;
                    if (lang.equals("ar"))
                    {
                        textViewTitle.setText(offersBean.getArabic_name());
                    }
                    else
                        textViewTitle.setText(offersBean.getEnglish_name());

//                textViewTitle.setText(offersBean.getEnglish_name());
                    textViewPriceAfter.setText(offersBean.getOffer_price() + " ");
                    textViewPriceBefore.setText(offersBean.getPrice_before() + " ");
                    textViewPriceBefore.setPaintFlags(textViewPriceBefore.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        textViewState.setText(offersBean.getType());
                    textViewTSortId.setText(getString(R.string.sort_id) + offersBean.getId());

                    pbar.hide();
                    pbar.dismiss();
                }

                else
                {
                    PharmacyResponse.OffersDataBean offersBean = (PharmacyResponse.OffersDataBean) getIntent().getBundleExtra("data").getSerializable("my object");

                    Log.e("Notif offer","From id : "+ offersBean.getId());


                    Glide.with(OfferDetailsActivity.this).load("http://pharmacyapi.codecaique.com/uploads/offers/" + offersBean.getImage()).into(imageView);

                    SharedPreferences pref = this.getSharedPreferences("CommonPrefs", 0); // 0 - for private mode

                    String lang = pref.getString("Language","");


                    assert lang != null;
                    if (lang.equals("ar"))
                    {
                        textViewTitle.setText(offersBean.getArabic_name());
                    }
                    else
                        textViewTitle.setText(offersBean.getEnglish_name());

//                textViewTitle.setText(offersBean.getEnglish_name());
                    textViewPriceAfter.setText(offersBean.getOffer_price() + " ");
                    textViewPriceBefore.setText(offersBean.getPrice_before() + " ");
                    textViewPriceBefore.setPaintFlags(textViewPriceBefore.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        textViewState.setText(offersBean.getType());
                    textViewTSortId.setText(getString(R.string.sort_id) + offersBean.getId());

                    pbar.hide();
                    pbar.dismiss();

                }

            }
        }
        else
        {
            Log.e("ELSE bundle","NUll");
        }

    }
    private void getOffer(String offerId) {

        Log.e("ID sasa" , offerId + "  ");

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<Notification_offer_response> call = projectAPIs.showNotificaionOfOffer(offerId,"offer");
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<Notification_offer_response>() {
            @Override
            public void onResponse(Call<Notification_offer_response> call, Response<Notification_offer_response> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.body().getMessage().equals("show successfully")) {
                    List<Notification_offer_response.DataBean> dataBean=response.body().getData();

                    Log.e("SIZE  sada" , dataBean.size() +  "        ");

                    if (response.body().getData() != null && response.body().getData().size() >0)
                    {
                        Glide.with(OfferDetailsActivity.this).load("http://pharmacyapi.codecaique.com/uploads/offers/" + response.body().getData().get(0).getImage()).into(imageView);
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("CommonPrefs", 0); // 0 - for private mode

                        String lang = pref.getString("Language","");
                        assert lang != null;
                        if (lang.equals("ar"))
                        {
                            textViewTitle.setText(dataBean.get(0).getA_name());
                        }
                        else
                            textViewTitle.setText(dataBean.get(0).getE_name());

                        textViewPriceAfter.setText(dataBean.get(0).getOffer_price() + " ");
                        textViewPriceBefore.setText(dataBean.get(0).getPrice_before() + " ");
                        textViewPriceBefore.setPaintFlags(textViewPriceBefore.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        textViewState.setText(offersBean.getType());
                        textViewTSortId.setText(getString(R.string.sort_id) + dataBean.get(0).getId());
                    }

                    pbar.hide();
                    pbar.dismiss();

                }
            }
            @Override
            public void onFailure(Call<Notification_offer_response> call, Throwable t) {
                pbar.hide();
                pbar.dismiss();

//                pbar.hide();
                Toast.makeText(OfferDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

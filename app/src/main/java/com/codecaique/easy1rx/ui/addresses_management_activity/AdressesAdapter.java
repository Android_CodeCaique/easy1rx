package com.codecaique.easy1rx.ui.addresses_management_activity;

/**
 * Created by I Love Allah on 06/10/2018.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.DeleteAddressResponse;
import com.codecaique.easy1rx.Models.responses.GetUserAddressesResponse;
import com.codecaique.easy1rx.Models.rquests.AddAddressModel;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.CustomItemClickListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class AdressesAdapter extends RecyclerView.Adapter<AdressesAdapter.ViewHolder> {

    List<GetUserAddressesResponse.DataBean> data;
    Context mContext;
    CustomItemClickListener listener;
    Dialog pbar;

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.addresses_item, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        TextView textViewAddress=holder.textViewAddress;
        TextView textViewCity=holder.textViewcity;
        TextView textViewLandMark=holder.textViewLandmark;
        ImageButton buttonDelete=holder.buttonDelete;
        ImageButton buttonUpdate=holder.buttonUpdate;


        textViewAddress.setText(data.get(position).getAddress());
        textViewCity.setText(data.get(position).getCity_arabicname());
        textViewLandMark.setText(data.get(position).getLand_mark());
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pbar = new Dialog(mContext);
                pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
                pbar.setContentView(R.layout.prograss_dialog);
                Window window = pbar.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                pbar.setTitle(mContext.getString(R.string.please_wait));
                pbar.show();




                String id=""+data.get(position).getId();
                deleteAddress(id,position);
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAddress(position);
            }
        });


        //        if(data.get(position).getState().equals("canceled"))
//        {
//            imageButtonoButtondelete.setVisibility(View.INVISIBLE);
//        }
//        else{
//            imageButtonoButtondelete.setVisibility(View.VISIBLE);
//
//        }




//        String state=data.get(position).getState();
//        if(state.equals("accept"))
//        {
//            textViewStatus.setTextColor(R.color.colorAccent);
//        }
//        else if(state.equals("receive"))
//        {
//            textViewStatus.setTextColor(R.color.colorBlack);
//        }
//        else if(state.equals("refused"))
//        {
//            textViewStatus.setTextColor(R.color.colorGray);
//        }



    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public AdressesAdapter(Context mContext, List<GetUserAddressesResponse.DataBean> data, CustomItemClickListener listener) {
        this.data = data;
        this.mContext = mContext;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewAddress;
        TextView textViewcity;
        TextView textViewLandmark;
        ImageButton buttonDelete;
        ImageButton buttonUpdate;

        ViewHolder(View v) {
            super(v);

            textViewAddress =(TextView)v
            .findViewById(R.id.textViewAddress);

            textViewcity =(TextView)v
                    .findViewById(R.id.textViewCity);
            textViewLandmark =(TextView)v
                    .findViewById(R.id.textViewLandMark);
            buttonDelete=(ImageButton)v
                    .findViewById(R.id.buttonDeleteAddress);
            buttonUpdate=(ImageButton)v
                    .findViewById(R.id.buttonUpdateAddress);


        }


    }





    public void deleteAddress(String id, final int itemPosition) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<DeleteAddressResponse> call = projectAPIs.deleteAddress(id);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<DeleteAddressResponse>() {
            @Override
            public void onResponse(Call<DeleteAddressResponse> call, Response<DeleteAddressResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {
                    if(response.body().getMessage().equals("delete success")) {
                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        data.remove(itemPosition);
                        notifyDataSetChanged();
                        pbar.hide();
                    }


                }
            }
            @Override
            public void onFailure(Call<DeleteAddressResponse> call, Throwable t) {
            }
        });
    }

    public void updateAddress(int position)
    {
        AdressesActivity.itemPosition=position;

        GetUserAddressesResponse.DataBean addressDataBean= data.get(position);
        AdressesActivity.editTextNewAddressUpdate.setText(addressDataBean.getAddress());
        String cityString=addressDataBean.getCity_arabicname();
//        for(int i=0;i<AdressesActivity.spinnerUpdate.getAdapter().getCount();i++)
//        {
//            if(cityString.equals(AdressesActivity.spinnerUpdate.getAdapter().getItem(i)))
//            {
//                AdressesActivity.spinnerUpdate.setSelection(i);
//                break;
//            }
//        }
        AdressesActivity.editTextLandMarkUpdate.setText(addressDataBean.getLand_mark());
        AdressesActivity.dialogUpdateAddress.show();

    }

}

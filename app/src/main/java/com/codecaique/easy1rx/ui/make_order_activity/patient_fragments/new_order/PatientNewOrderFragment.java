package com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.new_order;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.GetUserAddressesResponse;
import com.codecaique.easy1rx.Models.responses.ShowCitiesResponse;
import com.codecaique.easy1rx.ui.BranchesMapActivity.BranchesMapActivity;
import com.codecaique.easy1rx.ui.MapToGetAddressActivity.MapToGetAddress;
import com.codecaique.easy1rx.ui.custom_photo_gallery_activity.CustomPhotoGalleryActivity;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.codecaique.easy1rx.Models.responses.PatientMakeOrderResponse;
import com.codecaique.easy1rx.Models.rquests.PatentMakeOrderModel;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.ui.home_activity.settings_fragment.AdressesAdapterInSettings;
import com.codecaique.easy1rx.ui.make_order_activity.OrderImagesRecyclerAdapter;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;
import static com.codecaique.easy1rx.ui.make_order_activity.OrderImagesRecyclerAdapter.rotateImage;


public class PatientNewOrderFragment extends Fragment {
    String currentPhotoPath;
    private OnFragmentInteractionListener listener;
    private ArrayList<Bitmap> arrayListimages;
    ArrayList<String>paths;
    private RecyclerView recyclerView,recyclerViewAddresse;
    private RecyclerView.LayoutManager layoutManager;
    private OrderImagesRecyclerAdapter adapter;
    AdressesAdapterInSettings adapterAddresses;
    private PatentMakeOrderModel.order_data order_data;
    String deleveryState="0";
    public static String storeName="";
    List<ShowCitiesResponse.UserDataBean> cities;
    List<MultipartBody.Part>filesToUpload;
    List<RequestBody>filesNames;
    EditText editTextPromoCode;
    //    EditText editTextAddress;
    Button buttonuploadImage,buttonNewAddress,buttonSelectAddress;
    public  static Button buttonMakeOrder;
    ImageButton buttonOpenMap;
    CardView cardView;
    ImageButton imageButtonUploadImage;
    RadioGroup radioGroup;
    Spinner spinner;
    public static EditText editTextNewAddress;
    EditText editTextLandMark;
    Button buttonOK;
    TextView textViewDeliveryAddress;
    Dialog dialogOrderInserted,pbar,dialogSelectAddress;
    TextView dialogOrderId;
    TextView textViewCity;
    public static TextView textViewStoreName;
    public static int branchId=0;
    public static TextView textViewAddress;
    TextView textViewLandMark;
    Dialog dialogNewAddress,dialogSelectImageType;
    String addressState="";
    int address_id;
    private String CAPTURE_IMAGE_FILE_PROVIDER = "com.codecaique.easy1rx.fileprovider";
    private final int PICK_IMAGE_MULTIPLE =1;
    private final int PICK_IMAGE_FROM_CAMERA=0;
    List<GetUserAddressesResponse.DataBean> userAdressesDataBean;

    public static String city_name = "" , lat = "", lng = "";

    //    Spinner spinner;
    public static PatientNewOrderFragment newInstance() {
        return new PatientNewOrderFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 1);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view=inflater.inflate(R.layout.fragment_patient_make_order, container, false);
        buttonMakeOrder=view.findViewById(R.id.buttonMakeOrder);
        buttonNewAddress= view.findViewById(R.id.buttonAddNewAddress);
        buttonSelectAddress= view.findViewById(R.id.buttonSelectAddress);

        buttonuploadImage = view.findViewById(R.id.buttonuploadImage);
        imageButtonUploadImage = view.findViewById(R.id.imageButtonUploadImage);
        editTextPromoCode =  view.findViewById(R.id.editTextPromoCode);
        textViewCity  = view.findViewById(R.id.textViewCity);
        textViewAddress = view.findViewById(R.id.textViewAddress);
        textViewStoreName=view.findViewById(R.id.storeName);

        textViewLandMark= view.findViewById(R.id.textViewLandMark);
        textViewDeliveryAddress= view.findViewById(R.id.textViewDeliveyAddress);
        cardView= view.findViewById(R.id.card_view);

        buttonNewAddress.setVisibility(View.GONE);
        buttonSelectAddress.setVisibility(View.GONE);
        buttonMakeOrder.setVisibility(View.GONE);
        textViewDeliveryAddress.setVisibility(View.GONE);
        cardView.setVisibility(View.GONE);

        radioGroup= view.findViewById(R.id.radiogroup);


        if(!textViewStoreName.getText().equals(""))

        {
            textViewStoreName.setVisibility(View.GONE);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                RadioButton checkedRadioButton = group.findViewById(checkedId);
                boolean isChecked = checkedRadioButton.isChecked();
                if (isChecked)
                {
                    if(checkedRadioButton.getId()==R.id.radioButtonDelivery)
                    {
                        deleveryState="1";

                        buttonNewAddress.setVisibility(View.VISIBLE);
                        buttonSelectAddress.setVisibility(View.VISIBLE);
                        textViewStoreName.setVisibility(View.INVISIBLE);
                        buttonMakeOrder.setVisibility(View.GONE);
                    }
                    else {
                        buttonNewAddress.setVisibility(View.GONE);
                        buttonSelectAddress.setVisibility(View.GONE);
                        textViewDeliveryAddress.setVisibility(View.GONE);
                        cardView.setVisibility(View.GONE);

                        if(!textViewStoreName.getText().equals(""))
                        {
                            textViewStoreName.setVisibility(View.VISIBLE);
                            buttonMakeOrder.setVisibility(View.VISIBLE);
                            deleveryState="0";

                        }
                        else
                        {
                            Intent intent=new Intent(getContext(),BranchesMapActivity.class);
                            intent.putExtra("type","new");
                            startActivity(intent);
                        }



                    }
                }
            }
        });


        dialogOrderInserted = new Dialog(getContext());
        dialogOrderInserted.setContentView(R.layout.dialog_orderi_nserted);
        dialogOrderId= dialogOrderInserted.findViewById(R.id.dialogOrderId);



        Button continueShopping = dialogOrderInserted.findViewById(R.id.buttonConitnueShopping);

        continueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOrderInserted.hide();
                Intent intent=new Intent(getContext(), HomeActivity.class);
                startActivity(intent);
            }
        });


        pbar = new Dialog(getContext(),R.style.DialogCustomTheme);
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);

        pbar.setContentView(R.layout.prograss_dialog);

        pbar.setTitle(getString(R.string.please_wait));
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        dialogSelectAddress = new Dialog(getContext());
        dialogSelectAddress.setContentView(R.layout.dialog_select_address);
        recyclerViewAddresse= dialogSelectAddress.findViewById(R.id.recyclerView);
        recyclerViewAddresse.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerViewAddresse.setLayoutManager(layoutManager);
        recyclerViewAddresse.setItemAnimator(new DefaultItemAnimator());
//        ArrayList<String> arrayList=new ArrayList<String>();
//        arrayList.add("23st, beba");
//
//        arrayList.add("24, new bns, beni_suif");
//        arrayList.add("655,wasta");
//        arrayList.add("33 st elzahra, fashn");
        userAdressesDataBean=new ArrayList<GetUserAddressesResponse.DataBean>();
        adapterAddresses = new AdressesAdapterInSettings(getContext(),userAdressesDataBean,new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                addressState="selected";
                Log.e("Address",userAdressesDataBean.get(position).getCity_arabicname());
                address_id=userAdressesDataBean.get(position).getId();
                lat = userAdressesDataBean.get(position).getLat();
                lng = userAdressesDataBean.get(position).getLng();
                textViewDeliveryAddress.setVisibility(View.VISIBLE);
                cardView.setVisibility(View.VISIBLE);
                buttonMakeOrder.setVisibility(View.VISIBLE);


                textViewAddress.setText(userAdressesDataBean.get(position).getAddress());
                textViewCity.setText(userAdressesDataBean.get(position).getCity_arabicname());
                textViewLandMark.setText(userAdressesDataBean.get(position).getLand_mark());



                dialogSelectAddress.hide();
            }
        });
        recyclerViewAddresse.setAdapter(adapterAddresses);




        dialogNewAddress = new Dialog(getContext());
        dialogNewAddress.setContentView(R.layout.dialog_add_new_address);
//        spinner = new Spinner(getContext());
//        spinner = dialogNewAddress. findViewById(R.id.spinner1);
        editTextNewAddress= dialogNewAddress.findViewById(R.id.editTextUserAddress);
        editTextLandMark= dialogNewAddress.findViewById(R.id.editTextLandMark);
        buttonOK= dialogNewAddress.findViewById(R.id.buttonOK);
        buttonOpenMap = dialogNewAddress.findViewById(R.id.buttonMap);
        buttonOpenMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getContext(), MapToGetAddress.class);
                intent.putExtra("type","newOrder");
                startActivity(intent);
            }
        });
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address=(editTextNewAddress.getText().toString());
//                String city=spinner.getSelectedItem().toString();
                String landMark=(editTextLandMark.getText().toString());

                Log.e("Adress 123",address + "  ");
                Log.e("City 123",city_name + "  ");


                textViewAddress.setText(address);
                textViewCity.setText(city_name);

//                textViewCity.setText(city);
                textViewLandMark.setText(landMark);

                if(address!=null) {
                    addressState="new";
                    textViewDeliveryAddress.setVisibility(View.VISIBLE);
                    cardView.setVisibility(View.VISIBLE);
                    buttonMakeOrder.setVisibility(View.VISIBLE);
                }


                dialogNewAddress.hide();
            }
        });






        buttonSelectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

                String userId=pref.getString("userId","");
                NetworkConnection connection = new NetworkConnection();

                if (!connection.isOnline()) {
                    pbar.show();
                    getUserAdresses(userId);

                } else {
                    Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();


                }
            }
        });

        buttonNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCities();
                dialogNewAddress.show();
            }
        });


        arrayListimages=new ArrayList<>();
        paths=new ArrayList<>();
        recyclerView= view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new GridLayoutManager(getContext(),2);
//        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter=new OrderImagesRecyclerAdapter(getContext(), arrayListimages,paths, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                RecyclerView.ViewHolder viewHolder = new OrderImagesRecyclerAdapter.ViewHolder(v);
                position = viewHolder.getAdapterPosition();
                arrayListimages.remove(position);
                paths.remove(position);
                filesToUpload.remove(position);
                adapter.notifyDataSetChanged();
            }
        });

        recyclerView.setAdapter(adapter);







        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Remove swiped item from list and notify the RecyclerView
                int position = viewHolder.getAdapterPosition();
                arrayListimages.remove(position);
                paths.remove(position);
                filesToUpload.remove(position);
                adapter.notifyDataSetChanged();
            }
        };


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


        filesToUpload=new ArrayList<MultipartBody.Part>();
        filesNames=new ArrayList<RequestBody>();


        imageButtonUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chooseImage();

            }
        });

        buttonuploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chooseImage();
            }
        });

        buttonMakeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//        get cities for comparison
                showCities();


                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                String userId=pref.getString("userId","");

                order_data=new PatentMakeOrderModel.order_data();
                order_data.setOrder_id(Integer.parseInt(userId));
                String promocode=editTextPromoCode.getText().toString();

                NetworkConnection connection = new NetworkConnection();

                if (!connection.isOnline()) {
                    //Make order
                    pbar.show();
                    if(radioGroup.getCheckedRadioButtonId()==R.id.radioButtonDelivery) {
                        if (addressState.equals("new")) {
                            String address = textViewAddress.getText().toString();
                            String landMark = textViewLandMark.getText().toString();
                            String city = textViewCity.getText().toString();

//                            Log.e("CITY",city_name);

                            int city_id = 0;
//                            for (int i = 0; i < cities.size(); i++) {
//                                if (cities.get(i).getE_name().equals(city)) {
//                                    city_id = cities.get(i).getId();
//                                }
//                            }

                            Log.e("CITy  address" , city_name);
                            Log.e("LAT" , "" + lat + " Long" + lng);
                            Log.e("Address",address);
                            if (arrayListimages.size() == 0) {
                                pbar.hide();
                                Toast.makeText(getContext(), getString(R.string.upload_image), Toast.LENGTH_SHORT).show();
                            } else {
                                makePatientNewOrderNewAddress(Integer.parseInt(userId),
                                        adapter.getData(), promocode, deleveryState, address, city_name, landMark);
                            }
                        } else if (addressState.equals("selected")) {
                            Log.e("CITy  address", city_name + "    : ");
                            Log.e("Adreess", addressState + address_id);
                            if (arrayListimages.size() == 0) {
                                pbar.hide();
                                Toast.makeText(getContext(), getString(R.string.upload_image), Toast.LENGTH_SHORT).show();
                            } else {
                                makePatientNewOrderselectAddress(Integer.parseInt(userId), adapter.getData(), promocode,
                                        deleveryState, address_id);
                            }
                        }

                    }
                    else if(radioGroup.getCheckedRadioButtonId()==R.id.radioButtonPicKupFromStore) {
                        if (arrayListimages.size() == 0) {
                            pbar.hide();
                            Toast.makeText(getContext(), getString(R.string.upload_image), Toast.LENGTH_SHORT).show();
                        } else {
                            makePateintOrder2(Integer.parseInt(userId), adapter.getData(), promocode, deleveryState);
                        }
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();


                }



            }
        });






        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }




    public void chooseImage() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);

//        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        startActivityForResult(pickPhoto, 1);
//


//Intent intent=new Intent(getContext(), SelectImagesActivity.class);
//startActivity(intent);


        dialogSelectImageType = new Dialog(getContext());
        dialogSelectImageType.setContentView(R.layout.dialog_image_type);

        ImageButton camera = dialogSelectImageType.findViewById(R.id.buttonCamera);
        ImageButton gallery = dialogSelectImageType.findViewById(R.id.buttonGallery);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectImageType.hide();

//                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takePicture, PICK_IMAGE_FROM_CAMERA);

//                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//                int imageId=pref.getInt("image",0);
//
//                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
//                if (!path.exists()) path.mkdirs();
//                File image = new File(path, "image_capture_"+""+imageId+".jpg");
//                Uri imageUri = FileProvider.getUriForFile(getActivity().getApplicationContext(), CAPTURE_IMAGE_FILE_PROVIDER, image);
//                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//                providePermissionForProvider(cameraIntent, imageUri);
//                startActivityForResult(cameraIntent, PICK_IMAGE_FROM_CAMERA);

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Ensure that there's a camera activity to handle the intent
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(getContext(),
                                "com.codecaique.easy1rx.fileprovider",
                                photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, PICK_IMAGE_FROM_CAMERA);
                    }
                }

            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSelectImageType.hide();
                Intent intent = new Intent(getActivity(), CustomPhotoGalleryActivity.class);
                startActivityForResult(intent,PICK_IMAGE_MULTIPLE);

//                Intent intent = new Intent();
//                intent.setType("image/*");
//                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_MULTIPLE);

            }
        });

        dialogSelectImageType.show();



    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if(requestCode == PICK_IMAGE_FROM_CAMERA)
            {
//                Bitmap photo = (Bitmap) data.getExtras().get("data");
//                Uri tempUri = getImageUri(getContext(), photo);
//                File file1= new File(getRealPathFromURI(tempUri));
//                arrayListimages.add(photo);
//                adapter.notifyDataSetChanged();
//
//                    RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), file1);
//                    MultipartBody.Part body= MultipartBody.Part.createFormData("image[]", file1.getName(), requestFile);
//
//                    filesToUpload.add(body);
//                Log.e("camera", "camera: "+data.getExtras().get("data"));






//                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//                SharedPreferences.Editor editor = pref.edit();
//                int imageId=pref.getInt("image",0);
//
//                //get the file from the uri path  , it is the same declared in the path file in xml folder. in this case it is Gallery/MyImages/
//                File path = new File(getActivity().getFilesDir(), "Gallery/MyImages/");
//                if (!path.exists()) path.mkdirs();
//                File imageFile = new File(path, "image_capture_"+""+imageId+".jpg");
//
//                Uri stringUri = Uri.fromFile(imageFile);





                // Get the dimensions of the bitmap
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;



                // Determine how much to scale down the image

                // Decode the image file into a Bitmap sized to fill the View
                bmOptions.inJustDecodeBounds = false;
                bmOptions.inPurgeable = true;

//                Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
                File imageFile = new File(currentPhotoPath);

//                try {
                Bitmap bitmapFromMedia = BitmapFactory.decodeFile(currentPhotoPath);


                arrayListimages.add(bitmapFromMedia);
                paths.add(currentPhotoPath);
                adapter.notifyDataSetChanged();

//                    editor.putInt("image", imageId+1);
//                    editor.commit();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

                RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), imageFile);
                MultipartBody.Part body= MultipartBody.Part.createFormData("image[]", imageFile.getName(), requestFile);

                filesToUpload.add(body);

            }
            if(requestCode == PICK_IMAGE_MULTIPLE){
                String[] imagesPath = data.getStringExtra("data").split("\\|");

                for (int i=0;i<imagesPath.length;i++){
                    Log.e("onActivityResult", "onActivityResult: "+imagesPath[i]);

                    Bitmap bitmap = BitmapFactory.decodeFile(imagesPath[i]);
                    arrayListimages.add(bitmap);
                    paths.add(imagesPath[i]);


                    File file1=new File(imagesPath[i]);
                    RequestBody requestFile=RequestBody.create(MediaType.parse("image/*"), file1);
                    MultipartBody.Part body= MultipartBody.Part.createFormData("image[]", file1.getName(), requestFile);
                    Log.e("onActivityResult2", "onActivityResult: "+body.toString());

                    filesToUpload.add(body);

                }

                adapter.notifyDataSetChanged();
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }



    private void makePateintOrder2(final int user_id, final List<MultipartBody.Part>filesToUpload, String promo_code, String delivery) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */

        Call<PatientMakeOrderResponse> call = projectAPIs.makepatientNewOrderPickfromStore(user_id,filesToUpload,
                RequestBody.create(MediaType.parse("multipart/form-data"),promo_code),
                branchId,
                RequestBody.create(MediaType.parse("multipart/form-data"),delivery),
                RequestBody.create(MediaType.parse("multipart/form-data"),lng),
                RequestBody.create(MediaType.parse("multipart/form-data"),lat)
        );
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<PatientMakeOrderResponse>() {
            @Override
            public void onResponse(Call<PatientMakeOrderResponse> call, Response<PatientMakeOrderResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                Log.e("inner= ", response.message());

                pbar.hide();
                if(response.isSuccessful()) {


                    if (response.body().getMessage().contains("order inserted success") || response.body().getMessage().contains("تم ادخال الطلب بنجاح")) {
                        pbar.hide();

                        int selectedId= radioGroup.getCheckedRadioButtonId();
                        if(selectedId==R.id.radioButtonPicKupFromStore)
                        {
                            if(response.body().getOrder_data().getImages()!=null) {
                                pbar.hide();

                                filesToUpload.clear();
                                arrayListimages.clear();
                                adapter.notifyDataSetChanged();

                                dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                                dialogOrderInserted.show();
                            }
//                         Uri gmmIntentUri = Uri.parse("https://www.google.co.in/maps/dir/18.6121132,73.707989/18.5,73.7/18.8,73.71/18.9,73.75");
//                         Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                         mapIntent.setPackage("com.google.android.apps.maps");
//                         startActivity(mapIntent);

//                         Intent intent=new Intent(getContext(), BranchesMapActivity.class);
//                         startActivity(intent);

//                         Intent intent=new Intent(getContext(),HomeActivity.class);
//                         startActivity(intent);
                        }
                        else if(selectedId==R.id.radioButtonDelivery) {
                            if(response.body().getOrder_data().getImages()!=null) {
                                pbar.hide();

                                filesToUpload.clear();
                                arrayListimages.clear();
                                adapter.notifyDataSetChanged();

                                dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                                dialogOrderInserted.show();
                            }
                        }

                    } else {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        pbar.hide();

                    }
                }
            }
            @Override
            public void onFailure(Call<PatientMakeOrderResponse> call, Throwable t) {
                pbar.hide();

                Toast.makeText(getContext(), getString(R.string.failure)+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }




    private void makePatientNewOrderNewAddress(final int user_id, List<MultipartBody.Part>filesToUpload, String promo_code, String delivery, String address, String city_id, String land_mark) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */

        try {
            Call<PatientMakeOrderResponse> call = projectAPIs.makePatientNewOrderNewAddress(user_id,
                    filesToUpload,
                    RequestBody.create(MediaType.parse("multipart/form-data"),promo_code),
                    RequestBody.create(MediaType.parse("multipart/form-data"),delivery),
                    RequestBody.create(MediaType.parse("multipart/form-data"),address),
                    RequestBody.create(MediaType.parse("multipart/form-data"),city_id),
                    RequestBody.create(MediaType.parse("multipart/form-data"),land_mark),
                    RequestBody.create(MediaType.parse("multipart/form-data"),lng),
                    RequestBody.create(MediaType.parse("multipart/form-data"),lat)
                    );
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
            call.enqueue(new Callback<PatientMakeOrderResponse>() {
                @Override
                public void onResponse(Call<PatientMakeOrderResponse> call, Response<PatientMakeOrderResponse> response) {
                    /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                     */
                    Log.e("inner= ", response.message());

                    if(response.isSuccessful()) {


                        if (response.body().getMessage().contains("order inserted success") || response.body().getMessage().contains("تم ادخال الطلب بنجاح")) {
                            pbar.hide();

                            int selectedId= radioGroup.getCheckedRadioButtonId();
                            if(selectedId==R.id.radioButtonPicKupFromStore)
                            {


                                dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                                dialogOrderInserted.show();
//                            Uri gmmIntentUri = Uri.parse("https://www.google.co.in/maps/dir/18.6121132,73.707989/18.5,73.7/18.8,73.71/18.9,73.75");
//                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                            mapIntent.setPackage("com.google.android.apps.maps");
//                            startActivity(mapIntent);
//                            Intent intent=new Intent(getContext(), BranchesMapActivity.class);
//                            startActivity(intent);
                            }
                            else if(selectedId==R.id.radioButtonDelivery) {


                                dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                                dialogOrderInserted.show();
                            }

                        } else {
                            Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            pbar.hide();

                        }
                    }
                }
                @Override
                public void onFailure(Call<PatientMakeOrderResponse> call, Throwable t) {
                    pbar.hide();

                    Toast.makeText(getContext(), getString(R.string.failure)+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e)
        {
            pbar.hide();
            Toast.makeText(getContext(), "There is an error please try again", Toast.LENGTH_SHORT).show();
            Log.e("EXCEPTION",e.getMessage());
        }
    }


    private void makePatientNewOrderselectAddress(final int user_id, List<MultipartBody.Part>images, String promo_code, String delivery, int address_id) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */

        Log.e("CITy  address" , city_name + "    : ");
        Log.e("Adreess 5555", addressState + address_id);
        Log.e("LAT" , "" + lat + " Long" + lng);
        Log.e("Delivery",delivery);


        Call<PatientMakeOrderResponse> call = projectAPIs.makePatientNewOrderSelectedAddress(
                user_id,
                images,
                RequestBody.create(MediaType.parse("multipart/form-data"),promo_code),
                address_id,
                RequestBody.create(MediaType.parse("multipart/form-data"),delivery),
                RequestBody.create(MediaType.parse("multipart/form-data"),lng),
                RequestBody.create(MediaType.parse("multipart/form-data"),lat)
                );
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<PatientMakeOrderResponse>() {
            @Override
            public void onResponse(Call<PatientMakeOrderResponse> call, Response<PatientMakeOrderResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                Log.e("inner= ", response.message());

                pbar.hide();
                if(response.isSuccessful()) {






                    if (response.body().getMessage().contains("order inserted success") || response.body().getMessage().contains("تم ادخال الطلب بنجاح")) {
                        pbar.hide();



                        int selectedId= radioGroup.getCheckedRadioButtonId();
                        if(selectedId==R.id.radioButtonPicKupFromStore)
                        {

                            dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                            dialogOrderInserted.show();
                            filesToUpload.clear();
                            adapter.notifyDataSetChanged();
//                            Uri gmmIntentUri = Uri.parse("https://www.google.co.in/maps/dir/18.6121132,73.707989/18.5,73.7/18.8,73.71/18.9,73.75");
//                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                            mapIntent.setPackage("com.google.android.apps.maps");
//                            startActivity(mapIntent);


//                            Intent intent=new Intent(getContext(), BranchesMapActivity.class);
//                            startActivity(intent);
                        }
                        else if(selectedId==R.id.radioButtonDelivery) {

                            dialogOrderId.setText("# " + response.body().getOrder_data().getOrder_details().getId());
                            dialogOrderInserted.show();
                        }

                    }
                    else {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        pbar.hide();

                    }
                }
            }
            @Override
            public void onFailure(Call<PatientMakeOrderResponse> call, Throwable t) {
                pbar.hide();

                Toast.makeText(getContext(), getString(R.string.failure)+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void showCities() {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<ShowCitiesResponse> call = projectAPIs.showCities();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<ShowCitiesResponse>() {
            @Override
            public void onResponse(Call<ShowCitiesResponse> call, Response<ShowCitiesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {
                    assert response.body() != null;
                    cities=response.body().getUser_data();
                    ArrayList<String> citiesEnglishNames=new ArrayList<String>();
                    for(int i=0;i<cities.size();i++)
                    {
                        citiesEnglishNames.add(cities.get(i).getA_name());
//                        Log.e("CITIES no" + i , cities.get(i).getE_name());
                    }

                    Log.e("CITIES size" , cities.size() + "     ss");
                    ArrayAdapter<String> citiesAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(),R.layout.spinner_item, citiesEnglishNames);
//
//                    spinner.setAdapter(citiesAdapter);

                }
                else {
                }
            }
            @Override
            public void onFailure(Call<ShowCitiesResponse> call, Throwable t) {
//                pbar.hide();
            }
        });
    }


    private void getUserAdresses(String userId) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<GetUserAddressesResponse> call = projectAPIs.getUserAddresses(userId);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<GetUserAddressesResponse>() {
            @Override
            public void onResponse(Call<GetUserAddressesResponse> call, Response<GetUserAddressesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                    if (response.body().getMessage().equals("show address data")) {
                        userAdressesDataBean=new ArrayList<GetUserAddressesResponse.DataBean>();
                        userAdressesDataBean=response.body().getData();
                        adapterAddresses = new AdressesAdapterInSettings(getContext(),userAdressesDataBean,new CustomItemClickListener() {
                            @Override
                            public void onItemClick(View v, int position) {
                                addressState="selected";
                                address_id=userAdressesDataBean.get(position).getId();


                                textViewDeliveryAddress.setVisibility(View.VISIBLE);
                                cardView.setVisibility(View.VISIBLE);
                                buttonMakeOrder.setVisibility(View.VISIBLE);


                                textViewAddress.setText(userAdressesDataBean.get(position).getAddress());
                                textViewCity.setText(userAdressesDataBean.get(position).getCity_arabicname());
                                textViewLandMark.setText(userAdressesDataBean.get(position).getLand_mark());



                                dialogSelectAddress.hide();
                            }
                        });

                        recyclerViewAddresse.setAdapter(adapterAddresses);

                        adapterAddresses.notifyDataSetChanged();
                        pbar.hide();
                        dialogSelectAddress.show();

                    }


                    else {
                        pbar.hide();
                        Toast.makeText(getContext(), getString(R.string.you_dont_have_addresses), Toast.LENGTH_SHORT).show();
                    }
            }

            @Override
            public void onFailure(Call<GetUserAddressesResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }



    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getPath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
//        this.sendBroadcast(mediaScanIntent);
    }





    private void providePermissionForProvider(Intent intent, Uri uri) {
        List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            getActivity().grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }


    public Bitmap rotate(Bitmap rotatedBitmap,String path)
    {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(path);


        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90: {
                rotatedBitmap = rotateImage(rotatedBitmap, 90);

                break;
            }
            case ExifInterface.ORIENTATION_ROTATE_180: {
                rotatedBitmap = rotateImage(rotatedBitmap, 180);

                break;
            }
            case ExifInterface.ORIENTATION_ROTATE_270: {
                rotatedBitmap = rotateImage(rotatedBitmap, 270);

                break;
            }

            case ExifInterface.ORIENTATION_NORMAL:
            default: {
                rotatedBitmap = rotatedBitmap;
            }
        }
        return  rotatedBitmap;
    }

    public interface OnFragmentInteractionListener {
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}
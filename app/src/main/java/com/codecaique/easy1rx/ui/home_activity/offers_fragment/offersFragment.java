package com.codecaique.easy1rx.ui.home_activity.offers_fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.Get_whats_app_phone;
import com.codecaique.easy1rx.Models.responses.ClinicResponse;
import com.codecaique.easy1rx.Models.responses.PharmacyResponse;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.ui.offer_details_activity.OfferDetailsActivity;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class offersFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<PharmacyResponse.OffersDataBean> data;
    private List<ClinicResponse.OffersDataBean> clinicData;
    private RecyclerView.LayoutManager layoutManager;
    private PharmacyOffersRecyclerAdapter adapter;
    private ClinicOffersRecyclerAdapter clinicAdapter;
    private FloatingActionButton buttonWhatsApp;
    private Dialog pbar;
//    SearchView editTextSearch;
    Button buttonPaharmacy,buttonClinic;
    String whatsAppNumber="";
    NetworkConnection connection;
    private OnFragmentInteractionListener listener;
    ImageView gifImg;

    public static offersFragment newInstance() {
        return new offersFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_offers, container, false);
        buttonPaharmacy= view.findViewById(R.id.buttonPharmacyOffers);
        buttonClinic= view.findViewById(R.id.buttonClinicOffers);
//        editTextSearch=(SearchView) view.findViewById(R.id.editTextSearch);
        buttonWhatsApp= view.findViewById(R.id.buttonWhatsapp);

        recyclerView = view. findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new GridLayoutManager(getContext(),2);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter = new PharmacyOffersRecyclerAdapter(getContext(), data, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
            Intent intent=new Intent(getActivity(), OfferDetailsActivity.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable("my object",data.get(position));
            intent.putExtra("data",bundle);
            startActivity(intent);
            }
        });




        pbar = new Dialog(getContext(),R.style.DialogCustomTheme);
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pbar.setContentView(R.layout.prograss_dialog);
//        gifImg = pbar.findViewById(R.id.gifImage);
//        Glide.with(getContext()).load(R.drawable.ripple).into(gifImg);
        pbar.setTitle(getString(R.string.please_wait));
        connection = new NetworkConnection();


        if (!connection.isOnline()) {
            //Make order
            pbar.show();
            get_whats_app_phone();
            fetchPharmacyDetails();

        }
        else {
            Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

        }



        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
//        editTextSearch.setSearchableInfo(searchManager
//                .getSearchableInfo(getActivity().getComponentName()));
//        editTextSearch.setMaxWidth(Integer.MAX_VALUE);
//
//        // listening to search query text change
//        editTextSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                // filter recycler view when query submitted
//                adapter.getFilter().filter(query);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String query) {
//                // filter recycler view when text is changed
//                adapter.getFilter().filter(query);
//                return false;
//            }
//        });





        buttonPaharmacy.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                buttonClinic.setTextColor(getResources().getColor(R.color.colorAccent));
                buttonClinic.setBackgroundResource(R.drawable.borders_accent_bacground_white);

                buttonPaharmacy.setTextColor(getResources().getColor(R.color.colorWhite));
                buttonPaharmacy.setBackgroundResource(R.drawable.borders_accent_background);

                if (!connection.isOnline()) {
                    //Make order
                    pbar.show();

                    fetchPharmacyDetails();
                    adapter.notifyDataSetChanged();                }
                else {
                    Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

                }





            }
        });
        buttonClinic.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        buttonClinic.setTextColor(getResources().getColor(R.color.colorWhite));
        buttonClinic.setBackgroundResource(R.drawable.borders_accent_background);

        buttonPaharmacy.setTextColor(getResources().getColor(R.color.colorAccent));
        buttonPaharmacy.setBackgroundResource(R.drawable.borders_accent_bacground_white);


        if (!connection.isOnline()) {
            //Make order
            pbar.show();

            fetchClinicDetails();
            adapter.notifyDataSetChanged();              }
        else {
            Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

        }




          }
        });

//        buttonMakeOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//
//                String userId=pref.getString("userId","");
//                Toast.makeText(getContext(), userId, Toast.LENGTH_SHORT).show();
//
//                NetworkConnection connection = new NetworkConnection();
//
//                if (!connection.isOnline()) {
//                    getUserOrder(""+userId);
//
//                } else {
//                    Toast.makeText(getContext(), "No internet connection !", Toast.LENGTH_SHORT).show();
//
//
//                }
//
//
//
//
//            }
//        });




//        editTextSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                //after the change calling the method and passing the search input
//                filter(editable.toString());
//            }
//        });



        buttonWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWhatsApp(view);
            }
        });



        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnFragmentInteractionListener {
    }

    private void fetchPharmacyDetails() {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<PharmacyResponse> call = projectAPIs.getPhamacyOffers();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<PharmacyResponse>() {
            @Override
            public void onResponse(Call<PharmacyResponse> call, Response<PharmacyResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {




                    data=new ArrayList<PharmacyResponse.OffersDataBean>();

                    PharmacyResponse pharmacyResponse = response.body();

                    List<PharmacyResponse.OffersDataBean> offers = response.body().getOffers_data();

                    data =  offers;

                    adapter = new PharmacyOffersRecyclerAdapter(getContext(), data, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position) {

                            Intent intent=new Intent(getActivity(), OfferDetailsActivity.class);
                            Bundle bundle=new Bundle();
                            bundle.putSerializable("my object",data.get(position));
                            intent.putExtra("data",bundle);
                            intent.putExtra("type","pharm");
                            startActivity(intent);
//                            Log.d("clivk", "clicked position:" + position);
//
//                            Intent intent = new Intent(getContext(), NewsDetails.class);
//                            Bundle bundle = new Bundle();
//                            bundle.putSerializable("my object", (Serializable) data.get(position));
//                            intent.putExtra("data", bundle);
//                            startActivity(intent);
                        }
                    });
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                    pbar.hide();


                }
                else {
                    pbar.hide();
                }
            }
            @Override
            public void onFailure(Call<PharmacyResponse> call, Throwable t) {
                pbar.hide();
            }
        });
    }
    private void fetchClinicDetails() {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<ClinicResponse> call = projectAPIs.getClinicOffers();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<ClinicResponse>() {
            @Override
            public void onResponse(Call<ClinicResponse> call, Response<ClinicResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {
                    clinicData=new ArrayList<ClinicResponse.OffersDataBean>();

                    ClinicResponse articlesResponse = response.body();

                    List<ClinicResponse.OffersDataBean> offers = response.body().getOffers_data();

                    clinicData =  offers;

                    clinicAdapter = new ClinicOffersRecyclerAdapter(getContext(), clinicData, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position) {


                            Log.e("ONCLICK",clinicData.get(0).getId() + "" + clinicData.get(0).getEnglish_name());


                            Intent intent=new Intent(getContext(), OfferDetailsActivity.class);
                            Bundle bundle=new Bundle();
                            intent.putExtra("type","clinic");
                            bundle.putSerializable("my object",clinicData.get(position));
                            intent.putExtra("data",bundle);
                            startActivity(intent);

//                            Log.d("clivk", "clicked position:" + position);
//
//                            Intent intent = new Intent(getContext(), NewsDetails.class);
//                            Bundle bundle = new Bundle();
//                            bundle.putSerializable("my object", (Serializable) data.get(position));
//                            intent.putExtra("data", bundle);
//                            startActivity(intent);
                        }
                    });
                    pbar.hide();

                    recyclerView.setAdapter(clinicAdapter);
                    clinicAdapter.notifyDataSetChanged();


//                    editTextSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//                        @Override
//                        public boolean onQueryTextSubmit(String query) {
//                            // filter recycler view when query submitted
//                            clinicAdapter.getFilter().filter(query);
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onQueryTextChange(String query) {
//                            // filter recycler view when text is changed
//                            clinicAdapter.getFilter().filter(query);
//                            return false;
//                        }
//                    });




                }
            }
            @Override
            public void onFailure(Call<ClinicResponse> call, Throwable t) {
                pbar.hide();
            }
        });
    }

    private void get_whats_app_phone() {
        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<Get_whats_app_phone> call = projectAPIs.get_whats_app_phone();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<Get_whats_app_phone>() {
            @Override
            public void onResponse(Call<Get_whats_app_phone> call, Response<Get_whats_app_phone> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {

                    Get_whats_app_phone get_whats_app_phone = response.body();
                    String phone=get_whats_app_phone.getAbout_data().getPhone();
                    whatsAppNumber=phone;


                }
            }
            @Override
            public void onFailure(Call<Get_whats_app_phone> call, Throwable t) {
            }
        });
    }


//    private void get_whats_app_phone() {
//
//        //Obtain an instance of Retrofit by calling the static method.
//        Retrofit retrofit = NetworkClient.getRetrofitClient();
//        /*
//        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
//        */
//        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
//        /*
//        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
//        */
//        Call<ClinicResponse> call = projectAPIs.getClinicOffers();
//        /*
//        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
//        */
//        call.enqueue(new Callback<ClinicResponse>() {
//            @Override
//            public void onResponse(Call<ClinicResponse> call, Response<ClinicResponse> response) {
//                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
//                 */
//                if(response.isSuccessful()) {
//                    clinicData=new ArrayList<ClinicResponse.OffersDataBean>();
//
//                    ClinicResponse articlesResponse = response.body();
//
//                    List<ClinicResponse.OffersDataBean> offers = response.body().getOffers_data();
//
//                    clinicData =  offers;
//
//                    clinicAdapter = new ClinicOffersRecyclerAdapter(getContext(), clinicData, new CustomItemClickListener() {
//                        @Override
//                        public void onItemClick(View v, int position) {
//
//
//                            Intent intent=new Intent(getContext(), OfferDetailsActivity.class);
//                            startActivity(intent);
//
////                            Log.d("clivk", "clicked position:" + position);
////
////                            Intent intent = new Intent(getContext(), NewsDetails.class);
////                            Bundle bundle = new Bundle();
////                            bundle.putSerializable("my object", (Serializable) data.get(position));
////                            intent.putExtra("data", bundle);
////                            startActivity(intent);
//                        }
//                    });
//                    pbar.hide();
//
//                    recyclerView.setAdapter(clinicAdapter);
//                    clinicAdapter.notifyDataSetChanged();
//
//
////                    editTextSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
////                        @Override
////                        public boolean onQueryTextSubmit(String query) {
////                            // filter recycler view when query submitted
////                            clinicAdapter.getFilter().filter(query);
////                            return false;
////                        }
////
////                        @Override
////                        public boolean onQueryTextChange(String query) {
////                            // filter recycler view when text is changed
////                            clinicAdapter.getFilter().filter(query);
////                            return false;
////                        }
////                    });
//
//
//
//
//                }
//            }
//            @Override
//            public void onFailure(Call<ClinicResponse> call, Throwable t) {
//                pbar.hide();
//            }
//        });
//    }
//





//    private void filter(String text) {
//        //new array list that will hold the filtered data
//        ArrayList<String> filterdNames = new ArrayList<String>();
//
//        //looping through existing elements
//        ArrayList<String> names=new ArrayList<String>();
//        for(int k=0;k<data.size();k++)
//        {
//            names.add(data.get(k).getEnglish_name());
//        }
//
//
//        for (String s : names) {
//            //if the existing elements contains the search input
//            if (s.toLowerCase().contains(text.toLowerCase())) {
//                //adding the element to filtered list
//                filterdNames.add(s);
//            }
//        }
//
//        //calling a method of the adapter class and passing the filtered list
//        adapter.filterList(filterdNames);
//    }



    public void openWhatsApp(View view){
//        PackageManager pm=getActivity().getPackageManager();
        try {


            String toNumber = whatsAppNumber; // Replace with mobile phone number without +Sign or leading zeros, but with country code.
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.

//            toNumber=toNumber.substring(1);
//
//
//            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + "966570133304" +
//                    ""+ "?body=" + ""));
//            sendIntent.setPackage("com.whatsapp");
//            startActivity(sendIntent);

            try {
                // Raise exception if whatsapp doesn't exist
                get_whats_app_phone();
                String url = "https://api.whatsapp.com/send?phone=" + whatsAppNumber;

                Log.e("URL ssad",url);

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setPackage("com.whatsapp");
                i.setData(Uri.parse(url));
                startActivity(i);
            } catch (Exception e) {
                Toast.makeText(getActivity(), "Please install whatsapp app", Toast.LENGTH_SHORT)
                        .show();
            }

        }
        catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.dont_have_whats_app),Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

package com.codecaique.easy1rx.ui.make_order_activity;

import android.content.SharedPreferences;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.codecaique.easy1rx.Models.UserState_Response;
import com.codecaique.easy1rx.Models.responses.SignInResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.make_order_activity.doctor_fragment.DoctorMakeOrderFragment;
import com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.new_order.PatientNewOrderFragment;
import com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.referred_order.PatientReferredOrderFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MakeOrderActivity extends AppCompatActivity implements PatientNewOrderFragment.OnFragmentInteractionListener, PatientReferredOrderFragment.OnFragmentInteractionListener,DoctorMakeOrderFragment.OnFragmentInteractionListener {
ImageButton buttonBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_order);
        buttonBack=(ImageButton)findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        Fragment fragment = null;
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

        String userState=pref.getString("userState","");
        String user_id = pref.getString("userId","");
        String state = checkState(user_id);

        if (state != null)
        {
            if(state.equals("user")) {
                String orderType=getIntent().getExtras().getString("orderType");
                if(orderType.equals("referred")||orderType.equals("reOrder")||orderType.equals("reOrder2"))
                {
                    fragment= new PatientReferredOrderFragment();
                }
                else if(orderType.equals("new")) {

                    fragment = new PatientNewOrderFragment();
                }


            }
            else {
                fragment = DoctorMakeOrderFragment.newInstance();

            }
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, fragment);
            fragmentTransaction.commit();
        }

        else
        {
            assert userState != null;
            if(userState.equals("user")) {
                String orderType=getIntent().getExtras().getString("orderType");
                if(orderType.equals("referred")||orderType.equals("reOrder")||orderType.equals("reOrder2"))
                {
                    fragment= new PatientReferredOrderFragment();
                }
                else if(orderType.equals("new")) {

                    fragment = new PatientNewOrderFragment();
                }

            }
            else {

                fragment = DoctorMakeOrderFragment.newInstance();

            }
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            assert fragment != null;
            fragmentTransaction.replace(R.id.frame_layout, fragment);
            fragmentTransaction.commit();
        }
    }

    public String checkState(String user_id)
    {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<UserState_Response> call = projectAPIs.getState(user_id);

        final String[] state = new String[1];

        call.enqueue(new Callback<UserState_Response>() {
            @Override
            public void onResponse(Call<UserState_Response> call, Response<UserState_Response> response) {
                assert response.body() != null;
                state[0] = response.body().getData().getState();
            }

            @Override
            public void onFailure(Call<UserState_Response> call, Throwable t) {

            }
        });
        return state[0];
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

package com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.referred_order;

/**
 * Created by I Love Allah on 06/10/2018.
 */

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ReferredOrderImagesRecyclerAdapter extends RecyclerView.Adapter<ReferredOrderImagesRecyclerAdapter.ViewHolder> {

    ArrayList<String> data;

    Context mContext;
    CustomItemClickListener listener;

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.make_order_item, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        ImageView imageView = holder.imageView;


        String orderImagesPath="http://pharmacyapi.codecaique.com/uploads/orders/"+data.get(position);

//        Picasso.with(mContext).load(orderImagesPath).into(imageView);
        Glide.with(mContext).load(orderImagesPath).into(holder.imageView);



    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public ReferredOrderImagesRecyclerAdapter(Context mContext, ArrayList<String> data, CustomItemClickListener listener) {
        this.data = data;
        this.mContext = mContext;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;


        ViewHolder(View v) {
            super(v);






            imageView = (ImageView) v.findViewById(R.id.imageView);
        }
    }

}

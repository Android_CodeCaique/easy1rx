package com.codecaique.easy1rx.ui.addresses_management_activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.AddAddressResponse;
import com.codecaique.easy1rx.Models.responses.DeleteAddressResponse;
import com.codecaique.easy1rx.Models.responses.GetUserAddressesResponse;
import com.codecaique.easy1rx.Models.responses.ShowCitiesResponse;
import com.codecaique.easy1rx.Models.responses.UpdateAddressResponse;
import com.codecaique.easy1rx.Models.rquests.AddAddressModel;
import com.codecaique.easy1rx.Models.rquests.UpdateAddressRequest;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.MapToGetAddressActivity.MapToGetAddress;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AdressesActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AdressesAdapter adapter;
    Spinner spinner;
    public static Spinner spinnerUpdate;
    public static EditText editTextNewAddress;
    EditText editTextLandMark;
    public static EditText editTextLandMarkUpdate,editTextNewAddressUpdate;
    Button buttonAddNewAddress, buttonOKforNew,buttonOKforUpdate;
    ImageButton buttonOpenMap,buttonOpenMapUpdate;
    public static List<GetUserAddressesResponse.DataBean> userAdressesDataBean;
    ArrayList<String> arrayList;
    public static String city_name = "", lat = "" , lng = "";
    Dialog dialogNewAddress,pbar;
    public static Dialog dialogUpdateOrDelete,dialogUpdateAddress;
    Button buttonDelete,buttonUpdate;
    ImageButton buttonBack;
    public static int itemPosition=-1;
    List<ShowCitiesResponse.UserDataBean> cities;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adresses);

        userAdressesDataBean=new ArrayList<GetUserAddressesResponse.DataBean>();
        buttonBack=(ImageButton)findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(AdressesActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        arrayList=new ArrayList<String>();
//        arrayList.add("23st, beba");
//        arrayList.add("24, new bns, beni_suif");
//        arrayList.add("655,wasta");
//        arrayList.add("33 st elzahra, fashn");




        SharedPreferences pref =getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        String userId=pref.getString("userId","");

        pbar = new Dialog(AdressesActivity.this);
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pbar.setContentView(R.layout.prograss_dialog);
        pbar.setTitle(getString(R.string.please_wait));
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pbar.show();
        showCities();
        getUserAdresses(userId);





        buttonAddNewAddress=(Button)findViewById(R.id.buttonAddNewAddress);
        dialogNewAddress = new Dialog(AdressesActivity.this);
        dialogNewAddress.setContentView(R.layout.dialog_add_new_address);
//        spinner = new Spinner(this);
//        spinner =(Spinner)dialogNewAddress. findViewById(R.id.spinner1);
        editTextNewAddress=(EditText)dialogNewAddress.findViewById(R.id.editTextUserAddress);
        editTextLandMark=(EditText)dialogNewAddress.findViewById(R.id.editTextLandMark);
        buttonOKforNew =(Button)dialogNewAddress.findViewById(R.id.buttonOK);
        buttonOpenMap =(ImageButton)dialogNewAddress.findViewById(R.id.buttonMap);
        buttonOpenMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AdressesActivity.this, MapToGetAddress.class);
                intent.putExtra("type","newAddress");
                startActivity(intent);
            }
        });

        dialogUpdateOrDelete = new Dialog(AdressesActivity.this);
        dialogUpdateOrDelete.setContentView(R.layout.dialog_address_update);
        dialogUpdateOrDelete.setTitle(getString(R.string.do_you_want_to));
        buttonDelete=(Button) dialogUpdateOrDelete.findViewById(R.id.buttonDeleteAddress);
        buttonUpdate=(Button) dialogUpdateOrDelete.findViewById(R.id.buttonUpdateAddress);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id=""+userAdressesDataBean.get(itemPosition).getId();
                deleteAddress(id,itemPosition);
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogUpdateOrDelete.hide();
                GetUserAddressesResponse.DataBean addressDataBean= userAdressesDataBean.get(itemPosition);
                editTextNewAddressUpdate.setText(addressDataBean.getAddress());
                String cityString=addressDataBean.getCity_arabicname();
//                for(int i=0;i<spinnerUpdate.getAdapter().getCount();i++)
//                {
//                    if(cityString.equals(spinnerUpdate.getAdapter().getItem(i)))
//                    {
//                        spinnerUpdate.setSelection(i);
//                        break;
//                    }
//                }
                editTextLandMarkUpdate.setText(addressDataBean.getLand_mark());
                dialogUpdateAddress.show();





            }
        });



        dialogUpdateAddress = new Dialog(AdressesActivity.this);
        dialogUpdateAddress.setContentView(R.layout.dialog_update_address_with_action);
//        spinnerUpdate = new Spinner(this);
//        spinnerUpdate =(Spinner)dialogUpdateAddress. findViewById(R.id.spinner1);
        editTextNewAddressUpdate=(EditText)dialogUpdateAddress.findViewById(R.id.editTextUserAddress);
        editTextLandMarkUpdate=(EditText)dialogUpdateAddress.findViewById(R.id.editTextLandMark);
        buttonOpenMapUpdate=(ImageButton)dialogUpdateAddress.findViewById(R.id.buttonMap);
        buttonOKforUpdate =(Button)dialogUpdateAddress.findViewById(R.id.buttonOK);
        buttonOpenMapUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AdressesActivity.this, MapToGetAddress.class);
                intent.putExtra("type","updateAddress");

                startActivity(intent);
            }
        });
        buttonOKforUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address=(editTextNewAddressUpdate.getText().toString());
//                String city=spinnerUpdate.getSelectedItem().toString();
                String landMark=(editTextLandMarkUpdate.getText().toString());
                int addressId=userAdressesDataBean.get(itemPosition).getId();

                UpdateAddressRequest updateAddressRequest=new UpdateAddressRequest();
                GetUserAddressesResponse.DataBean dataBean=new GetUserAddressesResponse.DataBean();
                updateAddressRequest.setAddress(address);
                updateAddressRequest.setLatitude(lat);
                updateAddressRequest.setLongitude(lng);
                updateAddressRequest.setCity(city_name);
                dataBean.setAddress(address);
                dataBean.setE_name(city_name);
                dataBean.setCity_arabicname(city_name);
                updateAddressRequest.setLand_mark(landMark);
                dataBean.setLand_mark(landMark);
                dataBean.setLat(lat);
                dataBean.setLng(lng);
                updateAddressRequest.setId(addressId);

                pbar.show();
                updateUserAddress(updateAddressRequest,itemPosition);



            }
        });










        buttonAddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogNewAddress.show();


            }
        });


        buttonOKforNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address=(editTextNewAddress.getText().toString());
//                String city=spinner.getSelectedItem().toString();
//                int city_id=0;
//                for(int i=0;i<cities.size();i++)
//                {
//                    if(cities.get(i).getE_name().equals(city))
//                    {
//                        city_id=cities.get(i).getId();
//                    }
//                }
                String landMark=(editTextLandMark.getText().toString());
                SharedPreferences pref =getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                String userId=pref.getString("userId","");

//                Log.e("CITy  address 456464564" , city_name);
                Log.e("LAT LNG", "" +  lat + lng);

                AddAddressModel addressModel=new AddAddressModel();
                GetUserAddressesResponse.DataBean dataBean=new GetUserAddressesResponse.DataBean();
                addressModel.setAddress(address);
                addressModel.setLatitude(lat);
                addressModel.setLongitude(lng);
                addressModel.setCity(city_name);
                dataBean.setAddress(address);
                dataBean.setE_name(city_name);
                dataBean.setCity(city_name);
                dataBean.setCity_arabicname(city_name);
                addressModel.setLand_mark(landMark);
                dataBean.setLand_mark(landMark);
                dataBean.setLat(lat);
                dataBean.setLng(lng);
                addressModel.setUser_id(Integer.parseInt(userId));

                Log.e("LAT LNG 2222 ", "" +  addressModel.getLatitude() + addressModel.getLongitude());


                userAdressesDataBean.add(dataBean);
                addUserAddress(addressModel);
            }
        });

    }


    private void showCities() {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<ShowCitiesResponse> call = projectAPIs.showCities();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<ShowCitiesResponse>() {
            @Override
            public void onResponse(Call<ShowCitiesResponse> call, Response<ShowCitiesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {
                    cities=response.body().getUser_data();
                    ArrayList<String> citiesEnglishNames=new ArrayList<String>();
                    for(int i=0;i<cities.size();i++)
                    {
                        citiesEnglishNames.add(cities.get(i).getA_name());
                    }

                    ArrayAdapter<String> citiesAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_item, citiesEnglishNames);

//                    spinner.setAdapter(citiesAdapter);
//                    spinnerUpdate.setAdapter(citiesAdapter);

                }
                else {
                }
            }
            @Override
            public void onFailure(Call<ShowCitiesResponse> call, Throwable t) {
//                pbar.hide();
            }
        });
    }

    private void getUserAdresses(String userId) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<GetUserAddressesResponse> call = projectAPIs.getUserAddresses(userId);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<GetUserAddressesResponse>() {
            @Override
            public void onResponse(Call<GetUserAddressesResponse> call, Response<GetUserAddressesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                    if (response.body().getMessage().equals("show address data")) {
                        userAdressesDataBean=response.body().getData();
                        adapter = new AdressesAdapter(AdressesActivity.this,userAdressesDataBean,new CustomItemClickListener() {
                            @Override
                            public void onItemClick(View v, int position) {
//                              dialogUpdateOrDelete.show();
                              itemPosition=position;
                            }
                        });

                        recyclerView.setAdapter(adapter);

                        adapter.notifyDataSetChanged();

                        pbar.hide();
                    }


                    else {
                        pbar.hide();
                        Toast.makeText(AdressesActivity.this, getString(R.string.you_dont_have_addresses), Toast.LENGTH_SHORT).show();
                    }
            }

            @Override
            public void onFailure(Call<GetUserAddressesResponse> call, Throwable t) {
//                pbar.hide();
                Toast.makeText(AdressesActivity.this, t.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void addUserAddress(AddAddressModel addressModel) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<AddAddressResponse> call = projectAPIs.addAddressRequest(addressModel);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<AddAddressResponse>() {
            @Override
            public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                if(response.body().getMessage().equals("insert success"))
                {

                    adapter.notifyDataSetChanged();
                    dialogNewAddress.hide();

                    Toast.makeText(AdressesActivity.this,getString( R.string.new_address_successfully_added), Toast.LENGTH_LONG).show();
                }else{
//                    pbar.hide();
                    Toast.makeText(AdressesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
//                pbar.hide();
                Toast.makeText(AdressesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

        public void deleteAddress(String id, final int itemPosition) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<DeleteAddressResponse> call = projectAPIs.deleteAddress(id);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<DeleteAddressResponse>() {
            @Override
            public void onResponse(Call<DeleteAddressResponse> call, Response<DeleteAddressResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {
                    if(response.body().getMessage().equals("delete success")) {
                        Toast.makeText(AdressesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        userAdressesDataBean.remove(itemPosition);
                        adapter.notifyDataSetChanged();
                        dialogUpdateOrDelete.hide();
                    }


                }
            }
            @Override
            public void onFailure(Call<DeleteAddressResponse> call, Throwable t) {
            }
        });
    }


    public  void updateUserAddress(UpdateAddressRequest updateAddress, final int itemPosition) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<UpdateAddressResponse> call = projectAPIs.updateAddress(updateAddress);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<UpdateAddressResponse>() {
            @Override
            public void onResponse(Call<UpdateAddressResponse> call, Response<UpdateAddressResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                    if(response.body().getMessage().equals("update success"))
                    {
                        userAdressesDataBean.get(itemPosition).setLand_mark(editTextLandMarkUpdate.getText().toString());
                        userAdressesDataBean.get(itemPosition).setE_name(city_name);

                        userAdressesDataBean.get(itemPosition).setCity_arabicname(city_name);
                        userAdressesDataBean.get(itemPosition).setAddress(editTextNewAddressUpdate.getText().toString());
                        adapter.notifyDataSetChanged();
                        pbar.hide();
                        dialogUpdateAddress.hide();


                        Toast.makeText(AdressesActivity.this, getString(R.string.addres_successfully_updated), Toast.LENGTH_LONG).show();
                    }else{
                    pbar.hide();
                        Toast.makeText(AdressesActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
            }
            @Override
            public void onFailure(Call<UpdateAddressResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(AdressesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

package com.codecaique.easy1rx.ui.home_activity.notifications_fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.codecaique.easy1rx.ui.OrderDetailsActivity.OrderDetails;
import com.codecaique.easy1rx.ui.offer_details_activity.OfferDetailsActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.Get_whats_app_phone;
import com.codecaique.easy1rx.Models.responses.NotificationResponse;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class NotificationsFragment extends Fragment {
    private OnFragmentInteractionListener listener;
    private RecyclerView recyclerView;
    private Dialog pbar;
    private RecyclerView.LayoutManager layoutManager;
    private NotificationRecyclerAdapter adapter;
    private List<NotificationResponse.NotificationDataBean> notifications;
    private FloatingActionButton buttonWhatsApp;
    ImageButton buttonBack;

    public static NotificationsFragment newInstance() {
        return new NotificationsFragment();
    }
    String whatsAppNumber="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_notifications, container, false);
        buttonWhatsApp= view.findViewById(R.id.buttonWhatsapp);
        buttonBack=(ImageButton)view.findViewById(R.id.buttonBack);
        recyclerView = view. findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        pbar = new Dialog(getContext());
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pbar.setContentView(R.layout.prograss_dialog);
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pbar.setTitle(getString(R.string.please_wait));

        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

        String userId=pref.getString("userId","");
        NetworkConnection connection = new NetworkConnection();

        if (!connection.isOnline()) {
            pbar.show();
            getNotifications(userId);

        } else {
            Toast.makeText(getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();


        }


        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("onClick", "onClick:" );
                Intent intent=new Intent(getContext(), HomeActivity.class);
                startActivity(intent);
            }
        });

        buttonWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                openWhatsApp(view);
                get_whats_app_phone();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }




    private void getNotifications(String userId) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        final ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        String langPref = "Language";
        SharedPreferences prefs = getActivity().getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        Call<NotificationResponse> call = projectAPIs.showNotificaion(userId,language);

        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, final Response<NotificationResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful())
                    if(response.body().getMessage().equals("show all notification")) {
                        notifications=new ArrayList<NotificationResponse.NotificationDataBean>();

                        notifications = response.body().getNotification_data();


                        adapter = new NotificationRecyclerAdapter(getContext(), notifications, new CustomItemClickListener() {
                            @Override
                            public void onItemClick(View v, int position) {
                                if (response.body()!=null)
                                {
                                    if (notifications.get(position).getBody() != null &&
                                            notifications.get(position).getState() != null)
                                    {
//                                        assert notifications.get(position).getState() != null;
                                        if(notifications.get(position).getState().equals("offer"))
                                        {
                                            Log.e("OFFER I",notifications.get(position).getItem_id() + "  " );
                                            Intent intent=new Intent(getContext(), OfferDetailsActivity.class);
                                            intent.putExtra("from","notification");
                                            intent.putExtra("id",""+notifications.get(position).getItem_id());
                                            startActivity(intent);
                                        }
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                            if(notifications != null && Objects.equals(notifications.get(position).getState(), "order"))
                                            {
                                                Log.e("ORDer I",notifications.get(position).getItem_id() + "  " );

                                                Intent intent=new Intent(getContext(), OrderDetails.class);
                                                intent.putExtra("from","notification");
                                                intent.putExtra("id",""+notifications.get(position).getItem_id());
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                    else
                                        Toast.makeText(getContext(), "Notification is a default message from admin", Toast.LENGTH_SHORT).show();
                                }
                                else
                                    Toast.makeText(getContext(), "Notification is a default message from admin", Toast.LENGTH_SHORT).show();

                            }
                        });

                        pbar.hide();

                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();




                    }
                    else{
                        pbar.hide();
                        Toast.makeText(getContext(), getString(R.string.no_new_otifications), Toast.LENGTH_SHORT).show();
                    }
            }
            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void get_whats_app_phone() {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<Get_whats_app_phone> call = projectAPIs.get_whats_app_phone();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<Get_whats_app_phone>() {
            @Override
            public void onResponse(Call<Get_whats_app_phone> call, Response<Get_whats_app_phone> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {

                    Get_whats_app_phone get_whats_app_phone = response.body();
                    String phone=get_whats_app_phone.getAbout_data().getPhone();
                    whatsAppNumber=phone;

                    openWhatsApp(null);

                }
            }
            @Override
            public void onFailure(Call<Get_whats_app_phone> call, Throwable t) {
            }
        });
    }


    public void openWhatsApp(View view){
        PackageManager pm=getActivity().getPackageManager();
        try {


            String toNumber = whatsAppNumber; // Replace with mobile phone number without +Sign or leading zeros, but with country code.
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.

//            toNumber=toNumber.substring(1);
//
//
//            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + "966570133304" +
//                    ""+ "?body=" + ""));
//            sendIntent.setPackage("com.whatsapp");
//            startActivity(sendIntent);

            try {
                // Raise exception if whatsapp doesn't exist
//                get_whats_app_phone();
                String url = "https://api.whatsapp.com/send?phone=" + whatsAppNumber;

//                String url = "https://api.whatsapp.com/send?phone=+966570133304";

                Log.e("URL ssad",url);

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setPackage("com.whatsapp");
                i.setData(Uri.parse(url));
                startActivity(i);


            } catch (Exception e) {
                Toast.makeText(getActivity(), "Please install whatsapp app", Toast.LENGTH_SHORT)
                        .show();
            }

        }
        catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.dont_have_whats_app),Toast.LENGTH_LONG).show();

        }
    }


    public interface OnFragmentInteractionListener {
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

package com.codecaique.easy1rx.ui.OrderDetailsActivity;

import android.app.Dialog;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.codecaique.easy1rx.CustomItemClickListener;
import com.codecaique.easy1rx.Models.responses.Notification_offer_response;
import com.codecaique.easy1rx.Models.responses.Notification_order_response;
import com.codecaique.easy1rx.Models.responses.OrderImagesResponse;
import com.codecaique.easy1rx.Models.responses.PatientOrdersResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.make_order_activity.MakeOrderActivity;
import com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.referred_order.ReferredOrderImagesRecyclerAdapter;
import com.codecaique.easy1rx.ui.offer_details_activity.OfferDetailsActivity;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OrderDetails extends AppCompatActivity implements Serializable {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private OrderImagesRecyclerAdapter adapter;
    ArrayList<String> images;
    PatientOrdersResponse.UserOrdersBean orderResponse;
    Notification_order_response order_response2;
    CircleImageView close;
    TextView id, state,doctorName,description,price,comment,textRoute;
    Button buttonReOrder;
    ImageButton buttonBack,buttonMap;
    Dialog dialogViewImage;
    public static Dialog pbar;
    ZoomageView imageView;

    String longitude,latitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);


        id=(TextView) findViewById(R.id.orderId);
        state=findViewById(R.id.textVieworderStatus);
        doctorName=findViewById(R.id.doctorName);
        description=findViewById(R.id.description);
        price=findViewById(R.id.price);
        comment=findViewById(R.id.Comment);

        textRoute=findViewById(R.id.textRoute);
        buttonMap=findViewById(R.id.buttonMap);
        textRoute.setVisibility(View.GONE);
        buttonMap.setVisibility(View.GONE);

        buttonReOrder=findViewById(R.id.buttonReOrder);
        buttonBack=findViewById(R.id.buttonBack);
        dialogViewImage=new Dialog(OrderDetails.this);
        dialogViewImage.setContentView(R.layout.dialog_image_view);
        imageView=dialogViewImage.findViewById(R.id.imageView);
        close=dialogViewImage.findViewById(R.id.close);

        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this,2);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    protected void onStart() {
        super.onStart();
        images=new ArrayList<String>();

        String from=getIntent().getExtras().getString("from","else");
        if(from.equals("notification"))
        {
            String orederId=getIntent().getExtras().getString("id");
            order_response2=new Notification_order_response();
            pbar = new Dialog(OrderDetails.this,R.style.DialogCustomTheme);
            pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);

            pbar.setContentView(R.layout.prograss_dialog);

            pbar.setTitle(getString(R.string.please_wait));
            Window window = pbar.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            pbar.show();
            getOrder(orederId);

            buttonReOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(OrderDetails.this, MakeOrderActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("my object2", order_response2);
                    intent.putExtra("data2",bundle);
                    intent.putStringArrayListExtra("images",images);

                    Log.e("SIZE image",images.size() + "    ");

                    intent.putExtra("orderType","reOrder2");
                    startActivity(intent);


                }
            });

        }
        else
        {

            orderResponse= (PatientOrdersResponse.UserOrdersBean) getIntent().getBundleExtra("data2").getSerializable("my object2");
            Log.e("order", String.valueOf(orderResponse.getId()));
            id.setText("#"+orderResponse.getId());//+
            Log.e("state", orderResponse.getState()+"" );
            String stateString=String.valueOf(orderResponse.getState());
            state.setText(stateString);
            if(orderResponse.getDoctor_name()!=null) {
                doctorName.setText("Referred By: Dr/" + orderResponse.getDoctor_name());
            }
            else
            {
                doctorName.setVisibility(View.GONE);
            }
            pbar = new Dialog(OrderDetails.this,R.style.DialogCustomTheme);
            pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);

            pbar.setContentView(R.layout.prograss_dialog);

            pbar.setTitle(getString(R.string.please_wait));
            Window window = pbar.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            pbar.show();
            description.setText(orderResponse.getPro_description());


            String pr=orderResponse.getPrice();
            if(pr!=null)
            {
                price.setText(getString(R.string.price)+" "+pr+" "+getString(R.string.pound));
            }
            String lon=orderResponse.getLongitude();
            String lat=orderResponse.getLatitude();
            if(lat!=null)
            {
                longitude=lon;
                latitude=lat;
                buttonMap.setVisibility(View.VISIBLE);
                textRoute.setVisibility(View.VISIBLE);

            }



            adapter=new OrderImagesRecyclerAdapter(this, images, new CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {
                    pbar.show();
                    String orderImagesPath="http://pharmacyapi.codecaique.com/uploads/orders/"+images.get(position);

                    Glide.with(OrderDetails.this).load(orderImagesPath).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            OrderDetails.pbar.hide();
                            Toast.makeText(OrderDetails.this,getString(R.string.network__problems), Toast.LENGTH_SHORT).show();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            OrderDetails.pbar.hide();
                            return false;
                        }
                    }).into(imageView);
                    dialogViewImage.show();
                }
            }, "show");
//            if(images.size()>1)
//            {
//                layoutManager = new GridLayoutManager(this,2);
//            }
//            else
//            {
//                layoutManager=new LinearLayoutManager(OrderDetails.this);
//            }
            recyclerView.setLayoutManager(layoutManager);

            recyclerView.setAdapter(adapter);

            getImages(""+orderResponse.getId());


            buttonReOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(OrderDetails.this, MakeOrderActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("my object2",orderResponse);
                    intent.putExtra("data2",bundle);
                    intent.putStringArrayListExtra("images",images);
                    intent.putExtra("orderType","reOrder");
                    startActivity(intent);


//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("geo:0,0?q=31.2025,30.0395 (" + "Branch 1" + ")"));
//                startActivity(intent);






//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?daddr=31.2025,30.0395"));
//                startActivity(intent);

                }
            });

        }

        buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String my_data= String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr="+latitude+","+longitude+"(My Destination Place)");

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(my_data));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });


        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogViewImage.hide();
            }
        });
    }

    private void getImages(String oredrId) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<OrderImagesResponse> call = projectAPIs.getOrderImages(oredrId);
        /*
        
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<OrderImagesResponse>() {
            @Override
            public void onResponse(Call<OrderImagesResponse> call, Response<OrderImagesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.body().getMessage().equals("show all images of order")) {
                    for(int i=0;i<response.body().getData().size();i++)
                    {
                        images.add(response.body().getData().get(i).getImage());
                    }
                    adapter.notifyDataSetChanged();
                    comment.setText(response.body().getComments());
                }
            }
            @Override
            public void onFailure(Call<OrderImagesResponse> call, Throwable t) {
//                pbar.hide();
//                Toast.makeText(SignInActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    private void getOrder(String orderId) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<Notification_order_response> call = projectAPIs.showNotificaionOfOrder(orderId,"order");
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<Notification_order_response>() {
            @Override
            public void onResponse(Call<Notification_order_response> call, Response<Notification_order_response> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.body().getMessage().equals("show successfully")) {

                    order_response2=response.body();
                    id.setText("#"+response.body().getData().get(0).getId());//+
                    Log.e("state", response.body().getData().get(0).getState()+"" );
                    String stateString=String.valueOf(response.body().getData().get(0).getState());
                    state.setText(stateString);
                    if(response.body().getData().get(0).getDoctor_name()!=null) {
                        doctorName.setText("Referred By: Dr/" + response.body().getData().get(0).getDoctor_name());
                    }
                    else
                    {
                        doctorName.setVisibility(View.GONE);
                    }
                    description.setText(response.body().getData().get(0).getPro_description());
                    price.setText(getString(R.string.price)+" "+response.body().getData().get(0).getPrice()+" "+getString(R.string.pound));

                    String lon=response.body().getData().get(0).getLongitude();
                    String lat=response.body().getData().get(0).getLatitude();
                    if(lat!=null)
                    {
                        longitude=lon;
                        latitude=lat;
                        buttonMap.setVisibility(View.VISIBLE);
                        textRoute.setVisibility(View.VISIBLE);
                    }


                    for(int i=0;i<response.body().getImages().size();i++)
                    {
                        images.add(response.body().getImages().get(i).getImage());
                    }
                    adapter=new OrderImagesRecyclerAdapter(OrderDetails.this, images, new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position) {
                            String orderImagesPath="http://pharmacyapi.codecaique.com/uploads/orders/"+images.get(position);

                            Glide.with(OrderDetails.this).load(orderImagesPath).into(imageView);
                            dialogViewImage.show();
                        }
                    }, "show");

                    recyclerView.setAdapter(adapter);



                }
            }
            @Override
            public void onFailure(Call<Notification_order_response> call, Throwable t) {
//                pbar.hide();
//                Toast.makeText(SignInActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

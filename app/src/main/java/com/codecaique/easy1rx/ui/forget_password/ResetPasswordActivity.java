package com.codecaique.easy1rx.ui.forget_password;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.UpdateAddressResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.signin_activity.SignInActivity;
import com.codecaique.easy1rx.ui.signup_activity.Verification_Actitvity;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ResetPasswordActivity extends AppCompatActivity {
    Bundle bundle ;
    EditText phone_et,code_et, password_et;
    Button done;
    Dialog pbar;
    String txt_code,phone_num,code_fire,code_background,code;
    FirebaseAuth mAuth;
    CountryCodePicker ccp;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        bundle=getIntent().getExtras();
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        phone_et = findViewById(R.id.login_et_email);
        ccp.registerPhoneNumberTextView(phone_et);

        code_et = findViewById(R.id.et_code);
        password_et = findViewById(R.id.login_et_password);
        done = findViewById(R.id.done);
        txt_code=bundle.getString("txt");
        phone_num=bundle.getString("phone");
        mAuth=FirebaseAuth.getInstance();

        if (!(txt_code.equals("1"))) {
            otp_method();
        }
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String phone = phone_et.getText().toString();
                phone=phone.replace(" ","");
                phone=phone.trim();
                code = code_et.getText().toString();
                String password = password_et.getText().toString();

                if(phone.equals("")){
                    phone_et.setError(getString(R.string.error_enter_your_phone));
                }else if(phone.length()>9 || phone.length()<9){
                    phone_et.setError(getString(R.string.phone_correct));
                }else if(phone.startsWith("0")){
                    phone_et.setError(getString(R.string.phone_correct));
                }
                else if(password.equals("")) {
                    password_et.setError(getString(R.string.input_password));
                }else if(code.equals("")){
                    code_et.setError(getString(R.string.enter_the_code));
                }else{
                    pbar = new Dialog(ResetPasswordActivity.this);
                    pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    pbar.setContentView(R.layout.prograss_dialog);
                    Window window = pbar.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    pbar.setTitle(getString(R.string.please_wait));
                    pbar.show();

                    phone_num=phone_num.replace("+966","");
                    phone=phone_num;
                        code_background=bundle.getString("code");
                        if(code.equals(code_fire)) {
                            if(!txt_code.equals("1")) {
                            if (!phone.equals(phone_num)) {
                                Toast.makeText(ResetPasswordActivity.this, getString(R.string.phone_correct), Toast.LENGTH_SHORT).show();
                            } else {
                                resetPassword(phone, code_background, password);
                            }
                        }else {
                                resetPassword(phone, code_background, password);
                            }
                    }else {
                            pbar.hide();
                            Toast.makeText(ResetPasswordActivity.this, getString(R.string.correct_code), Toast.LENGTH_SHORT).show();
                        }
                }

            }
        });

    }

    void resetPassword(String phone,String code_background, String password)
    {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        ProjectAPIs apIs = retrofit.create(ProjectAPIs.class);

        Call<UpdateAddressResponse> call = apIs.resetPassword(
                RequestBody.create(MediaType.parse("multipart/form-data"), phone),
                RequestBody.create(MediaType.parse("multipart/form-data"), code_background),
                RequestBody.create(MediaType.parse("multipart/form-data"), password)
        );

        call.enqueue(new Callback<UpdateAddressResponse>() {
            @Override
            public void onResponse(Call<UpdateAddressResponse> call, Response<UpdateAddressResponse> response) {
                assert response.body() != null;
                if (response.body().getError() == 0) {
                    pbar.hide();
                    Toast.makeText(ResetPasswordActivity.this, "Resetting password is Done", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ResetPasswordActivity.this, SignInActivity.class));
                }
                else
                {
                    pbar.hide();
                    Toast.makeText(ResetPasswordActivity.this, response.body().getMessage()
                            , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UpdateAddressResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(ResetPasswordActivity.this, "Connection Error" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void otp_method() {
        //  pbar.show();
        phone_num=bundle.getString("phone");
        phone_num="+966"+phone_num;
        PhoneAuthOptions options = PhoneAuthOptions.newBuilder(mAuth)
                .setPhoneNumber(phone_num)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(this)
                .setCallbacks(new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onCodeSent(String verificationId,
                                           PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        // Save the verification id somewhere
                        // ...


                        // The corresponding whitelisted code above should be used to complete sign-in.

                    }

                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                        // Sign in with the credential
                        // ...
                     //   pbar.hide();
                        Toast.makeText(ResetPasswordActivity.this, "لقد ارسلنا اليك كود التفعيل"
                                , Toast.LENGTH_SHORT).show();
                        code_fire=phoneAuthCredential.getSmsCode();
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        // ...
                //        pbar.hide();
                        Log.e("error",e.getMessage());

                    }
                })
                .build();
        PhoneAuthProvider.verifyPhoneNumber(options);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

package com.codecaique.easy1rx.ui.forget_password;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.UpdateAddressResponse;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.signup_activity.SignUpActivity;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForgetPasswordActivity extends AppCompatActivity {

    Dialog pbar;
    String txt;
    CountryCodePicker ccp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        EditText editText=findViewById(R.id.login_et_email);
        ccp.registerPhoneNumberTextView(editText);
        Button button=findViewById(R.id.done);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone=editText.getText().toString();
                phone=phone.replace(" ","");
                phone=phone.trim();
                if (phone.equals("")){
                    editText.setError(getString(R.string.error_enter_your_phone));
                }else if(phone.startsWith("0")){
                    editText.setError(getString(R.string.phone_correct));
                }
                else if(phone.length()>9 || phone.length()<9){
                    editText.setError(getString(R.string.phone_correct));
                } else {

                    pbar = new Dialog(ForgetPasswordActivity.this);
                    pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    pbar.setContentView(R.layout.prograss_dialog);
                    Window window = pbar.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    pbar.setTitle(getString(R.string.please_wait));
                    pbar.show();
                    forgetPassword(phone);
                }
            }
        });

        TextView have_code = findViewById(R.id.have_code);
        have_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 txt="1";
                Intent intent=new Intent(ForgetPasswordActivity.this,ResetPasswordActivity.class);
                intent.putExtra("txt",txt);
                startActivity(intent);
            }
        });
    }

    void forgetPassword(String phone)
    {
        Retrofit retrofit = NetworkClient.getRetrofitClient();

        ProjectAPIs apIs = retrofit.create(ProjectAPIs.class);

        Call<UpdateAddressResponse> call = apIs.forgetPassword(
                RequestBody.create(MediaType.parse("multipart/form-data"), phone)
        );
        
        call.enqueue(new Callback<UpdateAddressResponse>() {
            @Override
            public void onResponse(Call<UpdateAddressResponse> call, Response<UpdateAddressResponse> response) {
                assert response.body() != null;
                if (response.body().getError() == 0)
                {
                    pbar.hide();
                      txt="0";
                    Intent intent=new Intent(ForgetPasswordActivity.this,ResetPasswordActivity.class);
                    intent.putExtra("phone",phone);
                    intent.putExtra("txt",txt);
                    intent.putExtra("code",response.body().getCode());
                    startActivity(intent);
                   // Toast.makeText(ForgetPasswordActivity.this, "The code is sent to ur phone", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<UpdateAddressResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(ForgetPasswordActivity.this, "Connection Error "+ t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }


}

package com.codecaique.easy1rx.ui.splash_activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.UserModel;
import com.codecaique.easy1rx.Models.responses.SignInResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.codecaique.easy1rx.ui.signin_activity.SignInActivity;
import com.codecaique.easy1rx.ui.signup_activity.Verification_Actitvity;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SplashActivity extends AppCompatActivity {

    SignInResponse.UserDataBean userDataBean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_splash);

        userDataBean=new SignInResponse.UserDataBean();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                boolean signrState=pref.getBoolean("SignState",false);
                boolean verification = pref.getBoolean("verification",false);
                 if(signrState==true) {
                     if (verification == true)
                     {
                         String username = pref.getString("username","");
                         String password = pref.getString("password","");
                         String firebaseToken= FirebaseInstanceId.getInstance().getToken();
                         userDataBean.setFirebase_token(firebaseToken);
                         userDataBean.setPhone(username);
                         userDataBean.setPasswords(password);
                         sigInUser(userDataBean);
                     }
                     else
                     {
                         Intent intent = new Intent(getApplicationContext(), Verification_Actitvity.class);
//                         intent.putExtra("phone",Phone_num);
//                         intent.putExtra("action","r");
//                         intent.putExtra("code",response.body().getCode());
                         startActivity(intent);
                     }
                 }
                 else {
                     Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                     startActivity(intent);
                 }
            }
        }, 1000);
    }

    private void sigInUser(SignInResponse.UserDataBean userDataBean) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<SignInResponse> call = projectAPIs.signInUser(userDataBean);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
               // assert response.body() != null;
                if(response.body()!=null && response.body().getMessage().equals("login success"))
                {
                    SignInResponse.UserDataBean user_data= response.body().getUser_data();
                    UserModel userModel=new UserModel();
                    userModel.setUser_id(user_data.getId());
                    userModel.setFirst_name(user_data.getFirst_name());
                    userModel.setLast_name(user_data.getLast_name());
                    userModel.setPhone(user_data.getPhone());
                    userModel.setPassword(user_data.getPasswords());
                    userModel.setEmail(user_data.getEmail());
                    userModel.setLand_mark(user_data.getLand_mark());
                    userModel.setGender(user_data.getGender());
                    userModel.setAddress(user_data.getAddress());

                    userModel.setState(user_data.getState());

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("userId", ""+user_data.getId());
                    editor.putString("userState", response.body().getUser_data().getState());
                    editor.putBoolean("SignState", true);
                    editor.putBoolean("verification",true);
                    editor.putString("username",response.body().getUser_data().getPhone());
                    editor.putString("password",response.body().getUser_data().getPasswords());
                    editor.commit();

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);

//                    Intent intent=new Intent(SignInActivity.this, HomeActivity.class);
//                    startActivity(intent);

                }
//                else{
//                    Toast.makeText(SignInActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                }
            }
            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
//                Toast.makeText(SignInActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    public void loadLocale() {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "ar");
        changeLang(language);
    }
    public  void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        Locale myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());

    }
    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

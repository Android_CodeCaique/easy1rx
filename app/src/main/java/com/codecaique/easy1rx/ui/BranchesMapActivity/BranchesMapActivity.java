package com.codecaique.easy1rx.ui.BranchesMapActivity;

import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.codecaique.easy1rx.Models.responses.ShowBranchesResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.new_order.PatientNewOrderFragment;
import com.codecaique.easy1rx.ui.make_order_activity.patient_fragments.referred_order.PatientReferredOrderFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BranchesMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    ArrayList<LatLng> markers;
    List<ShowBranchesResponse.BranchsDataBean> branchsDataBeans;

    private Map<Marker, Integer> mMarker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branches_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        getBranches();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                String type=getIntent().getExtras().getString("type");
                if(type.equals("new")) {
                    PatientNewOrderFragment.textViewStoreName.setText(marker.getTitle());

                    Integer index = mMarker.get(marker);

                    Log.e("INDEX",""+index);

                    for (ShowBranchesResponse.BranchsDataBean marker2: branchsDataBeans) {

                        if (String.valueOf(marker.getPosition().longitude).equals(marker2.getLongitude()))
                        {
                            PatientNewOrderFragment.branchId=marker2.getId();
                            PatientNewOrderFragment.textViewStoreName.setText(marker2.getAddress());
                            Log.e("MARKER m",marker.getPosition()+"");
                            Log.e("BranchID ", marker2.getId()+ "   ");
                        }

                    }
                    PatientNewOrderFragment.textViewStoreName.setVisibility(View.VISIBLE);
                    PatientNewOrderFragment.buttonMakeOrder.setVisibility(View.VISIBLE);
                    finish();
                }
                else if(type.equals("referred"))
                {

                    for (ShowBranchesResponse.BranchsDataBean marker2: branchsDataBeans) {

                        if (String.valueOf(marker.getPosition().longitude).equals(marker2.getLongitude()))
                        {
                            PatientReferredOrderFragment.branchId=marker2.getId();
                            PatientReferredOrderFragment.textViewStoreName.setText(marker2.getAddress());
                            Log.e("MARKER m",marker.getPosition()+"");
                        }

                    }
//                    PatientReferredOrderFragment.textViewStoreName.setText(marker.getTitle());
                    PatientReferredOrderFragment.textViewStoreName.setVisibility(View.VISIBLE);
                    PatientReferredOrderFragment.buttonMakeOrder.setVisibility(View.VISIBLE);
                    finish();
                }

                return false;
            }
        });

    }
    private void getBranches() {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<ShowBranchesResponse> call = projectAPIs.getBranches();
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<ShowBranchesResponse>() {
            @Override
            public void onResponse(Call<ShowBranchesResponse> call, Response<ShowBranchesResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.body().getMessage().equals("show all branches")) {
                    branchsDataBeans=new ArrayList<>();
                    branchsDataBeans=response.body().getBranchs_data();
                    markers= new ArrayList<>();
                    mMarker = new HashMap();
                    for(int i=0;i<response.body().getBranchs_data().size();i++) {
                        // Add a marker in Sydney and move the camera
                        if((response.body().getBranchs_data().get(i).getLatitude()!=null)&&( response.body().getBranchs_data().get(i).getLongitude()!=null))
                        {
                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(response.body().getBranchs_data().get(i).getLatitude())
                                    , Double.parseDouble(response.body().getBranchs_data().get(i).getLongitude()))));
                            markers.add(new LatLng(Double.parseDouble(response.body().getBranchs_data().get(i).getLatitude())
                                    , Double.parseDouble(response.body().getBranchs_data().get(i).getLongitude())));
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(response.body().getBranchs_data().get(i).getLatitude())
                                    , Double.parseDouble(response.body().getBranchs_data().get(i).getLongitude())), 15));

                            Marker marker =mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(response.body().getBranchs_data().get(i).getLatitude())
                                    , Double.parseDouble(response.body().getBranchs_data().get(i).getLongitude()))));

                            mMarker.put(marker,i);

                        }

                    }


                    if (!markers.isEmpty())
                    {
                        Log.e("BOUND","DONE Non empty");
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (LatLng marker : markers) {
                            builder.include(marker);
                        }
                        LatLngBounds bounds = builder.build();
                        int padding = 0; // offset from edges of the map in pixels
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

                        mMap.moveCamera(CameraUpdateFactory.zoomIn());
                        mMap.moveCamera(cu);
                    }

                }
            }
            @Override
            public void onFailure(Call<ShowBranchesResponse> call, Throwable t) {
//                pbar.hide();
//                Toast.makeText(SignInActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }
}

package com.codecaique.easy1rx.ui.signup_activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.Registerationresponse;
import com.codecaique.easy1rx.Models.responses.SignInResponse;
import com.codecaique.easy1rx.NetworkConnection;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.ui.home_activity.HomeActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Verification_Actitvity extends AppCompatActivity {

    EditText code_et;
    Button done;
    Dialog pbar;
    FirebaseAuth mAuth;
    String verificationmId;
    PhoneAuthCredential credential;
    PhoneAuthProvider.ForceResendingToken token;
    MultipartBody.Part filesToUpload=null;

    Bundle bundle ;
     String phone;
     String action;
     String code_background;
    String code_fire,code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification_actitvity);
        bundle = getIntent().getExtras();
        mAuth=FirebaseAuth.getInstance();
        code_et = findViewById(R.id.et_code);
        done = findViewById(R.id.done);

        pbar = new Dialog(Verification_Actitvity.this);
        pbar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pbar.setContentView(R.layout.prograss_dialog);
        Window window = pbar.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pbar.setTitle(getString(R.string.please_wait));

        otp_method();



        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 code = code_et.getText().toString();
                if (!code.isEmpty())
                {
                   pbar.show();
                    credential = PhoneAuthProvider.getCredential(verificationmId,code);
                    verifyAuth(credential);

                }else {
                    code_et.setError(getString(R.string.correct_code));
                }

            }
        });


    }

    private void otp_method() {
      //  pbar.show();
        phone =bundle.getString("phone");
        phone="+2"+phone;
        PhoneAuthOptions options = PhoneAuthOptions.newBuilder(mAuth)
                .setPhoneNumber(phone)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(this)
                .setCallbacks(new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onCodeSent(String verificationId,
                                           PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        Toast.makeText(Verification_Actitvity.this, "لقد ارسلنا اليك كود التفعيل"
                                , Toast.LENGTH_SHORT).show();
                        verificationmId = verificationId;
                        token = forceResendingToken;

                    }

                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                        // Sign in with the credential
                        // ...
                        pbar.hide();

                        code_fire=phoneAuthCredential.getSmsCode();
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        // ...
                        pbar.hide();
                        Log.e("error",e.getMessage());

                    }

                }).build();
        PhoneAuthProvider.verifyPhoneNumber(options);

    }

    private void make_Register(String firstname, String lastname, String password,
                               MultipartBody.Part filesToUpload, String phone_num,
                               String email, String gender) {

        NetworkConnection connection = new NetworkConnection();

        if (!connection.isOnline()) {



            RegisterUser(firstname, lastname, password, filesToUpload, phone_num, email, gender);

        } else {
             pbar.hide();
            Toast.makeText(Verification_Actitvity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();

        }
    }

    private void RegisterUser(String firstname, String lastname, String passwords,  MultipartBody.Part image,  String Phone_num, String email, String gender) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        String firebaseToken = FirebaseInstanceId.getInstance().getToken();

        Call<Registerationresponse> call = projectAPIs.registerUser(
                RequestBody.create(MediaType.parse("multipart/form-data"), firstname),
                RequestBody.create(MediaType.parse("multipart/form-data"), lastname),
                RequestBody.create(MediaType.parse("multipart/form-data"), passwords),
                image,
                RequestBody.create(MediaType.parse("multipart/form-data"), Phone_num),
                RequestBody.create(MediaType.parse("multipart/form-data"), email),
                RequestBody.create(MediaType.parse("multipart/form-data"), gender),
                RequestBody.create(MediaType.parse("multipart/form-data"), firebaseToken ));
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<Registerationresponse>() {
            @Override
            public void onResponse(Call<Registerationresponse> call, Response<Registerationresponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if (response.isSuccessful()) {
                    if (response.body().getError() == 0) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
//                        Toast.makeText(SignUpActivity.this, "id" + response.body().getUser_data().getId(), Toast.LENGTH_SHORT).show();
                        editor.putString("userId", "" + response.body().getUser_data().getId());
                        editor.putString("userState", "" + response.body().getUser_data().getState());
                        editor.putString("phone",Phone_num);
                        editor.putString("action","r");
                        editor.putBoolean("SignState", false);
                        editor.putBoolean("verification",false);
                        editor.commit();



                        if (bundle != null)
                        {
                            phone = bundle.getString("phone");
                            action = bundle.getString("action");

                            verifyCode(response.body().getCode(),phone,action);
                        }
                        else {
                            phone = pref.getString("phone","");
                            action = pref.getString("action","");
                   //        code_background = bundle.getString("code");
                            verifyCode(response.body().getCode(),phone,action);
                        }




                    }
                    else {
                        pbar.hide();

                        Toast.makeText(Verification_Actitvity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("onResponse", response.body().getMessage());
                    }

                }
            }

            @Override
            public void onFailure(Call<Registerationresponse> call, Throwable t) {
                pbar.hide();
                Log.e("failure", t.getMessage());

                Toast.makeText(Verification_Actitvity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }












    void verifyCode(String code_background,String phone,String action)
    {
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        ProjectAPIs apIs = retrofit.create(ProjectAPIs.class);

        Call<SignInResponse> call = apIs.verifyPhone(
                RequestBody.create(MediaType.parse("multipart/form-data"), phone),
                RequestBody.create(MediaType.parse("multipart/form-data"), code_background),
                RequestBody.create(MediaType.parse("multipart/form-data"), action)
        );

        call.enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                if (response.body().getError() == 0) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    pbar.hide();


                        editor.putBoolean("verification", true);
                        Toast.makeText(Verification_Actitvity.this, "Account Verification is Done", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Verification_Actitvity.this, HomeActivity.class));

                }
                else
                {
                    pbar.hide();
                    Toast.makeText(Verification_Actitvity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignInResponse> call, Throwable t) {
                pbar.hide();
                Toast.makeText(Verification_Actitvity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getName(), "CurrentScreen");

    }

    private void verifyAuth(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    phone = bundle.getString("phone");
                    String fname= bundle.getString("firstname");
                    String lname =bundle.getString("lastname");
                    String p =   bundle.getString("password");
                    url_fun(getIntent().getData());
                    String e =   bundle.getString("email");
                    String g  =  bundle.getString("gender");
                    make_Register(fname,lname,p,filesToUpload,phone,e,g);
                }else {
                    pbar.hide();
                    Toast.makeText(Verification_Actitvity.this, "Can not Verify phone", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void url_fun(Uri uri){

        MultipartBody.Part body = null;
        if (uri != null) {
            Cursor cursor = null;
            String p = null;
            try {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = getContentResolver().query(uri, proj, null, null, null);
                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                p = cursor.getString(column_index);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            File file1 = new File(p);
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file1);
            body = MultipartBody.Part.createFormData("image", file1.getName(), requestFile);

            filesToUpload=(body);

        }


    }


}

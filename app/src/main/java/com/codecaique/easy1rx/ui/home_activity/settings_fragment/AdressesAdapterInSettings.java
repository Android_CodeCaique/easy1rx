package com.codecaique.easy1rx.ui.home_activity.settings_fragment;

/**
 * Created by I Love Allah on 06/10/2018.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codecaique.easy1rx.Models.responses.GetOredrIdResponse;
import com.codecaique.easy1rx.Models.responses.GetUserAddressesResponse;
import com.codecaique.easy1rx.R;
import com.codecaique.easy1rx.data.NetworkClient;
import com.codecaique.easy1rx.data.ProjectAPIs;
import com.codecaique.easy1rx.CustomItemClickListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class AdressesAdapterInSettings extends RecyclerView.Adapter<AdressesAdapterInSettings.ViewHolder> {

    List<GetUserAddressesResponse.DataBean> data;

    Context mContext;
    CustomItemClickListener listener;

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.addresses_item_in_settings, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

//        TextView textViewAddress=holder.textViewAddress;
//        TextView textViewCity=holder.textViewcity;
//        TextView textViewLandMark=holder.textViewLandmark;

        holder.textViewAddress.setText(data.get(position).getAddress());
        holder.textViewcity.setText(data.get(position).getCity_arabicname());
        holder.textViewLandmark.setText(data.get(position).getLand_mark());

//        Log.e("ADAPTER ADDRESS", data.get(position).getCity_arabicname());


//        if(data.get(position).getState().equals("canceled"))
//        {
//            imageButtonoButtondelete.setVisibility(View.INVISIBLE);
//        }
//        else{
//            imageButtonoButtondelete.setVisibility(View.VISIBLE);
//
//        }




//        String state=data.get(position).getState();
//        if(state.equals("accept"))
//        {
//            textViewStatus.setTextColor(R.color.colorAccent);
//        }
//        else if(state.equals("receive"))
//        {
//            textViewStatus.setTextColor(R.color.colorBlack);
//        }
//        else if(state.equals("refused"))
//        {
//            textViewStatus.setTextColor(R.color.colorGray);
//        }



    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public AdressesAdapterInSettings(Context mContext, List<GetUserAddressesResponse.DataBean> data, CustomItemClickListener listener) {
        this.data = data;
        this.mContext = mContext;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewAddress;
        TextView textViewcity;
        TextView textViewLandmark;

        ViewHolder(View v) {
            super(v);

            textViewAddress = v
                    .findViewById(R.id.textViewAddress);

            textViewcity = v
                    .findViewById(R.id.textViewCity);
            textViewLandmark = v
                    .findViewById(R.id.textViewLandMark);

        }


    }



    private void CancelOrder(String orderId, final int position) {

        //Obtain an instance of Retrofit by calling the static method.
        Retrofit retrofit = NetworkClient.getRetrofitClient();
        /*
        The main purpose of Retrofit is to create HTTP calls from the Java interface based on the annotation associated with each method. This is achieved by just passing the interface class as parameter to the create method
        */
        ProjectAPIs projectAPIs = retrofit.create(ProjectAPIs.class);
        /*
        Invoke the method corresponding to the HTTP request which will return a Call object. This Call object will used to send the actual network request with the specified parameters
        */
        Call<GetOredrIdResponse> call = projectAPIs.cancelOrder(orderId);
        /*
        This is the line which actually sends a network request. Calling enqueue() executes a call asynchronously. It has two callback listeners which will invoked on the main thread
        */
        call.enqueue(new Callback<GetOredrIdResponse>() {
            @Override
            public void onResponse(Call<GetOredrIdResponse> call, Response<GetOredrIdResponse> response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                if(response.isSuccessful()) {
                    if(response.body().getMessage().equals("order canceled successfully")) {
                  Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        data.remove(position);
                        notifyDataSetChanged();
                    }


                }
            }
            @Override
            public void onFailure(Call<GetOredrIdResponse> call, Throwable t) {
            }
        });
    }



}

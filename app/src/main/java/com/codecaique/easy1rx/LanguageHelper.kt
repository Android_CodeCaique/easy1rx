package com.codecaique.easy1rx

import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import androidx.annotation.RequiresApi
import java.util.*

class LanguageHelper {

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun ChangeLang(resources: Resources, locale1: String?) {
        val config = Configuration(resources.configuration)
        when (locale1) {
            "ar" -> {
                val locale = Locale(locale1)
                config.locale = Locale("ar")
                config.setLayoutDirection(locale)
            }
            "en" -> {
                val locale2 = Locale(locale1)
                config.locale = Locale("en")
                config.setLayoutDirection(locale2)
            }
        }
        resources.updateConfiguration(config, resources.getDisplayMetrics())
    }
}
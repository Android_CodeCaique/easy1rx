package com.codecaique.easy1rx.data;

import com.codecaique.easy1rx.Models.UserState_Response;
import com.codecaique.easy1rx.Models.responses.AddAddressResponse;
import com.codecaique.easy1rx.Models.responses.AskDeliveryResponse;
import com.codecaique.easy1rx.Models.responses.CancelOrderResponse;
import com.codecaique.easy1rx.Models.responses.CheckOrderResponse;
import com.codecaique.easy1rx.Models.responses.ClinicResponse;
import com.codecaique.easy1rx.Models.responses.DeleteAddressResponse;
import com.codecaique.easy1rx.Models.responses.DoctorMakeOrderResponse;
import com.codecaique.easy1rx.Models.responses.GetUserAddressesResponse;
import com.codecaique.easy1rx.Models.responses.Get_whats_app_phone;
import com.codecaique.easy1rx.Models.responses.NotificationResponse;
import com.codecaique.easy1rx.Models.responses.Notification_offer_response;
import com.codecaique.easy1rx.Models.responses.Notification_order_response;
import com.codecaique.easy1rx.Models.responses.OrderImagesResponse;
import com.codecaique.easy1rx.Models.responses.PatientMakeOrderResponse;
import com.codecaique.easy1rx.Models.responses.ShowBranchesResponse;
import com.codecaique.easy1rx.Models.responses.ShowCitiesResponse;
import com.codecaique.easy1rx.Models.responses.UpdateAddressResponse;
import com.codecaique.easy1rx.Models.rquests.AddAddressModel;
import com.codecaique.easy1rx.Models.rquests.AskDeleveryModel;
import com.codecaique.easy1rx.Models.rquests.DoctorMakeorderModel;
import com.codecaique.easy1rx.Models.responses.GetOredrIdResponse;
import com.codecaique.easy1rx.Models.responses.PatientOrdersResponse;
import com.codecaique.easy1rx.Models.responses.PharmacyResponse;
import com.codecaique.easy1rx.Models.responses.ProfileResponse;
import com.codecaique.easy1rx.Models.responses.Registerationresponse;
import com.codecaique.easy1rx.Models.responses.SignInResponse;
import com.codecaique.easy1rx.Models.rquests.PatentMakeOrderModel;
import com.codecaique.easy1rx.Models.rquests.UpdateAddressRequest;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ProjectAPIs {
    /*
    Get request to fetch city weather.Takes in two parameter-city name and API key.
    */

    @GET("show_pharmacy")
    Call<PharmacyResponse> getPhamacyOffers();

    @GET("check_state")
    Call<UserState_Response> getState(@Query("user_id") String user_id);

    @GET("show_clinic")
    Call<ClinicResponse> getClinicOffers();

    @GET("show_about")
    Call<Get_whats_app_phone> get_whats_app_phone();

    @GET("user_address")
    Call<GetUserAddressesResponse> getUserAddresses(@Query("user_id") String user_id);

    @GET("user_orders")
    Call<PatientOrdersResponse> getPatientOrders(@Query("user_id") String user_id);

    @GET("show_user")
    Call<ProfileResponse> getUserData(@Query("user_id") String user_id);

    @GET("make_Order")
    Call<GetOredrIdResponse> getOrderId(@Query("user_id") String user_id);

    @GET("cancel_orders")
    Call<GetOredrIdResponse> cancelOrder(@Query("order_id") String orderId);

    @GET("user_checkorder")
    Call<CheckOrderResponse> checkReferredOrder(@Query("user_id") String user_id, @Query("order_id") String order_id);


    @GET("delete_address")
    Call<DeleteAddressResponse> deleteAddress(@Query("id") String id);

    @GET("show_orderimages")
    Call<OrderImagesResponse> getOrderImages(@Query("order_id") String order_id);

    @GET("show_cities")
    Call<ShowCitiesResponse> showCities();

    @POST("Add_address")
    Call<AddAddressResponse>
    addAddressRequest(@Body AddAddressModel addressModel);

    @Multipart
    @POST("change_language")
    Call<CancelOrderResponse> change_language(@Part("user_id") RequestBody user_id,
                                              @Part("lang") RequestBody lang);


    @POST("update_address")
    Call<UpdateAddressResponse>
    updateAddress(@Body UpdateAddressRequest updateAddress);



    @Multipart
    @POST("regesteration")
    Call<Registerationresponse> registerUser(@Part("first_name") RequestBody first_name,
                                             @Part("last_name") RequestBody last_name,
                                             @Part("passwords") RequestBody passwords,
                                             @Part  MultipartBody.Part image,
                                             @Part("phone") RequestBody phone,
                                             @Part("email") RequestBody email,
                                             @Part ("gender")RequestBody gender,
                                             @Part ("firebase_token")RequestBody firebase_token);

    @POST("login")
    Call<SignInResponse>
    signInUser(@Body SignInResponse.UserDataBean userDataBean);



    @Multipart
    @POST("update_profile")
    Call<UpdateAddressResponse> updateProfile(@Part("user_id")  int user_id,
                                              @Part("first_name") RequestBody first_name,
                                              @Part("last_name") RequestBody last_name,
                                              @Part("password") RequestBody password,
                                              @Part MultipartBody.Part image,
                                              @Part("phone") RequestBody phone,
                                              @Part("email") RequestBody email,
                                              @Part ("gender")RequestBody gender,
                                              @Part("reality_pro") RequestBody reality_pro);
    @POST("make_doctorOrderdetails")
    Call<DoctorMakeOrderResponse> makeDoctorOrder(@Body DoctorMakeorderModel.order_data order_data);

    @POST("make_userOrderdetails")
    Call<PatientMakeOrderResponse> makepatientOrder(@Body PatentMakeOrderModel.order_data order_data);

//    @Multipart
//    @POST("make_userOrderdetails")
//    Call <PatientMakeOrderResponse>uploadimages(@Body("order_id") int order_id,@Part List<MultipartBody.Part> file, @Part("image") List<RequestBody> names);


    @Multipart
    @POST("make_userOrderdetails")
    Call<PatientMakeOrderResponse> makepatientNewOrderPickfromStore(
            @Part("user_id") int user_id,
            @Part List<MultipartBody.Part> image,
            @Part("promo_code") RequestBody promo_code,
            @Part("branch_id") int branch_id,
            @Part("delivery") RequestBody delivery
            ,@Part("longitude") RequestBody longitude
            ,@Part("latitude") RequestBody latitude
    );


    @Multipart
    @POST("make_userOrderdetails")
    Call<PatientMakeOrderResponse> makepatientReferredOrderPickFromStore(
            @Part("order_id") int order_id,
            @Part("promo_code") RequestBody promo_code,
            @Part("branch_id") int branch_id,
            @Part("delivery") RequestBody delivery,
            @Part("refered") RequestBody referred,
            @Part("longitude") RequestBody longitude
            ,@Part("latitude") RequestBody latitude
    );



    @Multipart
    @POST("make_userOrderdetails")
    Call<PatientMakeOrderResponse> makePatientNewOrderNewAddress(
            @Part("user_id") int user_id,
            @Part List<MultipartBody.Part> image,
            @Part("promo_code") RequestBody promo_code,
            @Part("delivery") RequestBody delivery,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city_id,
            @Part("land_mark") RequestBody land_mark,
            @Part("longitude") RequestBody longitude
            ,@Part("latitude") RequestBody latitude
    );


    @Multipart
    @POST("make_userOrderdetails")
    Call<PatientMakeOrderResponse> makePatientReferredOrderNewAddress(
            @Part("order_id") int order_id,
            @Part("promo_code") RequestBody promo_code,
            @Part("delivery") RequestBody delivery,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city_id,
            @Part("land_mark") RequestBody land_mark,
            @Part("refered") RequestBody referred,
            @Part("longitude") RequestBody longitude
            ,@Part("latitude") RequestBody latitude
            );



    @Multipart
    @POST("make_userOrderdetails")
    Call<PatientMakeOrderResponse> makePatientNewOrderSelectedAddress(
            @Part("user_id") int user_id,
            @Part List<MultipartBody.Part> image,
            @Part("promo_code") RequestBody promo_code,
            @Part("address_id") int address_id,
            @Part("delivery") RequestBody delivery,
            @Part("longitude") RequestBody longitude
            ,@Part("latitude") RequestBody latitude
            );

    @Multipart
    @POST("make_userOrderdetails")
    Call<PatientMakeOrderResponse> makePatientReferredOrderSelectedAddress(
            @Part("order_id") int order_id,
            @Part("promo_code") RequestBody promo_code,
            @Part("delivery") RequestBody delivery,
            @Part("address_id") int address_id,
            @Part("refered") RequestBody referred,
            @Part("longitude") RequestBody longitude
            ,@Part("latitude") RequestBody latitude
            );


    @Multipart
    @POST("make_doctorOrderdetails")
    Call<DoctorMakeOrderResponse> makeDoctorOrder2(
            @Part("user_id") int user_id,
            @Part List<MultipartBody.Part> image,
            @Part("name") RequestBody name,
            @Part("phone") RequestBody phone,
            @Part("pro_description") RequestBody pro_description);


    @POST("ask_delivery")
    Call<AskDeliveryResponse> askDelivery(@Body AskDeleveryModel.order_details order_details);


    @GET("show_notification")
    Call<NotificationResponse> showNotificaion(@Query("user_id") String user_id,@Query("lang") String lang);

    @GET("show_Byid")
    Call<Notification_order_response> showNotificaionOfOrder(@Query("id") String id, @Query("state") String state);

    @GET("show_Byid")
    Call<Notification_offer_response> showNotificaionOfOffer(@Query("id") String id, @Query("state") String state);

    @GET("show_branches")
    Call<ShowBranchesResponse> getBranches();

    @Multipart
    @POST("Reset_password")
    Call<UpdateAddressResponse> resetPassword(
            @Part("phone") RequestBody phone,
            @Part("code") RequestBody code,
            @Part("password")RequestBody password
    );


    @Multipart
    @POST("forget_password")
    Call<UpdateAddressResponse> forgetPassword(@Part("phone") RequestBody phone);


    @Multipart
    @POST("phone_verify")
    Call<SignInResponse> verifyPhone(
            @Part("phone") RequestBody phone,
            @Part("code") RequestBody code,
            @Part("action") RequestBody action
    );

}

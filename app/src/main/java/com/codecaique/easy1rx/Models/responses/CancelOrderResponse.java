package com.codecaique.easy1rx.Models.responses;

public class CancelOrderResponse {

    /**
     * error : 0
     * message : order canceled successfully
     */

    private int error;
    private String message;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.codecaique.easy1rx.Models.rquests;

public class AskDeleveryModel {
   private order_details order_details;

    public AskDeleveryModel.order_details getOrder_details() {
        return order_details;
    }

    public void setOrder_details(AskDeleveryModel.order_details order_details) {
        this.order_details = order_details;
    }

    public static class order_details{
        private int order_id;
        private String delivery;
        private String address;

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }

        public String getDelivery() {
            return delivery;
        }

        public void setDelivery(String delivery) {
            this.delivery = delivery;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}

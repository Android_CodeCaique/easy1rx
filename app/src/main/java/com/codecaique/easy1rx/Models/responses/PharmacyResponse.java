package com.codecaique.easy1rx.Models.responses;

import java.io.Serializable;
import java.util.List;

public class PharmacyResponse implements Serializable {
    /**
     * offers_data : [{"id":1,"arabic_name":"gsdgsdg","english_name":"gsggssgsgs","image":"5743251577718543_15747737_1889741774593986_2634081083723701401_n.jpg","price_before":"222","offer_start_date":"2019-10-02","offer_end_date":"2019-10-02","offer_price":"33887","offer_arrange":"2","type":"p","created_at":"2019-12-20 07:32:18","updated_at":"2020-02-04 21:07:02"},{"id":3,"arabic_name":"k","english_name":null,"image":"2028481577717536_bg5.jpg","price_before":"100","offer_start_date":null,"offer_end_date":null,"offer_price":"33887","offer_arrange":"2","type":"p","created_at":"2019-12-20 07:32:18","updated_at":"2019-12-30 16:55:13"},{"id":11,"arabic_name":"ghghj","english_name":"gfhgfhg","image":null,"price_before":null,"offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"p","created_at":"2019-12-21 13:39:17","updated_at":"2019-12-21 11:39:17"},{"id":13,"arabic_name":"fhgfghg","english_name":"hgjh","image":"6619501576928486_64457407_146922786478445_2092562123069587456_n.jpg","price_before":null,"offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"p","created_at":"2019-12-21 13:41:26","updated_at":"2019-12-21 11:41:26"},{"id":15,"arabic_name":"jhgjh","english_name":null,"image":"6505651576928539_64457407_146922786478445_2092562123069587456_n.jpg","price_before":null,"offer_start_date":null,"offer_end_date":null,"offer_price":"200","offer_arrange":"3","type":"p","created_at":"2019-12-21 13:42:19","updated_at":"2019-12-30 16:54:24"},{"id":16,"arabic_name":"gjhgjhg","english_name":"fghgjh","image":null,"price_before":"66","offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"p","created_at":"2019-12-23 10:55:06","updated_at":"2019-12-23 08:55:06"},{"id":17,"arabic_name":"gjhgjhg","english_name":null,"image":"3795521577091384_back.jpg","price_before":"66","offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"p","created_at":"2019-12-23 10:56:24","updated_at":"2019-12-23 08:56:24"},{"id":18,"arabic_name":"gjhgjhg32","english_name":null,"image":"2979971577617653_back.jpg","price_before":"66","offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"p","created_at":"2019-12-29 13:07:33","updated_at":"2019-12-29 11:07:33"},{"id":19,"arabic_name":"gjhgjhg32","english_name":null,"image":"6473041577714846_back.jpg","price_before":"66","offer_start_date":"2019-12-10","offer_end_date":"2020-12-30","offer_price":"200","offer_arrange":"2","type":"p","created_at":"2019-12-30 16:07:26","updated_at":"2019-12-30 14:07:26"},{"id":24,"arabic_name":"aaaa","english_name":null,"image":"4159381577716238_logoCore.png","price_before":"200","offer_start_date":"2020-01-31","offer_end_date":null,"offer_price":"100","offer_arrange":"1","type":"p","created_at":"2019-12-30 16:30:38","updated_at":"2019-12-30 14:30:38"},{"id":25,"arabic_name":"f","english_name":null,"image":"5386221577716314_angular2-logo-red.png","price_before":"1000","offer_start_date":"2019-12-10","offer_end_date":"2020-02-15","offer_price":"10","offer_arrange":"1","type":"p","created_at":"2019-12-30 16:31:54","updated_at":"2019-12-30 14:31:54"},{"id":26,"arabic_name":"mnars","english_name":null,"image":"8374351577776366_bg5.jpg","price_before":"2001","offer_start_date":"2019-12-14","offer_end_date":"2019-12-28","offer_price":"33887","offer_arrange":"55","type":"p","created_at":"2019-12-31 09:12:46","updated_at":"2019-12-31 09:13:50"},{"id":28,"arabic_name":"يسنتات","english_name":"sdjhgjh","image":"2845981578218282_back.jpg","price_before":"66","offer_start_date":"2019-12-10","offer_end_date":"2020-12-30","offer_price":"200","offer_arrange":"2","type":"p","created_at":"2020-01-05 11:58:02","updated_at":"2020-01-05 09:58:02"},{"id":29,"arabic_name":null,"english_name":null,"image":"3113751578489582_500.png","price_before":"100","offer_start_date":"2020-01-05","offer_end_date":"2020-01-25","offer_price":"15","offer_arrange":"1","type":"p","created_at":"2020-01-08 15:19:42","updated_at":"2020-01-08 15:46:23"},{"id":30,"arabic_name":"هههههi","english_name":"hhhhhh","image":"6976561578492865_apple-touch-icon.png","price_before":"100","offer_start_date":"2020-01-01","offer_end_date":"2020-01-31","offer_price":"15","offer_arrange":"3","type":"p","created_at":"2020-01-08 16:14:25","updated_at":"2020-01-08 14:14:26"}]
     * error : 0
     * message : show all offers
     */

    private int error;
    private String message;
    private List<OffersDataBean> offers_data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OffersDataBean> getOffers_data() {
        return offers_data;
    }

    public void setOffers_data(List<OffersDataBean> offers_data) {
        this.offers_data = offers_data;
    }

    public static class OffersDataBean implements Serializable{
        /**
         * id : 1
         * arabic_name : gsdgsdg
         * english_name : gsggssgsgs
         * image : 5743251577718543_15747737_1889741774593986_2634081083723701401_n.jpg
         * price_before : 222
         * offer_start_date : 2019-10-02
         * offer_end_date : 2019-10-02
         * offer_price : 33887
         * offer_arrange : 2
         * type : p
         * created_at : 2019-12-20 07:32:18
         * updated_at : 2020-02-04 21:07:02
         */

        private int id;
        private String arabic_name;
        private String english_name;
        private String image;
        private String price_before;
        private String offer_start_date;
        private String offer_end_date;
        private String offer_price;
        private String offer_arrange;
        private String type;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getArabic_name() {
            return arabic_name;
        }

        public void setArabic_name(String arabic_name) {
            this.arabic_name = arabic_name;
        }

        public String getEnglish_name() {
            return english_name;
        }

        public void setEnglish_name(String english_name) {
            this.english_name = english_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice_before() {
            return price_before;
        }

        public void setPrice_before(String price_before) {
            this.price_before = price_before;
        }

        public String getOffer_start_date() {
            return offer_start_date;
        }

        public void setOffer_start_date(String offer_start_date) {
            this.offer_start_date = offer_start_date;
        }

        public String getOffer_end_date() {
            return offer_end_date;
        }

        public void setOffer_end_date(String offer_end_date) {
            this.offer_end_date = offer_end_date;
        }

        public String getOffer_price() {
            return offer_price;
        }

        public void setOffer_price(String offer_price) {
            this.offer_price = offer_price;
        }

        public String getOffer_arrange() {
            return offer_arrange;
        }

        public void setOffer_arrange(String offer_arrange) {
            this.offer_arrange = offer_arrange;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }

//    private int error;
//    private String message;
//    private List<PharmacyOffersBean> pharmacy_offers;
//
//    public int getError() {
//        return error;
//    }
//
//    public void setError(int error) {
//        this.error = error;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public List<PharmacyOffersBean> getPharmacy_offers() {
//        return pharmacy_offers;
//    }
//
//    public void setPharmacy_offers(List<PharmacyOffersBean> pharmacy_offers) {
//        this.pharmacy_offers = pharmacy_offers;
//    }
//
//    public static class PharmacyOffersBean implements Serializable {
//        /**
//         * id : 30
//         * arabic_name : هههههi
//         * english_name : hhhhhh
//         * image : 6976561578492865_apple-touch-icon.png
//         * price_before : 100
//         * offer_start_date : 2020-01-01
//         * offer_end_date : 2020-01-31
//         * offer_price : 15
//         * offer_arrange : 3
//         * type : p
//         * created_at : 2020-01-08 16:14:25
//         * updated_at : 2020-01-08 14:14:26
//         */
//
//        private int id;
//        private String arabic_name;
//        private String english_name;
//        private String image;
//        private String price_before;
//        private String offer_start_date;
//        private String offer_end_date;
//        private String offer_price;
//        private String offer_arrange;
//        private String type;
//        private String created_at;
//        private String updated_at;
//
//        public int getId() {
//            return id;
//        }
//
//        public void setId(int id) {
//            this.id = id;
//        }
//
//        public String getArabic_name() {
//            return arabic_name;
//        }
//
//        public void setArabic_name(String arabic_name) {
//            this.arabic_name = arabic_name;
//        }
//
//        public String getEnglish_name() {
//            return english_name;
//        }
//
//        public void setEnglish_name(String english_name) {
//            this.english_name = english_name;
//        }
//
//        public String getImage() {
//            return image;
//        }
//
//        public void setImage(String image) {
//            this.image = image;
//        }
//
//        public String getPrice_before() {
//            return price_before;
//        }
//
//        public void setPrice_before(String price_before) {
//            this.price_before = price_before;
//        }
//
//        public String getOffer_start_date() {
//            return offer_start_date;
//        }
//
//        public void setOffer_start_date(String offer_start_date) {
//            this.offer_start_date = offer_start_date;
//        }
//
//        public String getOffer_end_date() {
//            return offer_end_date;
//        }
//
//        public void setOffer_end_date(String offer_end_date) {
//            this.offer_end_date = offer_end_date;
//        }
//
//        public String getOffer_price() {
//            return offer_price;
//        }
//
//        public void setOffer_price(String offer_price) {
//            this.offer_price = offer_price;
//        }
//
//        public String getOffer_arrange() {
//            return offer_arrange;
//        }
//
//        public void setOffer_arrange(String offer_arrange) {
//            this.offer_arrange = offer_arrange;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        public String getCreated_at() {
//            return created_at;
//        }
//
//        public void setCreated_at(String created_at) {
//            this.created_at = created_at;
//        }
//
//        public String getUpdated_at() {
//            return updated_at;
//        }
//
//        public void setUpdated_at(String updated_at) {
//            this.updated_at = updated_at;
//        }
//    }





}

package com.codecaique.easy1rx.Models.rquests;

import android.net.Uri;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.http.Part;


public class PatentMakeOrderModel {
    private order_data order_data;

    public PatentMakeOrderModel.order_data getOrder_data() {
        return order_data;
    }

    public void setOrder_data(PatentMakeOrderModel.order_data order_data) {
        this.order_data = order_data;
    }

    public static class order_data {
        private int order_id;
        List<Uri> image;

        private String promo_code;

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }

        public List<Uri> getImage() {
            return image;
        }

        public void setImage(List<Uri> image) {
            this.image = image;
        }

        public String getPromo_code() {
            return promo_code;
        }

        public void setPromo_code(String promo_code) {
            this.promo_code = promo_code;
        }
    }

}

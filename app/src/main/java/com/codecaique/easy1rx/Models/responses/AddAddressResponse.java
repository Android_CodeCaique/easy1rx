    package com.codecaique.easy1rx.Models.responses;

    public class AddAddressResponse {

        /**
         * data : {"user_id":"2","land_mark":"dsfj gdjhfgjh","address":"h gd fgfds gjgjh dj","city_id":"2","created_at":"2020-02-06 10:53:03","updated_at":"2020-02-06 08:53:03","id":3}
         * error : 0
         * message : insert success
         */

        private DataBean data;
        private int error;
        private String message;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public int getError() {
            return error;
        }

        public void setError(int error) {
            this.error = error;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public static class DataBean {
            /**
             * user_id : 2
             * land_mark : dsfj gdjhfgjh
             * address : h gd fgfds gjgjh dj
             * city_id : 2
             * created_at : 2020-02-06 10:53:03
             * updated_at : 2020-02-06 08:53:03
             * id : 3
             */

            private String user_id;
            private String land_mark;
            private String address;
            private String city_id;
            private String created_at;
            private String updated_at;
            private int id;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getLand_mark() {
                return land_mark;
            }

            public void setLand_mark(String land_mark) {
                this.land_mark = land_mark;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getCity_id() {
                return city_id;
            }

            public void setCity_id(String city_id) {
                this.city_id = city_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }
    }

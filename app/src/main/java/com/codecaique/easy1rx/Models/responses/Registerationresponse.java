package com.codecaique.easy1rx.Models.responses;


public class Registerationresponse {


    /**
     * user_data : {"id":114,"first_name":"fgfdgfdg","last_name":"dddddkjhkjhkj","phone":"7889663214","passwords":"155555","image":"6824901581540504_csharp1.png","state":"user","email":"dsfd ssfs fsd","firebase_token":"22","created_at":"2020-02-12 22:48:24","updated_at":"2020-02-12 13:48:24"}
     * error : 0
     * message : regesteration success
     */

    private UserDataBean user_data;
    private int error;
    private String message;
    private String code;

    public UserDataBean getUser_data() {
        return user_data;
    }

    public void setUser_data(UserDataBean user_data) {
        this.user_data = user_data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class UserDataBean {
        /**
         * id : 114
         * first_name : fgfdgfdg
         * last_name : dddddkjhkjhkj
         * phone : 7889663214
         * passwords : 155555
         * image : 6824901581540504_csharp1.png
         * state : user
         * email : dsfd ssfs fsd
         * firebase_token : 22
         * created_at : 2020-02-12 22:48:24
         * updated_at : 2020-02-12 13:48:24
         */

        private int id;
        private String first_name;
        private String last_name;
        private String phone;
        private String passwords;
        private String image;
        private String state;
        private String email;
        private String firebase_token;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPasswords() {
            return passwords;
        }

        public void setPasswords(String passwords) {
            this.passwords = passwords;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFirebase_token() {
            return firebase_token;
        }

        public void setFirebase_token(String firebase_token) {
            this.firebase_token = firebase_token;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

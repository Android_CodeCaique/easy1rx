package com.codecaique.easy1rx.Models.responses;

import java.io.Serializable;
import java.util.List;

public class CheckOrderResponse implements Serializable {

    /**
     * data : {"order":{"id":13,"doctor_first_name":"kjjdsf","doctor_last_name":"gfhg","doctor_image":"9332591577347705_your_pic_name.png","created_at":"2019-12-09 14:22:46"},"images":[{"id":24,"order_id":"13","image":"3393731577616383_back.jpg","size":null,"created_at":"2019-12-31 05:48:38","updated_at":"2019-12-31 05:48:38"}]}
     * error : 0
     * message : show order data
     */

    private DataBean data;
    private int error;
    private String message;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataBean implements Serializable {
        /**
         * order : {"id":13,"doctor_first_name":"kjjdsf","doctor_last_name":"gfhg","doctor_image":"9332591577347705_your_pic_name.png","created_at":"2019-12-09 14:22:46"}
         * images : [{"id":24,"order_id":"13","image":"3393731577616383_back.jpg","size":null,"created_at":"2019-12-31 05:48:38","updated_at":"2019-12-31 05:48:38"}]
         */

        private OrderBean order;
        private List<ImagesBean> images;

        public OrderBean getOrder() {
            return order;
        }

        public void setOrder(OrderBean order) {
            this.order = order;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public static class OrderBean implements Serializable {
            /**
             * id : 13
             * doctor_first_name : kjjdsf
             * doctor_last_name : gfhg
             * doctor_image : 9332591577347705_your_pic_name.png
             * created_at : 2019-12-09 14:22:46
             */

            private int id;
            private String doctor_first_name;
            private String doctor_last_name;
            private String doctor_image;
            private String created_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getDoctor_first_name() {
                return doctor_first_name;
            }

            public void setDoctor_first_name(String doctor_first_name) {
                this.doctor_first_name = doctor_first_name;
            }

            public String getDoctor_last_name() {
                return doctor_last_name;
            }

            public void setDoctor_last_name(String doctor_last_name) {
                this.doctor_last_name = doctor_last_name;
            }

            public String getDoctor_image() {
                return doctor_image;
            }

            public void setDoctor_image(String doctor_image) {
                this.doctor_image = doctor_image;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }
        }

        public static class ImagesBean implements Serializable {
            /**
             * id : 24
             * order_id : 13
             * image : 3393731577616383_back.jpg
             * size : null
             * created_at : 2019-12-31 05:48:38
             * updated_at : 2019-12-31 05:48:38
             */

            private int id;
            private String order_id;
            private String image;
            private Object size;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getOrder_id() {
                return order_id;
            }

            public void setOrder_id(String order_id) {
                this.order_id = order_id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Object getSize() {
                return size;
            }

            public void setSize(Object size) {
                this.size = size;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}

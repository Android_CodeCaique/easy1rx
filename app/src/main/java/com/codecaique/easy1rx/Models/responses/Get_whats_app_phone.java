package com.codecaique.easy1rx.Models.responses;

public class Get_whats_app_phone {

    /**
     * about_data : {"id":1,"a_title":null,"E_title":null,"logo":"7233501578213912_EJCQy-oX0AACKY9.jpg","description":"منتجات التجميل والبشرة والعناية الصحية","address":"جدة حي الروضة شارع الأمير سلطان","email":"pharmamallksa@gmail.com","phone":"+966570133304","created_at":"2019-12-20 11:27:35","updated_at":"2020-02-14 00:19:33"}
     * error : 0
     * message : show about us data
     */

    private AboutDataBean about_data;
    private int error;
    private String message;

    public AboutDataBean getAbout_data() {
        return about_data;
    }

    public void setAbout_data(AboutDataBean about_data) {
        this.about_data = about_data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class AboutDataBean {
        /**
         * id : 1
         * a_title : null
         * E_title : null
         * logo : 7233501578213912_EJCQy-oX0AACKY9.jpg
         * description : منتجات التجميل والبشرة والعناية الصحية
         * address : جدة حي الروضة شارع الأمير سلطان
         * email : pharmamallksa@gmail.com
         * phone : +966570133304
         * created_at : 2019-12-20 11:27:35
         * updated_at : 2020-02-14 00:19:33
         */

        private int id;
        private Object a_title;
        private Object E_title;
        private String logo;
        private String description;
        private String address;
        private String email;
        private String phone;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getA_title() {
            return a_title;
        }

        public void setA_title(Object a_title) {
            this.a_title = a_title;
        }

        public Object getE_title() {
            return E_title;
        }

        public void setE_title(Object E_title) {
            this.E_title = E_title;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

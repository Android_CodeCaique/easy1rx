package com.codecaique.easy1rx.Models.responses;

import java.io.Serializable;
import java.util.List;

public class PatientOrdersResponse implements Serializable {

    /**
     * user_orders : [{"id":12,"user_name":"shaimaa","doctor_name":null,"patient_city":"Jeddah","doctor_city":null,"state":"receive","price":"100","promocode":"2232","pro_description":null,"created_at":"2019-12-09 14:22:11","updated_at":"2019-12-09 12:22:11"},{"id":14,"user_name":"shaimaa","doctor_name":null,"patient_city":"Jeddah","doctor_city":null,"state":"refused","price":"0","promocode":"765","pro_description":null,"created_at":"2019-12-09 14:23:08","updated_at":"2019-12-09 12:23:08"},{"id":15,"user_name":"shaimaa","doctor_name":null,"patient_city":"Jeddah","doctor_city":null,"state":"refused","price":"0","promocode":"3563","pro_description":null,"created_at":"2019-12-09 14:28:24","updated_at":"2019-12-09 12:28:24"},{"id":21,"user_name":"shaimaa","doctor_name":null,"patient_city":"Jeddah","doctor_city":null,"state":"accept","price":"1000","promocode":null,"pro_description":null,"created_at":"2019-12-10 00:46:25","updated_at":"2019-12-31 12:40:26"},{"id":27,"user_name":"shaimaa","doctor_name":"dsjgjhg","patient_city":"Jeddah","doctor_city":"ننااعخاعه","state":"receive","price":"0","promocode":null,"pro_description":null,"created_at":"2019-12-10 04:57:19","updated_at":"2020-01-01 13:26:05"},{"id":32,"user_name":"shaimaa","doctor_name":null,"patient_city":"Jeddah","doctor_city":null,"state":"accept","price":"100000000000","promocode":null,"pro_description":null,"created_at":"2019-12-10 19:19:16","updated_at":"2019-12-31 12:47:00"},{"id":34,"user_name":"shaimaa","doctor_name":null,"patient_city":"Jeddah","doctor_city":null,"state":"receive","price":"1000","promocode":null,"pro_description":null,"created_at":"2019-12-10 19:21:03","updated_at":"2020-01-01 13:40:39"},{"id":35,"user_name":"shaimaa","doctor_name":null,"patient_city":"Jeddah","doctor_city":null,"state":"receive","price":"10","promocode":null,"pro_description":null,"created_at":"2019-11-04 18:41:29","updated_at":"2019-12-18 16:41:29"}]
     * error : 0
     * message : show all orders for user
     */

    private int error;
    private String message;
    private List<UserOrdersBean> user_orders;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserOrdersBean> getUser_orders() {
        return user_orders;
    }

    public void setUser_orders(List<UserOrdersBean> user_orders) {
        this.user_orders = user_orders;
    }

    public static class UserOrdersBean implements Serializable {
        /**
         * id : 12
         * user_name : shaimaa
         * doctor_name : null
         * patient_city : Jeddah
         * doctor_city : null
         * state : receive
         * price : 100
         * promocode : 2232
         * pro_description : null
         * created_at : 2019-12-09 14:22:11
         * updated_at : 2019-12-09 12:22:11
         */

        private int id;
        private String user_name;
        private String doctor_name;
        private String patient_city;
        private String doctor_city;
        private String image;
        private String state;
        private String price;
        private String promocode;
        private String pro_description;
        private String longitude;
        private String latitude;
        private String created_at;
        private String updated_at;


        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public Object getDoctor_name() {
            return doctor_name;
        }

        public void setDoctor_name(String doctor_name) {
            this.doctor_name = doctor_name;
        }

        public String getPatient_city() {
            return patient_city;
        }

        public void setPatient_city(String patient_city) {
            this.patient_city = patient_city;
        }

        public Object getDoctor_city() {
            return doctor_city;
        }

        public void setDoctor_city(String doctor_city) {
            this.doctor_city = doctor_city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPromocode() {
            return promocode;
        }

        public void setPromocode(String promocode) {
            this.promocode = promocode;
        }

        public String getPro_description() {
            return pro_description;
        }

        public void setPro_description(String pro_description) {
            this.pro_description = pro_description;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

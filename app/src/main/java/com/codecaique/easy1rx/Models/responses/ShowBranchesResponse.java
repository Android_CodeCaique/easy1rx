package com.codecaique.easy1rx.Models.responses;

import java.util.List;

public class ShowBranchesResponse {

    /**
     * branchs_data : [{"id":1,"a_name":"مصر","E_name":null,"longitude":null,"latitude":null,"address":"ff","created_at":"2019-12-18 15:19:24","updated_at":"2019-12-23 14:46:44"},{"id":3,"a_name":"ن","E_name":null,"longitude":"332","latitude":"222","address":"df","created_at":"2019-12-23 16:17:31","updated_at":"2020-01-01 14:56:16"},{"id":5,"a_name":"dfhjh","E_name":null,"longitude":null,"latitude":null,"address":"dfd","created_at":"2019-12-24 10:41:07","updated_at":"2019-12-24 08:49:16"},{"id":6,"a_name":"مصر","E_name":null,"longitude":null,"latitude":null,"address":"dfddf","created_at":"2019-12-24 10:43:59","updated_at":"2019-12-24 08:45:27"},{"id":7,"a_name":"safsds","E_name":null,"longitude":"332","latitude":"222","address":null,"created_at":"2019-12-25 13:09:36","updated_at":"2019-12-25 11:09:36"},{"id":8,"a_name":"safsds","E_name":null,"longitude":"332","latitude":"222","address":"jhgjhgjhgjh","created_at":"2019-12-25 13:10:00","updated_at":"2019-12-25 11:10:00"},{"id":9,"a_name":"سشتنياس","E_name":"sahdgh","longitude":"664","latitude":"987","address":"fdg","created_at":"2019-12-25 13:10:07","updated_at":"2020-01-05 09:51:37"},{"id":10,"a_name":"sssss","E_name":null,"longitude":null,"latitude":null,"address":null,"created_at":"2020-01-01 16:52:28","updated_at":"2020-01-01 14:52:28"},{"id":12,"a_name":"safsds","E_name":null,"longitude":"332","latitude":"222","address":"jhgjhgjhgjh","created_at":"2020-01-05 11:40:58","updated_at":"2020-01-05 09:40:58"},{"id":13,"a_name":"بتاننت","E_name":"dsklhfkds","longitude":"332","latitude":"222","address":"jhgjhgjhgjh","created_at":"2020-01-05 11:50:56","updated_at":"2020-01-05 09:50:56"},{"id":15,"a_name":"للل","E_name":"kkk","longitude":"1","latitude":"-2","address":"benisuef, egypt","created_at":"2020-01-08 17:05:48","updated_at":"2020-01-08 15:05:48"}]
     * error : 0
     * message : show all branches
     */

    private int error;
    private String message;
    private List<BranchsDataBean> branchs_data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BranchsDataBean> getBranchs_data() {
        return branchs_data;
    }

    public void setBranchs_data(List<BranchsDataBean> branchs_data) {
        this.branchs_data = branchs_data;
    }

    public static class BranchsDataBean {
        /**
         * id : 1
         * a_name : مصر
         * E_name : null
         * longitude : null
         * latitude : null
         * address : ff
         * created_at : 2019-12-18 15:19:24
         * updated_at : 2019-12-23 14:46:44
         */

        private int id;
        private String a_name;
        private String E_name;
        private String longitude;
        private String latitude;
        private String address;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getA_name() {
            return a_name;
        }

        public void setA_name(String a_name) {
            this.a_name = a_name;
        }

        public String getE_name() {
            return E_name;
        }

        public void setE_name(String e_name) {
            E_name = e_name;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

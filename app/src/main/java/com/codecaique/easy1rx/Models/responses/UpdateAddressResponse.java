package com.codecaique.easy1rx.Models.responses;

public class UpdateAddressResponse {

    /**
     * error : 0
     * message : update success
     */

    private int error;
    private String message;
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.codecaique.easy1rx.Models.responses;

import java.util.List;

public class Notification_offer_response {

    /**
     * data : [{"id":3,"a_name":"k","E_name":null,"image":"2028481577717536_bg5.jpg","price_before":"100","offer_start_date":null,"offer_end_date":null,"offer_price":"33887","offer_arrange":"2","type":"p","created_at":"2019-12-20 14:32:18","updated_at":"2019-12-30 23:55:13"}]
     * images : []
     * error : 0
     * message : show successfully
     */

    private int error;
    private String message;
    private List<DataBean> data;
    private List<?> images;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public List<?> getImages() {
        return images;
    }

    public void setImages(List<?> images) {
        this.images = images;
    }

    public static class DataBean {
        /**
         * id : 3
         * a_name : k
         * E_name : null
         * image : 2028481577717536_bg5.jpg
         * price_before : 100
         * offer_start_date : null
         * offer_end_date : null
         * offer_price : 33887
         * offer_arrange : 2
         * type : p
         * created_at : 2019-12-20 14:32:18
         * updated_at : 2019-12-30 23:55:13
         */

        private int id;
        private String a_name;
        private String E_name;
        private String image;
        private String price_before;
        private String offer_start_date;
        private String offer_end_date;
        private String offer_price;
        private String offer_arrange;
        private String type;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getA_name() {
            return a_name;
        }

        public void setA_name(String a_name) {
            this.a_name = a_name;
        }



        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice_before() {
            return price_before;
        }

        public void setPrice_before(String price_before) {
            this.price_before = price_before;
        }


        public String getE_name() {
            return E_name;
        }

        public void setE_name(String e_name) {
            E_name = e_name;
        }

        public String getOffer_start_date() {
            return offer_start_date;
        }

        public void setOffer_start_date(String offer_start_date) {
            this.offer_start_date = offer_start_date;
        }

        public String getOffer_end_date() {
            return offer_end_date;
        }

        public void setOffer_end_date(String offer_end_date) {
            this.offer_end_date = offer_end_date;
        }

        public String getOffer_price() {
            return offer_price;
        }

        public void setOffer_price(String offer_price) {
            this.offer_price = offer_price;
        }

        public String getOffer_arrange() {
            return offer_arrange;
        }

        public void setOffer_arrange(String offer_arrange) {
            this.offer_arrange = offer_arrange;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

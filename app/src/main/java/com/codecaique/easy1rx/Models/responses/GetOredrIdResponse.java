package com.codecaique.easy1rx.Models.responses;

public class GetOredrIdResponse {

    /**
     * order_data : {"user_id":"5","doctor_id":null,"state":"requested","price":0,"created_at":"2020-01-23 15:41:53","updated_at":"2020-01-23 13:41:53","id":47}
     * error : 0
     * message : order inserted success
     */

    private OrderDataBean order_data;
    private int error;
    private String message;

    public OrderDataBean getOrder_data() {
        return order_data;
    }

    public void setOrder_data(OrderDataBean order_data) {
        this.order_data = order_data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class OrderDataBean {
        /**
         * user_id : 5
         * doctor_id : null
         * state : requested
         * price : 0
         * created_at : 2020-01-23 15:41:53
         * updated_at : 2020-01-23 13:41:53
         * id : 47
         */

        private String user_id;
        private Object doctor_id;
        private String state;
        private int price;
        private String created_at;
        private String updated_at;
        private int id;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public Object getDoctor_id() {
            return doctor_id;
        }

        public void setDoctor_id(Object doctor_id) {
            this.doctor_id = doctor_id;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}

package com.codecaique.easy1rx.Models.rquests;

import android.net.Uri;

import java.util.List;

public class DoctorMakeorderModel {
private order_data order_data;

    public DoctorMakeorderModel.order_data getOrder_data() {
        return order_data;
    }

    public void setOrder_data(DoctorMakeorderModel.order_data order_data) {
        this.order_data = order_data;
    }

    public static class order_data {
        int order_id;
        List<Uri> image;
        String name;
        String phone;
        String pro_description;

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }

        public List<Uri> getImage() {
            return image;
        }

        public void setImage(List<Uri> images) {
            this.image = images;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPro_description() {
            return pro_description;
        }

        public void setPro_description(String pro_description) {
            this.pro_description = pro_description;
        }
    }
}

package com.codecaique.easy1rx.Models.responses;

import java.util.List;

public class ProfileResponse {

    /**
     * user_data : [{"id":22,"first_name":"mohamed","last_name":"kariim","phone":"25555","passwords":"123","email":"kariim2@gmail.com","image":"1979271578736061_ui-zac.jpg","group_id":"2","city_arabicname":null,"city_englishname":null,"address":null,"land_mark":null,"created_at":"2020-01-11 11:47:41","updated_at":"2020-01-19 09:41:24"}]
     * error : 0
     * message : show data of user
     */

    private int error;
    private String message;
    private List<UserDataBean> user_data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserDataBean> getUser_data() {
        return user_data;
    }

    public void setUser_data(List<UserDataBean> user_data) {
        this.user_data = user_data;
    }

    public static class UserDataBean {
        /**
         * id : 22
         * first_name : mohamed
         * last_name : kariim
         * phone : 25555
         * passwords : 123
         * email : kariim2@gmail.com
         * image : 1979271578736061_ui-zac.jpg
         * group_id : 2
         * city_arabicname : null
         * city_englishname : null
         * address : null
         * land_mark : null
         * created_at : 2020-01-11 11:47:41
         * updated_at : 2020-01-19 09:41:24
         */

        private int id;
        private String first_name;
        private String last_name;
        private String phone;
        private String passwords;
        private String email;
        private String image;
        private String group_id;
        private String created_at;
        private String updated_at;
        private String gender;
        private String reality_program;


        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getReality_program() {
            return reality_program;
        }

        public void setReality_program(String reality_program) {
            this.reality_program = reality_program;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPasswords() {
            return passwords;
        }

        public void setPasswords(String passwords) {
            this.passwords = passwords;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getGroup_id() {
            return group_id;
        }

        public void setGroup_id(String group_id) {
            this.group_id = group_id;
        }



        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

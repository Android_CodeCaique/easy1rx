package com.codecaique.easy1rx.Models.responses;

import java.util.List;

public class OrderImagesResponse {

    /**
     * data : [{"id":15,"image":"3393731577616383_back.jpg"},{"id":17,"image":"3393731577616383_back.jpg"},{"id":39,"image":"6220451577554806_back.jpg"},{"id":40,"image":"6220451577554806_back.jpg"},{"id":41,"image":"6220451577554806_back.jpg"}]
     * error : 0
     * message : show all images of order
     */

    private int error;
    private String message;
    private List<DataBean> data;
    private  String comments;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public static class DataBean {
        /**
         * id : 15
         * image : 3393731577616383_back.jpg
         */

        private int id;
        private String image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}

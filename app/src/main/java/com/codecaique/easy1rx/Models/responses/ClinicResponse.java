package com.codecaique.easy1rx.Models.responses;

import java.io.Serializable;
import java.util.List;

public class ClinicResponse implements Serializable {


    /**
     * offers_data : [{"id":8,"arabic_name":null,"english_name":"fhgfh","image":"hgfgfhg","price_before":null,"offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"c","created_at":"2019-12-21 13:38:15","updated_at":"2020-01-08 15:35:56"},{"id":9,"arabic_name":null,"english_name":"ygj ghg","image":"jhgjhgjhh","price_before":null,"offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"c","created_at":"2019-12-21 13:38:35","updated_at":"2020-01-08 15:33:48"},{"id":10,"arabic_name":"تتت","english_name":"Kkk","image":null,"price_before":null,"offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"c","created_at":"2019-12-21 13:39:11","updated_at":"2020-01-08 16:21:08"},{"id":12,"arabic_name":null,"english_name":null,"image":"4477441576928395_64457407_146922786478445_2092562123069587456_n.jpg","price_before":null,"offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"c","created_at":"2019-12-21 13:39:55","updated_at":"2019-12-21 11:39:55"},{"id":14,"arabic_name":"ghjhgj","english_name":"hjgjh","image":"3884611576928510_64457407_146922786478445_2092562123069587456_n.jpg","price_before":null,"offer_start_date":"2019-12-10","offer_end_date":"2019-12-30","offer_price":"200","offer_arrange":"2","type":"c","created_at":"2019-12-21 13:41:50","updated_at":"2019-12-21 11:41:50"},{"id":22,"arabic_name":"الصالونات النسائية","english_name":"hgjh","image":"1455541577716145_bg5.jpg","price_before":"200","offer_start_date":"2020-01-01","offer_end_date":"2019-12-28","offer_price":"100","offer_arrange":"10","type":"c","created_at":"2019-12-30 16:29:05","updated_at":"2019-12-30 14:29:05"},{"id":23,"arabic_name":"aaaa","english_name":"jgjhgjhj","image":"8976511577716207_logo3.png","price_before":"200","offer_start_date":null,"offer_end_date":null,"offer_price":"100","offer_arrange":"3","type":"c","created_at":"2019-12-30 16:30:07","updated_at":"2019-12-30 16:54:01"},{"id":27,"arabic_name":"gfgjhg","english_name":"gjhgjhj","image":"1582831577911771_dr.organic.jpg","price_before":"100","offer_start_date":"2020-01-01","offer_end_date":"2020-01-31","offer_price":"50","offer_arrange":"1","type":"c","created_at":"2020-01-01 22:49:31","updated_at":"2020-01-08 15:25:56"}]
     * error : 0
     * message : show all offers
     */

    private int error;
    private String message;
    private List<OffersDataBean> offers_data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OffersDataBean> getOffers_data() {
        return offers_data;
    }

    public void setOffers_data(List<OffersDataBean> offers_data) {
        this.offers_data = offers_data;
    }

    public static class OffersDataBean implements Serializable{
        /**
         * id : 8
         * arabic_name : null
         * english_name : fhgfh
         * image : hgfgfhg
         * price_before : null
         * offer_start_date : 2019-12-10
         * offer_end_date : 2019-12-30
         * offer_price : 200
         * offer_arrange : 2
         * type : c
         * created_at : 2019-12-21 13:38:15
         * updated_at : 2020-01-08 15:35:56
         */

        private int id;
        private String arabic_name;
        private String english_name;
        private String image;
        private String price_before;
        private String offer_start_date;
        private String offer_end_date;
        private String offer_price;
        private String offer_arrange;
        private String type;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getArabic_name() {
            return arabic_name;
        }

        public void setArabic_name(String arabic_name) {
            this.arabic_name = arabic_name;
        }

        public String getEnglish_name() {
            return english_name;
        }

        public void setEnglish_name(String english_name) {
            this.english_name = english_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice_before() {
            return price_before;
        }

        public void setPrice_before(String price_before) {
            this.price_before = price_before;
        }

        public String getOffer_start_date() {
            return offer_start_date;
        }

        public void setOffer_start_date(String offer_start_date) {
            this.offer_start_date = offer_start_date;
        }

        public String getOffer_end_date() {
            return offer_end_date;
        }

        public void setOffer_end_date(String offer_end_date) {
            this.offer_end_date = offer_end_date;
        }

        public String getOffer_price() {
            return offer_price;
        }

        public void setOffer_price(String offer_price) {
            this.offer_price = offer_price;
        }

        public String getOffer_arrange() {
            return offer_arrange;
        }

        public void setOffer_arrange(String offer_arrange) {
            this.offer_arrange = offer_arrange;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

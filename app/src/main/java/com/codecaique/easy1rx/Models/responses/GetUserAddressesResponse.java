package com.codecaique.easy1rx.Models.responses;

import java.util.List;

public class GetUserAddressesResponse {

    /**
     * data : [{"id":2,"land_mark":"dsfj gdjhfgjh","address":"h gd fgfds gjgjh dj","city_arabicname":"Jeddah","E_name":"jeddah"}]
     * error : 0
     * message : show address data
     */

    private int error;
    private String message;
    private List<DataBean> data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 2
         * land_mark : dsfj gdjhfgjh
         * address : h gd fgfds gjgjh dj
         * city_arabicname : Jeddah
         * E_name : jeddah
         */

        private int id;
        private String land_mark;
        private String address;
        private String city_arabicname;


        private String city;
        private String E_name;
        String lat;
        String lng ;

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLand_mark() {
            return land_mark;
        }

        public void setLand_mark(String land_mark) {
            this.land_mark = land_mark;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity_arabicname() {
            return city_arabicname;
        }

        public void setCity_arabicname(String city_arabicname) {
            this.city_arabicname = city_arabicname;
        }

        public String getE_name() {
            return E_name;
        }

        public void setE_name(String E_name) {
            this.E_name = E_name;
        }
        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

    }
}

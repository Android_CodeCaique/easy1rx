package com.codecaique.easy1rx.Models.responses;

import java.util.List;

public class NotificationResponse {

    /**
     * notification_data : [{"id":7,"title":null,"body":"hkjhkjhjk","user_id":"2","state":"ffff","created_at":"2020-01-09 11:16:17","updated_at":"2020-01-09 09:16:17"}]
     * error : 0
     * message : show all notification
     */

    private int error;
    private String message;
    private List<NotificationDataBean> notification_data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NotificationDataBean> getNotification_data() {
        return notification_data;
    }

    public void setNotification_data(List<NotificationDataBean> notification_data) {
        this.notification_data = notification_data;
    }

    public static class NotificationDataBean {
        /**
         * id : 7
         * title : null
         * body : hkjhkjhjk
         * user_id : 2
         * state : ffff
         * created_at : 2020-01-09 11:16:17
         * updated_at : 2020-01-09 09:16:17
         */

        private int id;
        private String title;
        private String body;
        private String user_id;
        private String state;
        private String item_id;
        private String created_at;
        private String updated_at;

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

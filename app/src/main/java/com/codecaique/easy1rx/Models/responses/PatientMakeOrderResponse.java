package com.codecaique.easy1rx.Models.responses;

import java.util.List;

public class PatientMakeOrderResponse {

    /**
     * order_data : {"order_details":{"id":41,"user_id":"7","doctor_id":"6","price":"0","state":"neglect","promocode":"1","pro_description":null,"delivery":null,"created_at":"2020-01-06 13:49:36","updated_at":"2020-01-22 17:04:20"},"images":[{"id":52,"order_id":"41","image":"9917771578317268_medicine.jpg","size":null,"created_at":"2020-01-06 15:27:48","updated_at":"2020-01-06 13:27:48"},{"id":51,"order_id":"41","image":"6228601578317086_medicine.jpg","size":null,"created_at":"2020-01-06 15:24:46","updated_at":"2020-01-06 13:24:46"},{"id":59,"order_id":"41","image":"1789401579705460_IMG_1849.JPG","size":null,"created_at":"2020-01-22 17:04:20","updated_at":"2020-01-22 08:04:20"},{"id":60,"order_id":"41","image":"4367691579705460_IMG_1854.JPG","size":null,"created_at":"2020-01-22 17:04:20","updated_at":"2020-01-22 08:04:20"}]}
     * error : 0
     * message : order inserted success
     */

    private OrderDataBean order_data;
    private int error;
    private String message;

    public OrderDataBean getOrder_data() {
        return order_data;
    }

    public void setOrder_data(OrderDataBean order_data) {
        this.order_data = order_data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class OrderDataBean {
        /**
         * order_details : {"id":41,"user_id":"7","doctor_id":"6","price":"0","state":"neglect","promocode":"1","pro_description":null,"delivery":null,"created_at":"2020-01-06 13:49:36","updated_at":"2020-01-22 17:04:20"}
         * images : [{"id":52,"order_id":"41","image":"9917771578317268_medicine.jpg","size":null,"created_at":"2020-01-06 15:27:48","updated_at":"2020-01-06 13:27:48"},{"id":51,"order_id":"41","image":"6228601578317086_medicine.jpg","size":null,"created_at":"2020-01-06 15:24:46","updated_at":"2020-01-06 13:24:46"},{"id":59,"order_id":"41","image":"1789401579705460_IMG_1849.JPG","size":null,"created_at":"2020-01-22 17:04:20","updated_at":"2020-01-22 08:04:20"},{"id":60,"order_id":"41","image":"4367691579705460_IMG_1854.JPG","size":null,"created_at":"2020-01-22 17:04:20","updated_at":"2020-01-22 08:04:20"}]
         */

        private OrderDetailsBean order_details;
        private List<ImagesBean> images;
        private List<AddressBean> address;
        private List<AddressBean> Address;

        public OrderDetailsBean getOrder_details() {
            return order_details;
        }

        public void setOrder_details(OrderDetailsBean order_details) {
            this.order_details = order_details;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public static class OrderDetailsBean {
            /**
             * id : 41
             * user_id : 7
             * doctor_id : 6
             * price : 0
             * state : neglect
             * promocode : 1
             * pro_description : null
             * delivery : null
             * created_at : 2020-01-06 13:49:36
             * updated_at : 2020-01-22 17:04:20
             */

            private int id;
            private String user_id;
            private String doctor_id;
            private String price;
            private String state;
            private String promocode;
            private Object pro_description;
            private Object delivery;
            private String address;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getDoctor_id() {
                return doctor_id;
            }

            public void setDoctor_id(String doctor_id) {
                this.doctor_id = doctor_id;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getPromocode() {
                return promocode;
            }

            public void setPromocode(String promocode) {
                this.promocode = promocode;
            }

            public Object getPro_description() {
                return pro_description;
            }

            public void setPro_description(Object pro_description) {
                this.pro_description = pro_description;
            }

            public Object getDelivery() {
                return delivery;
            }

            public void setDelivery(Object delivery) {
                this.delivery = delivery;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class ImagesBean {
            /**
             * id : 52
             * order_id : 41
             * image : 9917771578317268_medicine.jpg
             * size : null
             * created_at : 2020-01-06 15:27:48
             * updated_at : 2020-01-06 13:27:48
             */

            private int id;
            private String order_id;
            private String image;
            private Object size;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getOrder_id() {
                return order_id;
            }

            public void setOrder_id(String order_id) {
                this.order_id = order_id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public Object getSize() {
                return size;
            }

            public void setSize(Object size) {
                this.size = size;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class AddressBean {
            private int id;
            private String land_mark;
            private String city;
            private String address;
            private String user_id;
            private String longitude;
            private String latitude;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getLand_mark() {
                return land_mark;
            }

            public void setLand_mark(String land_mark) {
                this.land_mark = land_mark;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }


    }
}

package com.codecaique.easy1rx.Models.responses;

import java.io.Serializable;
import java.util.List;

public class Notification_order_response implements Serializable {

    /**
     * data : [{"id":18,"state":"accept","price":"100","promocode":null,"pro_description":null,"created_at":"2019-12-09 21:32:50","updated_at":"2019-12-31 19:36:03"}]
     * images : [{"id":16,"image":"3393731577616383_back.jpg"}]
     * error : 0
     * message : show successfully
     */

    private int error;
    private String message;
    private List<DataBean> data;
    private List<ImagesBean> images;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public List<ImagesBean> getImages() {
        return images;
    }

    public void setImages(List<ImagesBean> images) {
        this.images = images;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 18
         * state : accept
         * price : 100
         * promocode : null
         * pro_description : null
         * created_at : 2019-12-09 21:32:50
         * updated_at : 2019-12-31 19:36:03
         */

        private int id;
        private String state;
        private String price;
        private String doctor_name;
        private String promocode;
        private String branch_id;
        private String pro_description;
        private String longitude;
        private String latitude;
        private String created_at;
        private String updated_at;

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getBranch_id() {
            return branch_id;
        }

        public void setBranch_id(String branch_id) {
            this.branch_id = branch_id;
        }

        public String getDoctor_name() {
            return doctor_name;
        }

        public void setDoctor_name(String doctor_name) {
            this.doctor_name = doctor_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPromocode() {
            return promocode;
        }

        public void setPromocode(String promocode) {
            this.promocode = promocode;
        }

        public String getPro_description() {
            return pro_description;
        }

        public void setPro_description(String pro_description) {
            this.pro_description = pro_description;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }

    public static class ImagesBean implements Serializable {
        /**
         * id : 16
         * image : 3393731577616383_back.jpg
         */

        private int id;
        private String image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }
}

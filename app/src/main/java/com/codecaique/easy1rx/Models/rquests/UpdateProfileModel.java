package com.codecaique.easy1rx.Models.rquests;

import java.util.List;

import okhttp3.MultipartBody;

public class UpdateProfileModel {
    public static class UserDataBean {
        /**
         * id : 22
         * first_name : fgfdgfdg
         * last_name : dddddkjhkjhkj
         * phone : 00000054
         * passwords : 111
         * email : dsfd ssfs fsd
         * image : 1979271578736061_ui-zac.jpg
         * state : الادمن المسؤل عن المستخدمين
         * gender : male
         * address : djjkfhk
         * land_mark : dsfds dsf
         * city_arabicname : يسابن
         * city_englishname : skjhkj
         * firebase_token : null
         * created_at : 2020-01-11 11:47:41
         * updated_at : 2020-01-23 11:27:08
         */

        private int user_id;
        private String first_name;
        private String last_name;
        private String phone;
        private String password;
        private String email;
        private List<MultipartBody.Part> image;
        private String gender;
        private String address;
        private String land_mark;
        private String city_id;
        private String reality_pro;

        public String getReality_pro() {
            return reality_pro;
        }

        public void setReality_pro(String reality_pro) {
            this.reality_pro = reality_pro;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }



        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String passwords) {
            this.password = passwords;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public List<MultipartBody.Part> getImage() {
            return image;
        }

        public void setImage(List<MultipartBody.Part> image) {
            this.image = image;
        }



        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLand_mark() {
            return land_mark;
        }

        public void setLand_mark(String land_mark) {
            this.land_mark = land_mark;
        }




    }
}

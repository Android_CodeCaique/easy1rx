package com.codecaique.easy1rx.Models.responses;

public class UpdateProfileReaponse {

    /**
     * user_data : {"id":22,"first_name":"fgfdgfdg","last_name":"dddddkjhkjhkj","phone":"00000054","passwords":"111","email":"dsfd ssfs fsd","image":"1979271578736061_ui-zac.jpg","state":"الادمن المسؤل عن المستخدمين","gender":"male","address":"djjkfhk","land_mark":"dsfds dsf","city_arabicname":"يسابن","city_englishname":"skjhkj","firebase_token":null,"created_at":"2020-01-11 11:47:41","updated_at":"2020-01-23 11:27:08"}
     * error : 0
     * message : update data successfully
     */

    private UserDataBean user_data;
    private int error;
    private String message;

    public UserDataBean getUser_data() {
        return user_data;
    }

    public void setUser_data(UserDataBean user_data) {
        this.user_data = user_data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class UserDataBean {
        /**
         * id : 22
         * first_name : fgfdgfdg
         * last_name : dddddkjhkjhkj
         * phone : 00000054
         * passwords : 111
         * email : dsfd ssfs fsd
         * image : 1979271578736061_ui-zac.jpg
         * state : الادمن المسؤل عن المستخدمين
         * gender : male
         * address : djjkfhk
         * land_mark : dsfds dsf
         * city_arabicname : يسابن
         * city_englishname : skjhkj
         * firebase_token : null
         * created_at : 2020-01-11 11:47:41
         * updated_at : 2020-01-23 11:27:08
         */

        private int id;
        private String first_name;
        private String last_name;
        private String phone;
        private String password;
        private String email;
        private String image;
        private String state;
        private String gender;
        private String address;
        private String land_mark;
        private String city_arabicname;
        private String city_englishname;
        private Object firebase_token;
        private String created_at;
        private String updated_at;
        private String city_id;
        private String reality_pro;

        public String getReality_pro() {
            return reality_pro;
        }

        public void setReality_pro(String reality_pro) {
            this.reality_pro = reality_pro;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }



        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String passwords) {
            this.password = passwords;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLand_mark() {
            return land_mark;
        }

        public void setLand_mark(String land_mark) {
            this.land_mark = land_mark;
        }

        public String getCity_arabicname() {
            return city_arabicname;
        }

        public void setCity_arabicname(String city_arabicname) {
            this.city_arabicname = city_arabicname;
        }

        public String getCity_englishname() {
            return city_englishname;
        }

        public void setCity_englishname(String city_englishname) {
            this.city_englishname = city_englishname;
        }

        public Object getFirebase_token() {
            return firebase_token;
        }

        public void setFirebase_token(Object firebase_token) {
            this.firebase_token = firebase_token;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

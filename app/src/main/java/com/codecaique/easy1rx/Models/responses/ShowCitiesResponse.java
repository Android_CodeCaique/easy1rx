package com.codecaique.easy1rx.Models.responses;

import java.util.List;

public class ShowCitiesResponse {

    /**
     * user_data : [{"id":1,"a_name":"Jeddah","E_name":null,"longitude":"21.4498898","latitude":"38.9309561","created_at":"2019-12-21 04:22:59","updated_at":"2020-01-01 23:28:50"},{"id":2,"a_name":"يسابن","E_name":"skjhkj","longitude":"2122","latitude":"12","created_at":"2019-12-21 04:22:59","updated_at":"2020-01-05 11:54:18"},{"id":4,"a_name":"ننااعخاعه","E_name":"Khamis Mushait","longitude":"18.329384","latitude":"42.759365","created_at":"2019-12-21 04:22:59","updated_at":"2020-01-09 10:50:22"},{"id":5,"a_name":"Taif","E_name":null,"longitude":"21.437273","latitude":"40.512714","created_at":"2019-12-21 04:22:59","updated_at":"2020-01-01 23:35:41"},{"id":6,"a_name":"sdg sjgd","E_name":null,"longitude":null,"latitude":null,"created_at":"2019-12-21 13:33:14","updated_at":"2019-12-21 11:33:14"},{"id":9,"a_name":"sdg sjgd","E_name":null,"longitude":null,"latitude":null,"created_at":"2019-12-23 11:18:50","updated_at":"2019-12-23 09:18:50"},{"id":10,"a_name":"mmmm","E_name":null,"longitude":null,"latitude":null,"created_at":"2019-12-30 14:59:42","updated_at":"2019-12-30 12:59:42"},{"id":11,"a_name":"mmm","E_name":null,"longitude":"112","latitude":"32","created_at":"2019-12-30 15:00:07","updated_at":"2019-12-30 13:00:08"},{"id":12,"a_name":"مصر","E_name":null,"longitude":null,"latitude":null,"created_at":"2019-12-31 09:43:40","updated_at":"2019-12-31 07:43:40"},{"id":13,"a_name":"كويا","E_name":null,"longitude":"1","latitude":"2","created_at":"2019-12-31 09:48:30","updated_at":"2019-12-31 07:48:30"},{"id":14,"a_name":"Riyadh","E_name":null,"longitude":"24.774265","latitude":"46.738586","created_at":"2019-12-31 09:48:41","updated_at":"2020-01-01 23:36:14"},{"id":15,"a_name":"Thuwal","E_name":null,"longitude":"22.3042003","latitude":"39.0505997","created_at":"2020-01-01 23:30:40","updated_at":"2020-01-01 21:30:40"},{"id":16,"a_name":"بايسب","E_name":"fjhk","longitude":"112","latitude":"32","created_at":"2020-01-05 11:53:44","updated_at":"2020-01-05 09:53:44"}]
     * error : 0
     * message : show all cities
     */

    private int error;
    private String message;
    private List<UserDataBean> user_data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserDataBean> getUser_data() {
        return user_data;
    }

    public void setUser_data(List<UserDataBean> user_data) {
        this.user_data = user_data;
    }

    public static class UserDataBean {
        /**
         * id : 1
         * a_name : Jeddah
         * E_name : null
         * longitude : 21.4498898
         * latitude : 38.9309561
         * created_at : 2019-12-21 04:22:59
         * updated_at : 2020-01-01 23:28:50
         */

        private int id;
        private String a_name;
        private String E_name;
        private String longitude;
        private String latitude;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getA_name() {
            return a_name;
        }

        public void setA_name(String a_name) {
            this.a_name = a_name;
        }

        public String getE_name() {
            return E_name;
        }

        public void setE_name(String E_name) {
            this.E_name = E_name;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

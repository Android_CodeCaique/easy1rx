package com.codecaique.easy1rx.Models.responses;

import java.util.List;

public class AskDeliveryResponse {

    /**
     * order_details : [{"id":47,"user_id":"5","doctor_id":null,"price":"0","state":"wait","promocode":null,"pro_description":null,"delivery":"1","address":"jhfjhg","created_at":"2020-01-23 15:41:53","updated_at":"2020-01-23 16:38:12"}]
     * images : []
     * error : 0
     * message : order inserted success
     */

    private int error;
    private String message;
    private List<OrderDetailsBean> order_details;
    private List<?> images;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OrderDetailsBean> getOrder_details() {
        return order_details;
    }

    public void setOrder_details(List<OrderDetailsBean> order_details) {
        this.order_details = order_details;
    }

    public List<?> getImages() {
        return images;
    }

    public void setImages(List<?> images) {
        this.images = images;
    }

    public static class OrderDetailsBean {
        /**
         * id : 47
         * user_id : 5
         * doctor_id : null
         * price : 0
         * state : wait
         * promocode : null
         * pro_description : null
         * delivery : 1
         * address : jhfjhg
         * created_at : 2020-01-23 15:41:53
         * updated_at : 2020-01-23 16:38:12
         */

        private int id;
        private String user_id;
        private Object doctor_id;
        private String price;
        private String state;
        private Object promocode;
        private Object pro_description;
        private String delivery;
        private String address;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public Object getDoctor_id() {
            return doctor_id;
        }

        public void setDoctor_id(Object doctor_id) {
            this.doctor_id = doctor_id;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public Object getPromocode() {
            return promocode;
        }

        public void setPromocode(Object promocode) {
            this.promocode = promocode;
        }

        public Object getPro_description() {
            return pro_description;
        }

        public void setPro_description(Object pro_description) {
            this.pro_description = pro_description;
        }

        public String getDelivery() {
            return delivery;
        }

        public void setDelivery(String delivery) {
            this.delivery = delivery;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
